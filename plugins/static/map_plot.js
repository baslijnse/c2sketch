class MapView extends HTMLElement {
    constructor() {
        super();
        this.contacts = [];

        let shadow = this.attachShadow({mode:'open'});
             
        let style = document.createElement('link');
        style.setAttribute('rel','stylesheet');
        style.setAttribute('href','/static/node_modules/leaflet/dist/leaflet.css');
        shadow.appendChild(style);
     
        let mapdiv = document.createElement('div');
        mapdiv.setAttribute('style',this.getAttribute('style'));
        mapdiv.setAttribute('class',this.getAttribute('class'));
        
        shadow.appendChild(mapdiv);

        const lat = parseFloat(this.getAttribute('lat'));
        const lng = parseFloat(this.getAttribute('lng'));
        const zoom = parseInt(this.getAttribute('zoom'));
        const base = this.getAttribute('base');

        this.map = L.map(mapdiv).setView([lat, lng], zoom);
        L.tileLayer(base, {}).addTo(this.map);

        this.drawContacts();
    }
    connectedCallback() {
        this.map.invalidateSize();
    }
    drawContacts() {

        const markers = this.getElementsByTagName('map-marker');
        for(let marker of markers) {
            const lat = parseFloat(marker.getAttribute('lat'));
            const lng = parseFloat(marker.getAttribute('lng'));
            if(isNaN(lat) || isNaN(lng)) {
                continue;
            }
            const identifier = marker.getAttribute('identifier');
            const label = marker.getAttribute('label');
            const color = marker.getAttribute('color');
            
            const heading = marker.getAttribute('heading');
            let marker_svg = '';
            if(heading !== null) {
                marker_svg = `<polygon points="45,55 50,40, 55,55" fill="${color}" stroke-width="0" stroke="transparent" transform="rotate(${heading},50,50)" />`;   
            } else {
                marker_svg = `<rect x="45" y="45" width="10" height="10" fill="${color}" stroke-width="0" stroke="transparent" />`;
            }
            const svg = `
                <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100">
                ${marker_svg}
                <text x="0" y="20" style="font-family: Monaco; font-size: 15px; fill: ${color};">${label}</text>
                </svg>
                `
            const url = 'data:image/svg+xml;base64,' + btoa(svg);
            const icon = L.icon({iconUrl: url, iconSize:[100,100]})
            this.contacts.push(L.marker([lat, lng],{'icon':icon,'title':label}).addTo(this.map));
        }
    }
    toppyt_replace(element) {
        this.replaceChildren(...element.children);
        const removals = [...this.contacts];
       
        for(let marker of removals) {
            this.map.removeLayer(marker);
        }
        this.drawContacts();
    }
}
customElements.define('map-view',MapView);