"""Represent models in c2sketch's own concrete syntax"""
from __future__  import annotations

from c2sketch.models import (Node, Model, Import, Attribute, Constraint, Actor, ActorType, ActorGroup, ActorMember, ActorLocation, Location, LocationGroup, LocationMember,
                               InformationSpaceRequirement, InformationSpaceBinding,
                               InformationSpace, FieldMode, KeyLimit, AgeLimit, Record, RecordType, RecordTypeField,
                               Task, TaskReference, Trigger, TaskDefinition, TaskInstance )
from typing import Any
import pathlib
import os

def model_node_to_c2s_str(node: Node) -> str:
    
    def indent(text: str) -> str:
        return os.sep.join(f'    {line}' for line in text.splitlines())
    def nodes_str(nodes: list[Node]) -> str:
        return f':{os.linesep}{os.linesep.join(indent(model_node_to_c2s_str(node)) for node in nodes)}' if nodes else ''
    def fields_str(fields: dict[str,Any]) -> str:
        fields_str = [f'{key}="{value}"' for key, value in fields.items()]
        return f'{{{", ".join(fields_str)}}}'

    match node:
        case Model():
            return os.linesep.join(model_node_to_c2s_str(child_node) for child_node in node.nodes)
        case Import(parent,nodes,reference):
            return f'import {reference}'
        case Attribute(parent,nodes,name,value):
            if os.linesep in value:
                return f'@{name}:{os.sep}{indent(value)}'
            else:
                return f'@{name} "{value}"'
        case Constraint(parent,nodes,name,value):
            return f'!{name} {value}'    
        case Actor(parent,nodes,name,parameter_type):
            type_str = '' if parameter_type is None else f'[{parameter_type}]'
            return f'actor {name}{type_str}{nodes_str(node.nodes)}'
        case ActorMember(parent,nodes,actor_id):
            return f'member {actor_id}'
        case ActorGroup(parent,nodes,group_id):
            return f'group {group_id}'
        case ActorLocation(parent,nodes,location_id):
            return f'at-location {location_id}'
        case Location(parent,nodes,name):
            return f'location {name}{nodes_str(node.nodes)}'
        case LocationMember(parent,nodes,location_id):
            return f'member {location_id}'
        case LocationGroup(parent,nodes,group_id):
            return f'group {group_id}'
        case Task(parent,nodes,name):
            return f'task {name}{nodes_str(node.nodes)}'
        case TaskReference(parent,nodes,reference,parameter):
            parameter_str = '' if parameter is None else f' {fields_str(parameter)}'
            return f'task-ref {reference}{parameter_str}'
        case Trigger(parent,nodes,reference):
            return f'trigger {reference}'
        case InformationSpaceRequirement(parent,nodes,name,type,read,write,binding): 
            if read and write:
                direction_str = '<-> '
            elif read:
                direction_str = '<- '
            elif write:
                direction_str = '-> '
            else:
                direction_str = ''
            type_str = '' if type is None else f' [{type}]'
            binding_str = '' if binding is None else f' = {binding}'
            return f'info-req {direction_str}{name}{type_str}{binding_str}'  
        case TaskDefinition(parent,nodes,name,parameter_type):
            type_str = '' if parameter_type is None else f'[{parameter_type}]'
            return f'task-def {name}{type_str}{nodes_str(node.nodes)}'
        case TaskInstance(parent,nodes,name,sequence,parameter) if parameter is not None:
            parameter_str = '' if parameter is None else f' {fields_str(parameter)}'
            return f'task-instance {name}{parameter_str}{nodes_str(node.nodes)}'
        case InformationSpaceBinding(parent,nodes,name,binding):
            return f'info-req {name} = {binding}'
        case InformationSpace(parent,nodes,name,type):
            type_str = '' if type is None else f' [{type}]'
            return f'info-space {name}{type_str}{nodes_str(node.nodes)}'
        case FieldMode(parent,nodes,field_name,mode):
            return f'field-mode {field_name} {mode}'
        case KeyLimit(parent,nodes,limit):
            return f'key-limit {limit}'
        case AgeLimit(parent,nodes,limit):
            return f'age-limit {limit}'
        case Record(parent,nodes,fields):
            return f'record {fields_str(fields)}'
        case RecordType(parent,nodes,name):
            return f'record-type {name}{nodes_str(node.nodes)}'
        case RecordTypeField(parent,nodes,name,type):
            type_str = '' if type is None else f' [{type}]'
            return f'field {name}{type_str}'
    return ''

def model_to_c2s_str(model: Model) -> str:
    return model_node_to_c2s_str(model)

def model_to_c2s_file(model: Model, path: str | pathlib.Path) -> None:
    
    if isinstance(path,str):
        path = pathlib.Path(path)

    path.write_text(model_to_c2s_str(model))