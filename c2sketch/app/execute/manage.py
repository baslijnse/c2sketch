"""Creating and managing execution runs"""

from toppyt import (ParallelTask, TaskResult, TaskStatus, TaskVariable,
                    after_value, all_tasks, constant,
                    enter_information, forever,
                    update_information,
                    with_information,
                    write_information)
from toppyt.bulma import BulmaButtons

from c2sketch.models import *
from ..config import AppConfig
from ..data import ModelStore, ExecutionStore
from ..ui import *
from ..patterns import action_choice

__all__ = ('manage_executions',)

def manage_executions(config: AppConfig, model_store: ModelStore, execute_store: ExecutionStore, path_var, cookie_jar):
    
    selection = TaskVariable(None)
 
    def select_execution():
        def to_nodes(executions,scenarios):
            execution_nodes = [
                {'name': execution_id,'icon': 'play','value': f'e:{execution_id}','children':[]}
                for execution_id in sorted(executions)
            ]
            
            scenario_nodes = [
                {'name': scenario_id,'icon': 'file','value': f's:{scenario_id}','children':[]}
                for scenario_id in sorted(scenarios)
            ]
            nodes = [{'name': 'Active scenario\'s','icon':'folder','value':None,'children': execution_nodes},
                     {'name': 'Saved scenario\'s','icon':'folder','value':None,'children': scenario_nodes}
                     ]
            return nodes

        return with_information(execute_store, lambda es:es.list_executions(),
            lambda executions: 
                with_information(execute_store, lambda es:es.list_scenarios(),
                lambda scenarios:              
                    update_information(selection, editor=TreeChoice(to_nodes(executions,scenarios)))
                )
        )
    def select_action(selection):
        execution_id = None if selection is None or not selection.startswith('e:') else selection[2:]
        scenario_id = None if selection is None or not selection.startswith('s:') else selection[2:]
        actions = [
                ('new','New...','plus',add_new_execution(model_store,execute_store)),
                ('load','Load...','play',None if scenario_id is None else load_execution(model_store,execute_store,scenario_id)),
                ('save','Save...','save',None if execution_id is None else save_execution(execute_store,execution_id)),
                ('delete','Delete...','trash',None if execution_id is None else delete_execution(execute_store,execution_id))
                ]
        return forever(action_choice(actions))

    def manage_selected_execution():    
        return with_information(selection,lambda s:s.read(),lambda selection_id:
                view_information('')
                if selection_id is None or not selection_id.startswith('e:') else
                with_information(execute_store,lambda es: es.execution_exists(selection_id[2:]),lambda exists:
                    view_information('')
                    if not exists else
                    manage_running_execution(model_store,execute_store,path_var,selection_id[2:])
                ))
                               
    def layout(parts):
        return f'''
        <div class="prepare-grid">
            <div class="prepare-header">{header_layout(config.name, 'Execute','','',parts['app_mode'])}</div>
            <div class="prepare-body">
                <div class="prepare-side">
                    <div class="panel-block buttons">
                    {parts['execution_actions']}
                    </div>
                    <div class="panel-block">
                    {parts['choose_execution']}
                    </div>
                </div>
                <flex-resizer></flex-resizer>
                <div class="prepare-main">
                <div class="container prepare-inner">
                {parts['preview_execution']}
                </div>
                </div>
            </div>
        </div>
        '''

    return ParallelTask(
        [('choose_execution',select_execution())
        ,('execution_actions',with_information(selection,lambda s:s.read(), select_action))
        ,('preview_execution',manage_selected_execution())
        ,('app_mode',choose_app_mode(path_var))
        ],layout=layout,result_builder=lambda rs:rs[-1])


def manage_running_execution(model_store: ModelStore, execute_store: ExecutionStore, path_var: TaskVariable[str], execution_id: ScenarioID):

    def view_scenario_info():
        editor = BulmaRecordEditor([
            ('model',BulmaTextView(label='Model'))
        ])
        
        async def read_fun(es: ExecutionStore):
            scenario = await es.execution_scenario(execution_id)
            return {'model':scenario.model}
        
        return view_information(execute_store,read_fun,editor=editor)

    def manage_actor_status():

        def view_status(actors, status):

            def choose_action():
                headers=['Actor with agent','Active Agent']
                options = []
                for (actor_id,actor_label) in actors:
                    enabled_label = str(status[actor_id]) if actor_id in status else 'N/A'
                    actions = [('view','View','eye')]
                    if actor_id in status:
                        actions = [('start','Start agent','play'),('stop','Stop agent','stop')] + actions
                    options.append(
                        (actor_id,[actor_label,enabled_label],actions)
                    )
                
                editor = TableChoice(options=options,headers=headers)
                return enter_information(editor=editor)

            def do_action(action):
                match action[1]:
                    case 'view':
                        return write_information(path_var,lambda pv: pv.write(f'/execute/{execution_id}/{action[0]}'))
                    case 'start':
                        return write_information(execute_store,lambda es:es.start_agent(execution_id,action[0]))
                    case 'stop':
                        return write_information(execute_store,lambda es:es.stop_agent(execution_id,action[0]))
                return constant(None)
            
            return forever(after_value(choose_action(),do_action))

        def agent_actions():
            actions = [('start','Start all agents','play'),('stop','Stop all agents','stop')]

            def do_action(action):
                match action:
                    case 'start':
                        return write_information(execute_store,lambda es: es.start_agents(execution_id))
                    case 'stop':
                        return write_information(execute_store,lambda es: es.stop_agents(execution_id))
            return forever(
                after_value(enter_information(editor=BulmaButtons(actions)),do_action)
            )

        return all_tasks(
            with_information(execute_store,lambda es:es.list_actors(execution_id),
                lambda actors: with_information(execute_store,lambda es:es.agent_status(execution_id),
                    lambda agent_status: view_status(actors, agent_status))),
            agent_actions()
        )
   
    def control_timer():

        def view_time():
            editor = BulmaTextView(label='Time')
            return view_information(execute_store,lambda es:es.execution_time(execution_id),editor=editor)
    
        def timer_actions():
            actions = [('step','Step','step-forward'),('play','Play','play'),('stop','Stop','stop')]

            def do_action(action):
                match action:
                    case 'step':
                        return write_information(execute_store,lambda es: es.step_timer(execution_id))
                    case 'play':
                        return write_information(execute_store,lambda es: es.start_timer(execution_id))
                    case 'stop':
                        return write_information(execute_store,lambda es: es.stop_timer(execution_id))
            return forever(
                after_value(enter_information(editor=BulmaButtons(actions)),do_action)
            )
        return all_tasks(
            view_time(),
            timer_actions()
        )

    return all_tasks(
        view_scenario_info(),
        control_timer(),
        manage_actor_status() 
    )

def add_new_execution(model_store: ModelStore, execute_store: ExecutionStore):
    def enter(task, validate):

        def enter_name(name, validate):
            help = None
            if validate is not None and 'name' in validate:
                help = (validate['name'],'danger')
            
            return update_information(name,editor=BulmaTextInput(help=help,label='Scenario name'))
        
        def choose_model(model_ids, model_id):
            help = None
            if validate is not None and 'model_id' in validate:
                help = (validate['model_id'],'danger')
            options = [(label if label is not None else value,value) for (value,label,_) in model_ids]

            return update_information(model_id,editor=BulmaSelect(options=options,label='Model',help=help))
        
        def result(results):
            names = ['model_id','name']
            return TaskResult({k: r.value for k,r in zip(names,results)}, TaskStatus.ACTIVE)
    
        return with_information(model_store,lambda ps: ps.list_models(), lambda model_ids: ParallelTask([  
            ('model_id',choose_model(model_ids,None if task is None else task.get('model_id'))),
            ('name',enter_name(None if task is None else task.get('name'),validate))
        ],result_builder=result))
        
    def validate(task, action):
        if action == 'cancel':
            return constant({})
        
        def check_input(existing_scenarios):
            errors = {}

            name = None if task is None else task.get('name','')
           
            if 'model_id' not in task or task['model_id'] is None or task['model_id'] == '':
                errors['model_id'] = 'You need to select a model to start a scenario.'

            if not is_safe_name(name):
                errors['name'] = 'Name is not a valid name. It may not contain spaces and cannot start with a number.'
            else:
                if name in existing_scenarios:
                    errors['name'] = f'A scenario named \'{name}\' is already active.'

            return constant(errors)
        
        return with_information(execute_store, lambda es: es.list_executions(),check_input,refresh=False)
     
    def save(task):
        name = task.get('name','')
        if name == '':
            name = 'untitled'
        model_id = task.get('model_id')
        return write_information(execute_store,lambda ps:ps.create_execution(name,model_id))
    
    return after_dialog('New scenario...',
                enter,
                validate,
                [('cancel','Cancel','ban',lambda _: constant(None))
                ,('add','Add','plus',save)
                ])

def save_execution(execute_store: ExecutionStore, scenario_id: ScenarioID):
    def enter(name, validate):
        if name is None:
            name = scenario_id
        help = None
        if validate is not None and 'name' in validate:
            help = (validate['name'],'danger')
        
        return update_information(name,editor=BulmaTextInput(help=help,label='Scenario name'))

    def validate(name, action):
        if action == 'cancel':
            return constant({})
        
        def check_input(existing_scenarios):
            errors = {}

            if not is_safe_name(name):
                errors['name'] = 'Name is not a valid name. It may not contain spaces and cannot start with a number.'
            else:
                if name in existing_scenarios:
                    errors['name'] = f'A scenario named \'{name}\' already exists.'

            return constant(errors)
        
        return with_information(execute_store, lambda ms: ms.list_scenarios(),check_input,refresh=False)
     
    def save(name):
        return write_information(execute_store,lambda es:es.save_execution(scenario_id, name))
        return constant(None)
    
    return after_dialog('Save scenario...',
                enter,
                validate,
                [('cancel','Cancel','ban',lambda _: constant(None))
                ,('add','Add','plus',save)
                ])

def delete_execution(execute_store: ExecutionStore, scenario_id: ScenarioID):
    return write_information(execute_store, lambda es: es.delete_execution(scenario_id))

def load_execution(model_store: ModelStore, execute_store: ExecutionStore,scenario_id: ScenarioID):
    return write_information(execute_store, lambda es: es.load_execution(scenario_id))

###############

# def choose_scenario(execute_store: ExecutionStore, model_id: ModelID, path_var: TaskVariable):
#     def choose_action(scenarios):    
#         options = [(s[0],[str(n),('view',s[0]), s[1]],[('delete','Delete','trash')]) for n, s in enumerate(scenarios,1)]
#         actions = [('add','Add scenario','plus')]
#         return any_task(
#                 enter_information(editor=TableChoice(options)),
#                 map_value(enter_information(editor=BulmaButtons(actions,align_right=True)),lambda c: (None,c))
#             )
#     def do_action(scenario_id,action):
#         if action == 'view':
#             url = f'/{model_id}/execute/{scenario_id}'
#             return write_information(path_var, lambda p: p.write(url))
#         if action == 'add':
#             return add_scenario(execute_store, model_id)
#         # if action == 'delete':
#         #     return delete_scenario(execute_store, model_id, scenario_id)
#         return constant(None)
  
#     return with_information(execute_store,lambda es: es.list_scenarios(model_id),lambda scenarios:
#         forever(after_value(choose_action(scenarios),lambda r: do_action(*r))))

# def add_scenario(execute_store: ExecutionStore, model_id):
    
#     def enter(task, validate):

#         def enter_title(title):
#             return update_information(title,editor=BulmaTextInput(label='Title',sync=True))            

#         def enter_name(name, title, validate):
#             help = None
#             if validate is not None and 'name' in validate:
#                 help = (validate['name'],'danger')
#             placeholder = to_safe_name('' if title.value is None else title.value)
#             return update_information(name,editor=BulmaTextInput(placeholder=placeholder,help=help,label='Name'))
        
#         def enter_description(description):
#             return update_information(description,editor=BulmaTextArea(label='Description'))            

#         def result(results):
#             names = ['title','name','description']
#             return TaskResult({k: r.value for k,r in zip(names,results)}, TaskStatus.ACTIVE)
    
#         return with_information(execute_store,lambda ps: ps.list_scenarios(model_id), lambda scenarios: ParallelTask([
#             ('title',enter_title(None if task is None else task.get('title'))),
#             ('name','title',lambda title: enter_name(None if task is None else task.get('name'),title,validate)),
#             ('description',enter_description(None if task is None else task.get('description')))
#         ],result_builder=result))
        
#     def validate(task, action):
#         if action == 'cancel':
#             return constant({})
        
#         def check_input(existing_scenarios):
#             errors = {}

#             name = None if task is None else task.get('name','')
#             if name == '':
#                 name = to_safe_name(task.get('title',''))
            
#             if not is_safe_name(name):
#                 errors['name'] = 'Name is not a valid name. It may not contain spaces and cannot start with a number.'
#             else:
#                 if name in [item[0] for item in existing_scenarios]:
#                     errors['name'] = f'A scenario named \'{name}\' already exists.'

#             return constant(errors)
        
#         return with_information(execute_store, lambda ps: ps.list_scenarios(model_id),check_input,refresh=False)
     
#     def save(task):
#         title = task.get('title')
#         name = task.get('name','')
#         if name == '':
#             name = to_safe_name('' if title is None else title)
#         description = task.get('description')
#         return write_information(execute_store,lambda ps:ps.create_scenario(model_id,name,title,description))
    
#     return after_dialog('Add scenario...',
#                 enter,
#                 validate,
#                 [('cancel','Cancel','ban',lambda _: constant(None))
#                 ,('add','Add','plus',save)
#                 ])
