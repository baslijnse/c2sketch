
import os
import asyncio

from toppyt import DataSource, read, write
from copy import copy, deepcopy

from toppyt.datasources import Registration
from toppyt.tasks import Application

from c2sketch.models import *
from c2sketch.execute import ExecutionState
from c2sketch.app.plugins import AppPluginLoader

from ..read.model import model_from_c2s_file
from ..read.folder import model_set_from_folder, list_models_in_folder
from ..read.scenario import scenario_from_c2e_file
from ..write.model import model_to_c2s_file
from ..write.scenario import scenario_to_c2e_file
from ..edit.model import get_node_source, update_node_source

from typing import Optional, Any
from pathlib import Path

class ModelStore(DataSource):
    """Database abstraction that manages a collection of mission plans"""

    model_path: Path
    model_set: ModelSet
    model_ids: set[ModelID]

    def __init__(self, model_path: Path, initial_models: Optional[dict[str,Model]] = None):
        self.model_path = model_path
        self.model_set = ModelSet()
        
        #Scan model path for c2s files
        self.model_ids = list_models_in_folder(self.model_path)

        if initial_models is not None:
            for model_id, model in initial_models.items():
                self.model_set.add_model(model)
                self.model_ids.add(model_id)
            
        super().__init__()

    def _list_models(self) -> list[ModelID]:
        return list(sorted(self.model_ids))

    def load_opt_plan(self, model_id: ModelID) -> Optional[Model]:
        
        if self.model_set.model_exists(model_id):
            return self.model_set.get_model_by_id(model_id)
        else:
            parts = model_id.split('.')
            path = Path(self.model_path).joinpath(*(parts[:-1] + [f'{parts[-1]}.c2s']))
            if path.is_file():
                model = model_from_c2s_file(model_id, path)
                self.model_set.add_model(model,path)
                return model
            else:
                return None
            
    def load_model(self, model_id: ModelID) -> Model:
        model = self.load_opt_plan(model_id)
        if model is None:
            raise KeyError(f'No such model {model_id}')
        else:
            return model

    def save_model(self, model: Model) -> None:
        #Write model to disk
        parts = model.id.split('.')

        if len(parts) > 1:
            namespace_dir = os.path.join(self.model_path,*(parts[:-1]))
        else:
            namespace_dir = self.model_path
        if not os.path.isdir(namespace_dir):
            os.makedirs(namespace_dir,exist_ok=True)
       
        #DON'T WRITE UNTIL PROPERLY TESTED
        #model_to_c2s_file(model,os.path.join(namespace_dir, f'{parts[-1]}.c2s'))

        #Add or update the planset
        #self.model_set.models[model.id] = model

    # Managing plans
    @read          
    async def list_models(self) -> list[tuple[ModelID, str | None, str | None]]:
        items = []
        for model_id in self._list_models():
            model: Model | None = None
            
            if self.model_set.model_exists(model_id):
                model = self.model_set.get_model_by_id(model_id)
                items.append((model.id,model.title,model.summary))
            else:
                items.append((model_id,None,None))
        return items

    @write
    async def write_create_plan(self, plan_id, template_id = None,title = None):
        if template_id is not None:
            plan = deepcopy(self.load_model(template_id))
            plan.id = plan_id
        else:
            plan = Model(plan_id, title)   
        self.save_model(plan)

    @write
    async def write_import_plan(self, plan_id, yaml_path, title = None):
        plan = model_from_c2s_file(plan_id, yaml_path)
        if not title is None:
            plan.title = title
        self.save_model(plan)

    @write
    async def write_delete_plan(self, plan_id):
        parts = plan_id.split('.')
        path = os.path.join(*([self.model_path] + parts[:-1] + [f'{parts[-1]}.yml']))
        if os.path.isfile(path):
            os.remove(path)
        if plan_id in self.model_set.models:
            self.model_set.models.pop(plan_id)
        
    ## Editing the elements of a single model ##
    @read
    async def model(self, model_id: ModelID):
        return self.load_model(model_id)
    
    # Model structure

    @read
    async def read_actor_exists(self, model_id: ModelID, actor_id: ActorID) -> bool: 
        return self.model_set.actor_exist(actor_id, model_id)
    
    @read
    async def read_task_exists(self, model_id: ModelID, task_id: TaskID) -> bool:
        return self.model_set.task_exist(task_id, model_id)
    
    @read
    async def read_info_space_exists(self, model_id: ModelID, ifs_id: InformationSpaceID) -> bool:
        return self.model_set.info_space_exist(ifs_id, model_id)
    
    @read
    async def read_type_exists(self, model_id: ModelID, type_id: RecordTypeID) -> bool:
        return self.model_set.record_type_exist(type_id, model_id)

    @write
    async def write_create_actor(self,plan_id, actor_name, actor_title = None, actor_description = None, actor_type= None):
        plan = self.load_model(plan_id) 
        plan.add_actor(actor_name,actor_title,actor_description,actor_type)
        self.save_model(plan)

    @write
    async def write_create_task(self,plan_id,parent, task_name, task_title = None, task_description = None):
        plan = self.load_model(plan_id)
        if parent is not None and parent != '':
            task = plan.get_task_by_id(parent)
            task.add_task(task_name,task_title,task_description)
        else:
            plan.add_task(task_name,task_title,task_description)
        self.save_model(plan)

    @write
    async def write_create_info_space(self,plan_id,parent,ifs_name,ifs_title=None,ifs_description = None):
        plan = self.load_model(plan_id)
        if parent is not None and parent != '':
            task = plan.get_task_by_id(parent)
            task.add_info_space(ifs_name,ifs_title,ifs_description)
        else:
            plan.add_info_space(ifs_name,ifs_title,ifs_description)
        self.save_model(plan)

    @write
    async def write_create_type(self,plan_id,type_name):
        plan = self.load_model(plan_id)
        plan.add_record_type(type_name)
        self.save_model(plan)

    @write
    async def write_create_scenario(self,plan_id, scenario_name, scenario_title = None, scenario_description = None):
        plan = self.load_model(plan_id)
        plan.add_scenario(scenario_name,scenario_title,scenario_description)
        self.save_model(plan)
        self.save_model(plan)

    @write
    async def write_move_task(self,plan_id,task_id,new_parent_id):
        plan = self.load_model(plan_id)
        plan.move_task(task_id,new_parent_id)
        self.save_model(plan)

    @write
    async def write_move_task_up(self, plan_id,task_id):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.move_up()
        self.save_model(plan)

    @write
    async def write_move_task_down(self, plan_id, task_id):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.move_down()
        self.save_model(plan)

    @write
    async def write_move_info_space(self,plan_id,ifs_id,new_parent_id):
        plan = self.load_model(plan_id)
        plan.move_info_space(ifs_id,new_parent_id)
        self.save_model(plan)

    @write
    async def write_rename_actor(self,plan_id,actor_id,new_name):
        plan = self.load_model(plan_id)
        plan.rename_actor(actor_id,new_name)
        self.save_model(plan)

    @write
    async def write_rename_task(self,plan_id,task_id,new_name):
        plan = self.load_model(plan_id)
        plan.rename_task(task_id,new_name)
        self.save_model(plan)

    @write
    async def write_rename_info_space(self,plan_id,ifs_id,new_name):
        plan = self.load_model(plan_id)
        plan.rename_info_space(ifs_id,new_name)
        self.save_model(plan)

    @write
    async def write_rename_type(self,plan_id,type_name,new_name):
        plan = self.load_model(plan_id)
        plan.rename_record_type(type_name,new_name)
        self.save_model(plan)

    @write
    async def write_delete_actor(self, plan_id, actor_name):
        plan = self.load_model(plan_id) 
        plan.delete_actor(actor_name)
        self.save_model(plan)

    @write
    async def write_delete_task(self,plan_id,task_id):
        plan = self.load_model(plan_id) 
        plan.delete_task(task_id)
        self.save_model(plan)

    @write
    async def write_delete_info_space(self,plan_id,ifs_id):
        plan = self.load_model(plan_id) 
        plan.delete_info_space(ifs_id)
        self.save_model(plan)

    @write
    async def write_delete_type(self,plan_id,type_name):
        plan = self.load_model(plan_id) 
        plan.delete_record_type(type_name)
        self.save_model(plan)
        
    # Model node attributes
    @read
    async def read_model_attributes(self, model_id):
        model = self.load_model(model_id)
        return {'name':model.id,'title': model.title, 'summary':model.summary}
    
    @write
    async def write_model_attributes(self, model_id, attributes):
        title = None if attributes is None else attributes['title']
        summary = None if attributes is None else attributes['summary']
        model = self.load_model(model_id)
        model.title = title
        model.summary = summary
        self.save_model(model)

    @read
    async def model_imports(self, model_id):
        return self.load_model(model_id).imports

    @write
    async def write_plan_add_import(self, plan_id: ModelID, extra_import: ModelID):
        plan = self.load_model(plan_id)
        plan.add_import(extra_import)
        self.save_model(plan)

    @write
    async def write_plan_remove_import(self, plan_id: ModelID, remove_import: ModelID):
        plan = self.load_model(plan_id)
        plan.delete_import(remove_import) 
        self.save_model(plan)

    @read
    async def model_source(self, model_id: ModelID) -> str:
        model = self.load_model(model_id)
        return '\n'.join(model.source) if model.source else ''
    
    @write
    async def write_model_source(self, model_id: ModelID, update: str) -> None:
        model = self.load_model(model_id)
        update_node_source(model, update)
    
    # Actor node attributes
    @read
    async def actor_attributes(self, model_id: ModelID, actor_id: ActorID):
        actor = self.model_set.get_actor_by_id(actor_id, model_id)
        actor_type = None if actor.type is None else actor.type.value
        return {'name':actor.name,'title':actor.title,'description':actor.description,'type':actor_type}

    @write
    async def write_actor_general(self, model_id: ModelID, actor_id: ActorID, general: dict[str,str]):
        title = None if general is None else general['title']
        description = None if general is None else general['description']
        actor_type = None
        if general is not None and general['type'] is not None:
            actor_type = ActorType(general['type'])

        model = self.load_model(model_id)
        actor = self.model_set.get_actor_by_id(actor_id, model_id)
        actor.title = title
        actor.description = description
        actor.type = actor_type
        self.save_model(model)

    @read
    async def read_actor_affiliations(self, model_id: ModelID, actor_id: ActorID):
        actor_id = global_id_from_id(actor_id,model_id)
        return list_actor_affiliations(self.model_set, model_id, actor_id)
    
    @read
    async def read_actor_affiliated(self, model_id: ModelID, actor_id: ActorID):
        actor_id = global_id_from_id(actor_id,model_id)
        return list_affiliated_actors(self.model_set, model_id, actor_id)
    
    @write
    async def write_actor_add_affiliation(self, model_id: ModelID, actor_id: ActorID, affiliation: ActorID):
        model = self.load_model(model_id)
        actor = self.model_set.get_actor_by_id(actor_id, model_id)
        actor.add_group(affiliation)
        self.save_model(model)

    @write
    async def write_actor_remove_affiliation(self, model_id: ModelID, actor_id: ActorID, affiliation: ActorID):
        model = self.load_model(model_id)
        actor = self.model_set.get_actor_by_id(actor_id, model_id)
        actor.remove_group(affiliation)
        self.save_model(model)

    @write
    async def write_actor_add_affiliated(self, model_id: ModelID, actor_id: ActorID, affiliated: ActorID):
        model = self.load_model(model_id)
        actor = self.model_set.get_actor_by_id(affiliated, model_id)
        actor.add_group(actor_id)
        self.save_model(model)

    @write
    async def write_actor_remove_affiliated(self, model_id: ModelID, actor_id: ActorID, affiliated: ActorID):
        model = self.load_model(model_id)
        actor = self.model_set.get_actor_by_id(affiliated, model_id)
        actor.remove_group(actor_id)
        self.save_model(model)

    @read
    async def actor_source(self, model_id: ModelID, actor_id: ActorID) -> str:
        actor = self.model_set.get_actor_by_id(actor_id, model_id)
        return get_node_source(actor)
    
    @write
    async def write_actor_source(self, model_id: ModelID, actor_id: ActorID, update: str) -> None:
        actor = self.model_set.get_actor_by_id(actor_id, model_id)
        update_node_source(actor,update)

    # Location node attributes
    @read
    async def location_attributes(self, model_id, location_id):
        location = self.model_set.get_location_by_id(location_id, model_id)
        return {'name':location.name,'title':location.title,'description':location.description}
  
    @read
    async def location_source(self, model_id: ModelID, location_id: LocationID) -> str:
        location = self.model_set.get_location_by_id(location_id, model_id)
        return get_node_source(location)
    
    @write
    async def write_location_source(self, model_id: ModelID, location_id: LocationID, update: str) -> None:
        location = self.model_set.get_location_by_id(location_id, model_id)
        update_node_source(location,update)

    # Task node attributes
    
    @read
    async def task_attributes(self, model_id: ModelID, task_id: TaskID):
        task = self.model_set.get_task_by_id(task_id, model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        return {'name':task.name,'title':task.title,'description':task.description,'compound':task.is_compound()}
    
    @write
    async def write_task_general(self, model_id: ModelID, task_id: TaskID, general: dict[str,str]):
        title = None if general is None else general.get('title')
        description = None if general is None else general.get('description')
        model = self.load_model(model_id)
        task = self.model_set.get_task_by_id(task_id, model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        task.title = title
        task.description = description
        self.save_model(model)

    @read
    async def read_task_parameter_type(self, plan_id, task_id, dereference = False):

        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        
        if dereference and isinstance(task.parameter_type, str):
            definition_plan_id = local_id_from_global_id(task.parameter_type)
            if definition_plan_id is None:
                return deepcopy(plan.get_record_type(task.parameter_type))
            else:
                definition_plan = self.load_model(definition_plan_id[1])
                return deepcopy(definition_plan.get_record_type(definition_plan_id[0]))
            
        return deepcopy(task.parameter_type)

    @write
    async def write_task_parameter_type(self, plan_id, task_id, parameter_type):
        plan = self.load_model(plan_id)
        plan.get_task_by_id(task_id).parameter_type = parameter_type
        self.save_model(plan)

    @read
    async def read_task_is_parametric(self, model_id: ModelID, task_id: TaskID) -> bool:
        task = self.model_set.get_task_by_id(task_id, model_id)
        return isinstance(task,TaskDefinition)

    @read
    async def read_task_triggers(self, model_id: ModelID, task_id: TaskID):
        task = self.model_set.get_task_by_id(task_id, model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        return task.triggers
    
    @write
    async def write_task_triggers(self, plan_id, task_id, triggers):
        plan = self.load_model(plan_id)
        plan.get_task_by_id(task_id).triggers = copy(triggers)
        self.save_model(plan)

    @read
    async def read_task_info_space_requirements(self, model_id: ModelID, task_id: TaskID):
        task = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        return task.info_space_requirements
    
    @write
    async def write_task_info_space_requirements(self, plan_id, task_id, ifs_reqs):
        plan = self.load_model(plan_id)
        plan.get_task_by_id(task_id).info_space_requirements = copy(ifs_reqs)
        self.save_model(plan)

    @read
    async def read_task_info_space_bindings(self, model_id: ModelID, task_id: TaskID):
        task = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        bindings = {}
        for req in task.info_space_requirements:
            if req.binding is not None:
                bindings[req.name] = req.binding
        return bindings
    
    @write
    async def write_task_info_space_bindings(self, plan_id, task_id, bindings):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.info_space_bindings = copy(bindings)

        self.save_model(plan)

    @read
    async def read_task_instances_info_space_bindings(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        
        bindings = {}
        for instance in task.instances:
            bindings[instance.name] = instance.info_space_bindings
        return bindings

    @write
    async def write_task_instances_info_space_bindings(self, plan_id, task_id, bindings):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        
        for instance_name, instance_bindings in bindings.items():
            instance_index = int(instance_name.split('-')[1]) - 1 #FIXME: This approach is too fragile
            task.instances[instance_index].info_space_bindings = copy(instance_bindings)
       
        self.save_model(plan)

    @read
    async def read_task_info_space_content(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        return task.info_space_content
    
    @write
    async def write_task_info_space_content(self, plan_id, task_id, content):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.info_space_content = deepcopy(content)

        self.save_model(plan)

    @write
    async def read_task_instances_info_space_content(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        contents = {}
        for instance in task.instances:
            contents[instance.name] = instance.info_space_content
        return contents
    
    @write
    async def write_task_instances_info_space_content(self, plan_id, task_id, content):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        
        for instance_name, instance_content in content.items():
            instance_index = int(instance_name.split('-')[1]) - 1 #FIXME: This approach is too fragile, use same approach as for_actor
            task.instances[instance_index].info_space_content = deepcopy(instance_content)
            
        self.save_model(plan)
    
    @read
    async def read_task_for_actor(self, model_id: ModelID, task_id: TaskID):
        task = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        return task.for_actor
    
    @read
    async def read_task_instances_for_actor(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        return [instance.for_actor for instance in task.instances]
    
    @write
    async def write_task_instances_for_actor(self, plan_id, task_id, for_actors):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)

        for instance, for_actor in zip(task.instances,for_actors):
            instance.for_actor = for_actor
        
        self.save_model(plan)

    @write
    async def write_task_for_actor(self, plan_id, task_id, for_actor):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.for_actor = for_actor
        self.save_model(plan)
    

    @read
    async def read_task_automation_plugin(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        if not task.automation is None:
            return task.automation[0]
        return None
    
    @write
    async def write_task_automation_plugin(self, plan_id, task_id, plugin):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        if not task.automation is None:
            task.automation = (plugin, task.automation[1])
        else:
            task.automation = (plugin, None)
        self.save_model(plan)
    
    @read
    async def read_task_automation_config(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)  
        if not task.automation is None:
            return task.automation[1]
        return None

    @write
    async def write_task_automation_config(self, plan_id, task_id, config):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        if not task.automation is None:
            task.automation = (task.automation[0],copy(config))
        self.save_model(plan)
   
    @read
    async def read_task_instances(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)    
        return task.instances
    
    @write
    async def write_task_instances(self, plan_id, task_id, instances):
        plan = self.load_model(plan_id)
        plan.get_task_by_id(task_id).instances = deepcopy(instances)
        self.save_model(plan)

    @write
    async def write_task_add_instance(self, plan_id, task_id, parameter = None):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.instances = task.instances + [TaskInstance(task,deepcopy(parameter))]
        self.save_model(plan)
    
    @write
    async def write_task_remove_instance(self, plan_id, task_id, instance_no):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        task.instances.pop(instance_no)
        self.save_model(plan)

    @read
    async def task_source(self, model_id: ModelID, task_id: TaskID) -> str:
        task = self.model_set.get_task_by_id(task_id, model_id)
        if not isinstance(task,Task):
            raise ValueError(f'{task_id} does not reference a task')
        
        return get_node_source(task)
   
    @write
    async def write_task_source(self, model_id: ModelID, task_id: TaskID, update: str) -> None:
        task = self.model_set.get_task_by_id(task_id, model_id)
        update_node_source(task, update)

    @read
    async def task_def_attributes(self, model_id: ModelID, task_id: TaskID):
        task_def = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_def,TaskDefinition):
            raise ValueError(f'{task_id} does not reference a task definition')

        return {'name':task_def.name,'title':task_def.title,'description':task_def.description,'compound':task_def.is_compound()}
    
    @read
    async def task_def_source(self, model_id: ModelID, task_id: TaskID) -> str:
        task_def = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_def,TaskDefinition):
            raise ValueError(f'{task_id} does not reference a task definition')
        
        return get_node_source(task_def)
    
    @write
    async def write_task_def_source(self, model_id: ModelID, task_id: TaskID, update: str) -> None:
        task_def = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_def,TaskDefinition):
            raise ValueError(f'{task_id} does not reference a task definition')
        
        update_node_source(task_def,update)


    # Task instance attributes
    @read
    async def read_task_instance_template_id(self, model_id: ModelID, task_id: TaskID):
        
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')
        
        return task_instance.get_definition().node_id

    @read
    async def task_instance_general(self, model_id: ModelID, task_id: TaskID):

        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')

        name = name_from_task_id(task_id)
        definition = task_instance.get_definition()
        parameter = task_instance.parameter if task_instance.parameter is not None else {}
        return Record({'name':name,'title':definition.title,'description':definition.description,'compound':definition.is_compound()}
                                         ).inline_references(Record(parameter)).fields
    
    @read
    async def task_instance_parameter_type(self, model_id: ModelID, task_id: TaskID):
        
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')
        task_definition = task_instance.get_definition()
        
        if task_definition.parameter_type is None:
            return None
        else:
            return self.model_set.get_record_type_by_id(task_definition.parameter_type, model_id)
        
    @read
    async def task_instance_parameter(self, model_id: ModelID, task_id: TaskID):
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')
      
        return task_instance.parameter

    @read
    async def task_instance_info_space_requirements(self, model_id: ModelID, task_id: TaskID):
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')
        return task_instance.info_space_requirements

    @write
    async def write_task_instance_parameter(self, plan_id, instance_id, parameter):
        plan = self.load_model(plan_id)
        instance = plan.get_task_by_id(instance_id)
        instance.parameter = deepcopy(parameter)
        self.save_model(plan)
    
    @read
    async def read_task_instance_for_actor(self, model_id: ModelID, task_id: TaskID):
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')

        return task_instance.for_actor
    
    @write
    async def write_task_instance_for_actor(self, plan_id, instance_id, for_actor):
        plan = self.load_model(plan_id)
        instance = plan.get_task_by_id(instance_id)
        instance.for_actor = for_actor
        self.save_model(plan)

    @read
    async def task_instance_info_space_bindings(self, model_id: ModelID, task_id: TaskID):
        
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')
    
        bindings = {}
        for binding in task_instance.info_space_bindings:
            bindings[binding.name] = binding.binding
        return bindings
    
    @write
    async def write_task_instance_info_space_bindings(self, plan_id, instance_id, bindings):
        plan = self.load_model(plan_id)
        instance = plan.get_task_by_id(instance_id)
        instance.info_space_bindings = copy(bindings)
        self.save_model(plan)

    @read
    async def read_task_instance_info_space_content(self, plan_id, instance_id, ifs_name):
        
        plan = self.load_model(plan_id)
        instance = plan.get_task_by_id(instance_id)
        return []
    
    @write  
    async def write_task_instance_info_space_content(self, plan_id, instance_id, ifs_name, messages):
        plan = self.load_model(plan_id)
        instance = plan.get_task_by_id(instance_id)
        instance.info_space_content[ifs_name] = deepcopy(messages)
        self.save_model(plan)

    @read
    async def task_instance_source(self, model_id: ModelID, task_id: TaskID) -> str:
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')

        return get_node_source(task_instance)
     
    @write
    async def write_task_instance_source(self, model_id: ModelID, task_id: TaskID, update: str) -> None:
        task_instance = self.model_set.get_task_by_id(task_id,model_id)
        if not isinstance(task_instance,TaskInstance):
            raise ValueError(f'{task_id} does not reference a task instance')

        update_node_source(task_instance,update)
    
    # Information space attributes
    @read
    async def read_info_space_general(self, model_id: ModelID, ifs_id: InformationSpaceID):
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        return {'name':ifs.name,'title':ifs.title,'description':ifs.description}
    
    @write
    async def write_info_space_general(self, model_id: ModelID, ifs_id: InformationSpaceID, general):
        title = None if general is None else general['title']
        description = None if general is None else general['description']
        model = self.load_model(model_id)
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        ifs.title = title
        ifs.description = description
        self.save_model(model)

    @read
    async def read_info_space_type(self, model_id: ModelID, ifs_id: InformationSpaceID, dereference = False):
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        return ifs.type
        
    @write
    async def write_info_space_type(self, model_id, ifs_id, type):
        model = self.load_model(model_id)
        model.get_info_space_by_id(ifs_id).type = type
        self.save_model(model)

    @read
    async def read_info_space_init_content(self, model_id: ModelID, ifs_id: InformationSpaceID):
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        return deepcopy(ifs.records)
    
    @write
    async def write_info_space_init_content(self, plan_id, ifs_id, messages):
        plan = self.load_model(plan_id)
        plan.get_info_space_by_id(ifs_id).records = messages
        self.save_model(plan)

    @read
    async def read_info_space_visualization_plugin(self, model_id: ModelID, ifs_id: InformationSpaceID):
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        return ifs.get_attribute('graphic-type')
    
    @write
    async def write_info_space_visualization_plugin(self, plan_id, ifs_id, visualization_plugin):
        plan = self.load_model(plan_id)
        plan.get_info_space_by_id(ifs_id).visualization_plugin = visualization_plugin
        self.save_model(plan)

    @read
    async def read_info_space_display(self, model_id: ModelID, ifs_id: InformationSpaceID):
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        return ifs.display
    
    @write
    async def write_info_space_display(self, model_id: ModelID, ifs_id: InformationSpaceID, display):
        model = self.load_model(model_id)
        self.model_set.get_info_space_by_id(ifs_id, model_id).set_display(display)
        self.save_model(model)

    @read
    async def info_space_source(self, model_id: ModelID, ifs_id: InformationSpaceID) -> str:
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        return get_node_source(ifs)
    
    @write
    async def write_info_space_source(self, model_id: ModelID, ifs_id: InformationSpaceID, update: str) -> None:
        ifs = self.model_set.get_info_space_by_id(ifs_id, model_id)
        update_node_source(ifs,update)
    
    # Record type attributes

    @read
    async def read_type_fields(self, model_id: ModelID, type_id: RecordTypeID):
        record_type = self.model_set.get_record_type_by_id(type_id, model_id)
        return record_type.fields

    @write
    async def write_type_fields(self, plan_id, type_name, fields):
        plan = self.load_model(plan_id)
        type = plan.get_record_type(type_name)
        type.fields = fields
        self.save_model(plan)

    @read
    async def record_type_source(self, model_id: ModelID, type_id: RecordTypeID) -> str:
        record_type = self.model_set.get_record_type_by_id(type_id, model_id)
        return get_node_source(record_type)

    @write
    async def write_record_type_source(self, model_id: ModelID, type_id: RecordTypeID, update: str) -> None:
        record_type = self.model_set.get_record_type_by_id(type_id, model_id)
        update_node_source(record_type,update)

    # Plan sub-nodes

    @read
    async def read_list_actors(self, plan_id):
        plan = self.load_model(plan_id)
        return [(actor.label,actor.node_id) for actor in plan.actors]
    
    @read
    async def read_list_tasks(self, plan_id):
        plan = self.load_model(plan_id)
        return [(task.name,task.name) for task in plan.tasks]
    
    @read
    async def read_plan_tasks(self, plan_id):
        plan = self.load_model(plan_id)
        return list(plan.tasks)
    
    @read
    async def read_list_types(self, plan_id):
        names = self.load_model(plan_id).record_types
        return list(names)
    
    @read
    async def read_list_scenarios(self, plan_id):
        plan = self.load_model(plan_id)
        return [(scenario.name, scenario.title, scenario.description) for scenario in plan.scenarios]
    
    # Actor sub-nodes
    
    @read
    async def read_actor_tasks(self, plan_id, actor_id): #PSEUDO sub-nodes
        return self.load_model(plan_id).get_actor_tasks(actor_id)

    
    # Task sub-nodes
    @read
    async def read_task_parts(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        return task.tasks
    
    @read
    async def read_task_info_spaces(self, plan_id, task_id):
        return []
 
    
    # Information space sub-nodes

    # Message type sub-nodes

    ## Listing reference options (actors, tasks, information spaces, types) ##
    
    @read
    async def read_list_actor_id_options(self, plan_id):
        """List all references to actors, including from imported plans"""
        plan = self.load_model(plan_id)
        options = [(actor.label,actor.name) for actor in plan.actors]
        for imported_plan_id in plan.imports:
            imported_plan = self.load_model(imported_plan_id)
            options.extend([(f'{actor.label} (from {imported_plan_id})',actor.get_id(local=False)) for actor in imported_plan.actors])

        return options

    @read
    async def read_actor_affiliation_options(self, model_id, actor_id):
        #An actor can't be affiliated with itself or its own affiliations
        model = self.load_model(model_id)
        exclude = set([actor_id])

        exclude.update(list_affiliated_actors(self.model_set, global_id_from_id(actor_id,model_id)))
        exclude.update(list_actor_affiliations(self.model_set, global_id_from_id(actor_id,model_id)))
        
        options = [a.name for a in model.actors if not a.name in exclude]
        return options

    @read
    async def read_actor_affiliated_options(self, model_id, actor_id):
        #An actor can't add his own affiliations as affiliated actor
        model = self.load_model(model_id)
        exclude = set([actor_id])
        exclude.update(list_actor_affiliations(self.model_set, global_id_from_id(actor_id,model_id)))
        return [a.name for a in model.actors if not a.name in exclude]
    
    @read
    async def read_list_executors(self, plan_id):
        plan = self.load_model(plan_id)
        executors = plan.get_executors()
        return [(actor.name,actor.name) for actor in plan.actors if actor.name in executors]

    @read
    async def read_list_instance_id_options(self, plan_id):
        plan = self.load_model(plan_id)
        return plan.list_instance_id_options()

    @read
    async def read_task_info_spaces_in_scope(self, plan_id, task_id):
        task = self.load_model(plan_id).get_task_by_id(task_id)
        return task.get_info_spaces_in_scope()

    @read
    async def read_list_type_options(self, plan_id):
        """List all references to types, including from imported plans"""     
        plan = self.load_model(plan_id)
        options = [(type.name,type.name) for type in plan.record_types]

        for imported_plan_id in plan.imports:
            imported_plan = self.load_opt_plan(imported_plan_id)
            if not imported_plan is None:
                options.extend([(f'{t.name} (from {imported_plan_id})',f'{t.name}@{imported_plan_id}') for t in imported_plan.record_types])

        return options

    ## Looking up referenced elements internal and external ##
    @read
    async def read_plan(self, plan_id):
        plan = self.load_opt_plan(plan_id)
        if not plan is None:
            return deepcopy(plan)
        return None
    
    @read
    async def read_info_space(self, ifs_id):
        return self.model_set.get_info_space_by_id(ifs_id)

    @read
    async def read_type(self, type_id):
        return self.model_set.get_record_type_by_id(type_id)
    
    @read
    async def read_resolve_info_space_id(self, plan_id, task_id, ifs_name):
        plan = self.load_model(plan_id)
        task = plan.get_task_by_id(task_id)
        ifs_id = task.resolve_info_space(ifs_name)
        return ifs_id


    ## Internal analysis ##
    @read
    async def read_info_space_producers(self, model_id, ifs_id):
        ifs = self.load_model(model_id).get_info_space_by_id(ifs_id)
        return []
        #return ifs.find_uses(include_producers=True,include_consumers=False)

    @read
    async def read_info_space_consumers(self, model_id, ifs_id):
        ifs = self.load_model(model_id).get_info_space_by_id(ifs_id)
        return []
        #return ifs.find_uses(include_producers=False,include_consumers=True)
    
    @read
    async def read_info_space_triggered(self, model_id, ifs_id):
        ifs = self.load_model(model_id).get_info_space_by_id(ifs_id)
        return []
        #return ifs.find_triggered()
    
    @read
    async def read_type_uses(self, model_id, type_id):
        model = self.load_model(model_id)
        return [] #TODO

    @staticmethod
    def affects(action:str, action_args: list[Any], view: str, view_args: list[Any]) -> bool:
        #Notify on structural changes
        if view == 'plan':
            if action.startswith('create') or action.startswith('delete') or action.startswith('rename') or action.startswith('move'):
                return action_args[0] == view_args[0]
        
            if action in ['actor_add_affiliation','actor_remove_affiliation','actor_add_affiliated','actor_remove_affiliated','task_add_instance','task_remove_instance']:
                return True

        if (view == 'task_info_spaces' or view == 'task_parts') and (action.startswith('create') or action.startswith('delete')):
            return True #TODO: Should be even more specific
        if view == 'list_scenarios' and action in ['create_scenario','delete_scenario']:
            return action_args[0] == view_args[0]
        if view in ['task_parts','plan_tasks'] and action in ['move_task_up','move_task_down','create_task','delete_task']:
            return action_args[0] == view_args[0]
        if action.startswith('plan_') and view.startswith('plan_'):
            return action_args[0] == view_args[0]
        if action.startswith('actor_') and view.startswith('actor_'):
            return action_args[0:2] == view_args[0:2]
        if action.startswith('task_') and view.startswith('task_'):
            return action_args[0:2] == view_args[0:2]
        if action.startswith('info_space_') and view.startswith('info_space_'):
            return action_args[0:2] == view_args[0:2]
        if action.startswith('type_') and view.startswith('type_'):
            return action_args[0:2] == view_args[0:2]
        
        return False

class ExecutionStore(DataSource):
    """Database abstraction that manages execution information"""

    model_path: Path
    scenario_path: Path
    plugin_loader: AppPluginLoader

    executions: dict[ScenarioID,ExecutionState]
    timers: dict[ScenarioID,asyncio.Task]

    def __init__(self, model_path: Path, scenario_path: Path, plugin_loader: AppPluginLoader):
        
        self.model_path = model_path
        self.scenario_path = scenario_path
        self.plugin_loader = plugin_loader

        self.executions = {}
        self.timers = {}

        super().__init__()

    def start(self, application: Application):
        return super().start(application)
    
    def register(self, registration: Registration):
        return super().register(registration)
    
    def notify(self):
        match self._method:
            case 'start_agents':
                criterium = lambda method, args, kwargs: method == 'agent_status'
            case 'stop_agents':
                criterium = lambda method, args, kwargs: method == 'agent_status'
            case '_tick_timer':
                criterium = lambda method, args, kwargs: method in ('execution_time','info_space_content','actor_concrete_atomic_tasks')
            case _:
                criterium = lambda method, args, kwargs: True
    
        remaining_readers = []
        if self._readers and self._application is not None:
            for registration, method, args, kwargs in self._readers:
                if criterium(method,args,kwargs):
                    self._application.notify_session(registration.session, registration.task)
                else:
                    remaining_readers.append((registration, method, args, kwargs))
        self._readers = remaining_readers

    @read
    async def list_scenarios(self) -> list[ScenarioID]:
        scenario_ids = []
        for item in self.scenario_path.iterdir():
            if item.name.endswith('.c2e'):
                scenario_ids.append(item.stem) 
        return scenario_ids
    
    @read
    async def list_executions(self) -> list[ScenarioID]:
        return sorted(self.executions.keys())

    @write
    async def create_execution(self, scenario_id: ScenarioID, model_id: ModelID) -> ScenarioID:

        #Construct empty scenario
        scenario = Scenario(scenario_id,model_id)
        models = model_set_from_folder(self.model_path, scenario.model)

        #Construct execution state
        self.executions[scenario_id] = ExecutionState(models,scenario)
    
        return scenario_id
    
    @write
    async def delete_execution(self, scenario_id: ScenarioID):
        if scenario_id not in self.executions:
            return
        
        if scenario_id in self.timers:
            timer = self.timers.pop(scenario_id)
            timer.cancel()

        self.executions.pop(scenario_id)

    @write
    async def load_execution(self, scenario_id: ScenarioID) -> None:
        
        if scenario_id in self.executions:
            return

        scenario = scenario_from_c2e_file(scenario_id,self.scenario_path.joinpath(f'{scenario_id}.c2e'))
        models = model_set_from_folder(self.model_path, scenario.model)
        
        self.executions[scenario_id] = ExecutionState(models,scenario)

    async def save_execution(self, scenario_id: ScenarioID, save_id: ScenarioID) -> None:
        scenario = Scenario(save_id, self.executions[scenario_id].scenario.model, self.executions[scenario_id].scenario.events)
        scenario_to_c2e_file(scenario,self.scenario_path.joinpath(f'{save_id}.c2e'))

    @read
    async def execution_exists(self, scenario_id: ScenarioID) -> bool:
        return scenario_id in self.executions
    
    @read
    async def execution_time(self, scenario_id: ScenarioID) -> int:
        return self.executions[scenario_id].time

    @read
    async def execution_scenario(self, scenario_id: ScenarioID) -> Scenario:
        return self.executions[scenario_id].scenario

    @read
    async def list_actors(self, scenario_id: ScenarioID) -> list[tuple[ActorID,str]]:
        """List all actor id's and titles"""
        result = []
        execution = self.executions[scenario_id]
        for actor_id in execution.models.list_actors(execution.scenario.model):
            actor_label = execution.models.get_actor_by_id(actor_id,execution.scenario.model).label
            result.append((actor_id,actor_label))
        return result
    
    @read
    async def list_locations(self, scenario_id: ScenarioID) -> list[tuple[LocationID,str]]:
        """List all actor id's and titles"""
        result = []
        execution = self.executions[scenario_id]
        for location_id in execution.models.list_locations(execution.scenario.model):
            location_label = execution.models.get_location_by_id(location_id,execution.scenario.model).label
            result.append((location_id,location_label))
        return result

    @read
    async def agent_status(self, scenario_id: ScenarioID) -> dict[ActorID,bool]:
        execution = self.executions[scenario_id]
        return execution.agent_status()

    @read
    async def actor_title(self, scenario_id: ScenarioID, actor_id: ActorID) -> str:
        models = self.executions[scenario_id].models
        scenario = self.executions[scenario_id].scenario
        actor = models.get_actor_by_id(actor_id,scenario.model)

        return actor.label

    @read
    async def actor_locations(self, scenario_id: ScenarioID, actor_id: ActorID) -> str:
        models = self.executions[scenario_id].models
        scenario = self.executions[scenario_id].scenario
        actor = models.get_actor_by_id(actor_id,scenario.model)
        locations = actor.locations
        if locations:
            return ", ".join([models.get_location_by_id(location,scenario.model).label for location in locations])
        else:
            return "-"
    
    @read
    async def actor_concrete_atomic_tasks(self, scenario_id: ScenarioID, actor_id: ActorID) -> list[TaskID]:
        models = self.executions[scenario_id].models
        return collect_actor_concrete_atomic_tasks(models, actor_id)
    
    @read
    async def actor_concrete_task_definitions(self, scenario_id: ScenarioID, actor_id: ActorID) -> list[TaskID]:
        models = self.executions[scenario_id].models
        model_id = self.executions[scenario_id].scenario.model
        return collect_actor_concrete_task_definitions(models, model_id, actor_id)

    @read
    async def actor_location_information_spaces(self, scenario_id: ScenarioID, actor_id: ActorID) -> list[InformationSpaceID]:
        models = self.executions[scenario_id].models
        model_id = self.executions[scenario_id].scenario.model
        return collect_actor_information_spaces(models, model_id, actor_id)

    @read
    async def actor_groups(self, scenario_id: ScenarioID, actor_id: ActorID) -> list[ActorID]:
        models = self.executions[scenario_id].models
        model_id = self.executions[scenario_id].scenario.model
        groups = list_actor_affiliations(models,model_id,actor_id)
        groups.remove(actor_id)
        return groups

    @read
    async def task_information_spaces(self, scenario_id: ScenarioID, task_id: TaskID) -> list[InformationSpaceID]:
        models = self.executions[scenario_id].models
        task = models.get_task_by_id(task_id)
        bindings = resolve_info_space_bindings(models,task)
        return [ifs.node_id for req, ifs in bindings.values() if ifs is not None]

    @read
    async def info_space_type(self, scenario_id, ifs_id) -> RecordType | None:
        models = self.executions[scenario_id].models
        model_id = local_id_from_global_id(ifs_id)[1]
        ifs = models.get_info_space_by_id(ifs_id)
        if ifs.type is None:
            return None
        
        record_type = models.get_record_type_by_id(ifs.type,model_id) 
        return record_type
    
    @read
    async def info_space_content(self, scenario_id: ScenarioID, ifs_id: InformationSpaceID) -> list[Record]:
        models = self.executions[scenario_id].models
        ifs = models.get_info_space_by_id(ifs_id)
        return ifs.records
    
    @read
    async def info_space_graphic_plugin(self, scenario_id: ScenarioID, ifs_id: InformationSpaceID) -> str | None:
        models = self.executions[scenario_id].models
        ifs = models.get_info_space_by_id(ifs_id)
        return ifs.get_attribute("graphic-type")
    
    @read
    async def task_definition_parameter_type(self, scenario_id: ScenarioID, task_id: TaskID) -> RecordType | None:
        models = self.executions[scenario_id].models
        model_id = local_id_from_global_id(task_id)[1]
        task_def = models.get_task_definition_by_id(task_id)
        if task_def.parameter_type is None:
            return None
        
        record_type = models.get_record_type_by_id(task_def.parameter_type,model_id) 
        return record_type

    @write
    async def start_agents(self, scenario_id: ScenarioID) -> None:
        self.executions[scenario_id].start_agents(self.plugin_loader)
    @write
    async def start_agent(self, scenario_id: ScenarioID, actor_id: ActorID) -> None:
        self.executions[scenario_id].start_agent(self.plugin_loader,actor_id)
    @write
    async def stop_agents(self, scenario_id: ScenarioID) -> None:
        self.executions[scenario_id].stop_agents()
    @write
    async def stop_agent(self, scenario_id: ScenarioID, actor_id: ActorID) -> None:
        self.executions[scenario_id].stop_agent(actor_id)

    @write
    async def step_timer(self, scenario_id: ScenarioID) -> None:
        self.executions[scenario_id].step_time()
        
    @write
    async def start_timer(self, scenario_id: ScenarioID) -> None:
        if scenario_id not in self.timers:
            self.timers[scenario_id] = asyncio.create_task(self._tick_timer(scenario_id))
    
    async def _tick_timer(self, scenario_id: ScenarioID) -> None:
        while True:
            self.executions[scenario_id].step_time()
            self._method = '_tick_timer'
            self.notify()
            await asyncio.sleep(1)
            
    @write
    async def stop_timer(self, scenario_id: ScenarioID) -> None:
        if scenario_id in self.timers:
            timer = self.timers.pop(scenario_id)
            timer.cancel()

    @write
    async def emit_information_event(self, scenario_id: ScenarioID, actor_id: ActorID, ifs_id: InformationSpaceID, fields: dict[str,Any], task_id: TaskID | None = None):
        execution = self.executions[scenario_id]
        event = ScenarioInformationEvent(
            time=execution.time, actor = actor_id, information_space = ifs_id,
            fields = deepcopy(fields),task = task_id
        )
        execution.apply_event(event)

    @write
    async def emit_task_initiate_event(self,  scenario_id: ScenarioID, actor_id: ActorID, task_id: TaskID, parameter: dict[str,Any], trigger: InformationSpaceID | None = None, for_actor: ActorID | None = None):
        execution = self.executions[scenario_id]
        event = ScenarioTaskInitiateEvent(
            time = execution.time, actor = actor_id, task_definition = task_id, parameter = parameter, trigger = trigger, for_actor = for_actor
        )
        execution.apply_event(event)
    
    @write
    async def emit_move_event(self, scenario_id: ScenarioID, actor_id: ActorID, location_id: LocationID):
        execution = self.executions[scenario_id]
        event = ScenarioMoveEvent(execution.time, actor_id, location_id)
        execution.apply_event(event)

    @write
    async def emit_join_group_event(self, scenario_id: ScenarioID, actor_id: ActorID, group_id: ActorID):
        execution = self.executions[scenario_id]
        event = ScenarioJoinGroupEvent(execution.time, actor_id, group_id)
        execution.apply_event(event)
    
    @write
    async def emit_leave_group_event(self, scenario_id: ScenarioID, actor_id: ActorID, group_id: ActorID):
        execution = self.executions[scenario_id]
        event = ScenarioLeaveGroupEvent(execution.time, actor_id, group_id)
        execution.apply_event(event)

    async def end(self):
        for timer in self.timers.values():
            timer.cancel()
        return await super().end()

