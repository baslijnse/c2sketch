
from c2sketch.models import *
from ..data import ModelStore
from ..patterns import choose_task, view_with_edit
from ..ui import view_title, record_editor, map_ui, after_dialog, TableChoice, TableSelect, SourceView
from toppyt import view_information, enter_information, update_information, all_tasks
from toppyt import write_information, with_information, ParallelTask
from toppyt import constant, any_task, forever, map_value, after_value, MappedEditor
from toppyt.bulma import BulmaRecordEditor, BulmaTextInput, BulmaTextArea, BulmaButtons
from typing import Any

__all__ = ('edit_task_instance',)

def edit_task_instance(model_store: ModelStore, model_id, task_id, path_var):
    #Edit parameters
    #Edit info space bindings
    def choose(general):
        if general is None:
            return view_information(f'Task instance "{task_id}" does not exist.')
        return all_tasks (
            view_title(general['name']),
            choose_task([
            ('Attributes','file-alt', edit_instance_attributes(model_store, model_id, task_id)),
            ('Information Requirements','sign-in-alt', edit_instance_information(model_store, model_id, task_id)),
            ('Constraints','ban',edit_instance_contraints(model_store, model_id, task_id)),
            ('Source','code', view_task_instance_source(model_store,model_id,task_id)),
            ],'Attributes')
        )
    return with_information(model_store,lambda ps:ps.task_instance_general(model_id, task_id),choose,refresh=False)

def edit_instance_attributes(model_store: ModelStore, model_id, task_id):
    
    def view_general():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled= True)),
            ('title',BulmaTextInput(label = 'Title', disabled = True)),
            ('description',BulmaTextArea(label= 'Description', disabled = True))
        ])
        return view_information(model_store, lambda ps:ps.task_instance_general(model_id,task_id), editor=editor)

    def edit_parameter(parameter_type):
        def view():
            editor = record_editor(parameter_type,disabled=True)          
            return view_information(model_store, lambda ps:ps.task_instance_parameter(model_id, task_id), editor=editor)
        
        def edit(fields):
            editor = record_editor(parameter_type)         
            return update_information(fields, editor=editor)
    
        def save(updated_fields):
            return write_information(model_store,lambda ps:ps.task_instance_parameter(model_id, task_id, updated_fields))

        return view_with_edit(view(),edit,save)

    return ParallelTask([
        with_information(model_store,lambda ps:ps.task_instance_parameter_type(model_id, task_id), edit_parameter, refresh=False),
        view_general() 
    ])
    
def edit_instance_information(model_store: ModelStore, model_id, instance_id):

    def edit_info_space_bindings():
        
        def view():
            def view_bindings(requirements: list[InformationSpaceRequirement],bindings):
                headers = ['Requirement','Binding']
                options: list[tuple[Any, list[str], list[str]]] = [
                    (None,[r.name, bindings.get(r.name,'-')],[])
                    for r in requirements]
                return view_information(None,editor=TableChoice(options,headers))
            
            return with_information(model_store,lambda ps:ps.task_instance_info_space_requirements(model_id,instance_id),lambda requirements:
                with_information(model_store,lambda ps:ps.task_instance_info_space_bindings(model_id,instance_id),lambda bindings:
                view_bindings(requirements,bindings)))

        def edit(value):

            def edit_bindings(requirements: list[InformationSpaceRequirement],scope, bindings):
                rows = []
                value = []
                n = 0
                for req in requirements:
                    rows.append([req.name])
                    value.append(bindings.get(req.name))
                    n += 1
                headers = ['Requirement','Binding']
                return update_information(value,editor=TableSelect(rows,headers,scope))
    
            return with_information(model_store,lambda ps:ps.task_instance_template_id(model_id,instance_id),lambda task_id:
                with_information(model_store,lambda ps:ps.task_info_space_requirements(model_id,task_id),lambda requirements:
                with_information(model_store,lambda ps:ps.task_info_spaces_in_scope(model_id,task_id), lambda scope:           
                with_information(model_store,lambda ps:ps.task_instance_info_space_bindings(model_id,instance_id), lambda bindings:
                edit_bindings(requirements,scope,bindings)))),refresh=False)

        def save(update):
            def save_bindings(requirements: list[InformationSpaceRequirement], update):
                bindings = {}
                for req in requirements:
                    binding = update.pop()
                    if not binding is None:
                        bindings[req.name] = binding
                return write_information(model_store,lambda ps:ps.task_instance_info_space_bindings(model_id,instance_id,bindings))
        
            return with_information(model_store,lambda ps:ps.task_instance_template_id(model_id,instance_id),lambda task_id:
                with_information(model_store,lambda ps:ps.task_info_space_requirements(model_id,task_id),lambda requirements:
                save_bindings(requirements,update)),refresh=False)

        return view_with_edit(view(), edit, save)
        
    def edit_information_content():

        def edit_initial_content(ifs_name, ifs_id):

            def edit_messages(type):
                if type is None:
                    return view_information('Configure a type first')
            
                def edit(messages):
                    return forever([after_value(choose_action(messages),lambda r: do_action(messages,*r))])
            
                def choose_action(messages):
                    headers = [field.name for field in type.fields]
                    def row(m):
                        return [m[field.name] if field.name in m else '-' for field in type.fields]

                    options = [(n,row(m),[('edit','Edit','edit'),('delete','Delete','trash')]) for (n,m) in enumerate(messages)]
                    actions = [('add','Add message','plus')]
                    return any_task(
                        enter_information(editor=TableChoice(options,headers)),
                        map_value(enter_information(editor=BulmaButtons(actions,align_right=True)),lambda c: (None,c))
                    )
                
                def do_action(messages, subject, action):
                    if action == 'add':
                        return after_dialog('Add message..',
                            lambda task, validate: edit_message(None, task, validate),
                            lambda validate, task: constant(dict()),
                            [('cancel','Cancel','ban',lambda _: constant(None))
                            ,('add','Add','plus', lambda v: add_message(messages,v))
                            ])
                    if action == 'edit':
                        message_no = int(subject)
                        return after_dialog('Edit message..',
                            lambda task, validate: edit_message(messages[message_no], task, validate),
                            lambda validate, task: constant(dict()),
                            [('cancel','Cancel','ban',lambda _: constant(None))
                            ,('add','Save','save', lambda v: update_message(messages,message_no,v))
                            ])
                    if action == 'delete':
                        message_no = int(subject)
                        return delete_message(messages,message_no)

                    return constant(None)
                
                def edit_message(default, value, validate):
                    editor_value = value
                    if validate is None and default is not None:
                        editor_value = default

                    return update_information(editor_value,editor=record_editor(type))

                def add_message(messages, message):
                    return write_information(model_store,lambda ps:ps.task_instance_info_space_content(model_id, instance_id, ifs_name, messages + [message]))
            
                def update_message(messages, message_no, updated_message):
                    messages = [updated_message if i == message_no else message for i, message in enumerate(messages)]
                    return write_information(model_store,lambda ps:ps.task_instance_info_space_content(model_id, instance_id, ifs_name, messages))
                
                def delete_message(messages, message_no):
                    messages = [message for i, message in enumerate(messages) if i != message_no]
                    return write_information(model_store,lambda ps:ps.task_instance_info_space_content(model_id, instance_id, ifs_name, messages))

                return with_information(model_store,lambda ps:ps.task_instance_info_space_content(model_id, instance_id, ifs_name), edit)

            def layout(ui):
                return f'''
                <div class="content">
                <h3>Additional initial content for information space '{ifs_name}'</h3>
                </div>
                {ui}
                '''

            return map_ui(layout,with_information(model_store,lambda ps:ps.info_space_type(model_id,ifs_id,True),edit_messages))
    

        return with_information(model_store,lambda ps:ps.read_task_instance_template_id(model_id,instance_id),lambda task_id:
            with_information(model_store,lambda ps:ps.read_task_info_spaces(model_id,task_id),lambda ifss:                      
            all_tasks(*[edit_initial_content(ifs.name, ifs.get_id()) for ifs in ifss])),refresh=False)

    
    def layout(parts):
        return f'''
        <div class="content">
        <h2 class="is-size-4"><i class="fas fa-link"></i> Information space bindings</h2>
        <p>Define how the information space requirements are bound to information spaces defined in the parent context.</p>
        </div>
        {parts['info_space_bindings']}
        <div class="content">
        <h2 class="is-size-4"><i class="fas fa-chalkboard"></i> Local information space content</h2>
        <p>The following additional information </p>
        </div>
        {parts['info_space_content']}
        '''
    
    return ParallelTask([
        ('info_space_bindings',edit_info_space_bindings()),
        ('info_space_content',edit_information_content())
    ],layout = layout)


def edit_instance_contraints(planstore: ModelStore, plan_id, instance_id):
    
    def view():
        def view_for_actor(for_actor):
            headers = ['Actor']
            options = [(None,['-' if for_actor is None else for_actor],[])]
            return view_information(None,editor=TableChoice(options,headers))
        
        return with_information(planstore,lambda ps:ps.read_task_instance_for_actor(plan_id, instance_id),lambda for_actor:
                    view_for_actor(for_actor))
        
    def edit(value):
        def edit_for_actor(options, for_actor):
            headers = ['Actor']
            rows = [[]]
            return update_information([for_actor],editor=TableSelect(rows,headers,options))

        return with_information(planstore,lambda ps:ps.read_list_actor_id_options(plan_id),lambda options: 
            with_information(planstore,lambda ps:ps.read_task_instance_for_actor(plan_id, instance_id),lambda for_actor:
                edit_for_actor(options,for_actor)))
    
    def save(value):
        return write_information(planstore,lambda ps:ps.read_task_instance_for_actor(plan_id,instance_id,value[0]))
    
    def layout(ui):
        return f'''
        <div class="content">
        <h2 class="is-size-4">Constrain to actor(s)</h2>
        <p>If specified, only the specified actor or its affiliated actors can perform this task.</p>
        </div>
        <div class="content">
        {ui}
        </div>
        '''
    
    return map_ui(layout,view_with_edit(view(), edit, save))

def view_task_instance_source(model_store: ModelStore, model_id: ModelID, task_id: TaskID):
    

    def view():
        return view_information(model_store,lambda ms: ms.task_instance_source(model_id,task_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_task_instance_source(model_id,task_id,value))
        
    return view_with_edit(view(),edit,save)