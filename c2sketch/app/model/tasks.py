
from toppyt import (ParallelTask, TaskResult, TaskStatus, after_task,
                    after_value, all_tasks, any_task, constant, enter_information,
                    fail, forever, map_result, map_value,
                    read_information, right_task, enter_information, update_information,
                    view_information, with_information, with_source,
                    write_information)
from toppyt.bulma import BulmaButtons

from c2sketch.models import *
from ..patterns import choose_task, view_with_edit
from ..ui import *
from ..data import ModelStore

from .record_types import edit_local_type
from copy import copy, deepcopy
from functools import partial
from typing import Any

__all__ = ('edit_task',)
def edit_task(model_store: ModelStore, plugins, model_id, task_id, path_var):
    
    def choose(general):
        if general is None:
            return view_information(f'Task "{task_id}" does not exist.')
        return all_tasks (
            view_title(general['name']),
            choose_task([
            ('Attributes','file-alt', edit_task_general(model_store, model_id, task_id)),
            ('Information Requirements','sign-in-alt', edit_task_information(model_store, model_id, task_id, path_var)),
            ('Constraints','ban',edit_task_contraints(model_store, model_id, task_id)),
            ('Source','code', view_task_source(model_store,model_id,task_id)),
            ],'Attributes')
        )
    return with_information(model_store,lambda ps:ps.task_attributes(model_id,task_id),choose,refresh=False)

def active(task):
    return map_result(task,lambda r: TaskResult(r.value,TaskStatus.ACTIVE))

def edit_task_general(model_store: ModelStore, model_id: ModelID, task_id: TaskID):
    def view():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled= True)),
            ('title',BulmaTextInput(label = 'Title', disabled = True)),
            ('description',BulmaTextArea(label= 'Description', disabled = True))
        ])
        return view_information(model_store,lambda ps:ps.task_attributes(model_id, task_id), editor=editor)
    
    def edit(fields):
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label = 'Title')),
            ('description',BulmaTextArea(label = 'Description'))
        ])
        return update_information(fields, editor=editor)
    
    def save(updated_fields):
        return write_information(model_store,lambda ps:ps.write_task_general(model_id,task_id,updated_fields))

    return view_with_edit(view(),edit,save)

def edit_task_information(planstore: ModelStore, plan_id: ModelID, task_id: TaskID, path_var):

    def edit_info_space_requirements():

        def choose_action(ifs_reqs):
            
            headers = ['Name','Type','Read','Write']
            options = list()
            for req in ifs_reqs:
                cols = [req.name,'-' if req.type is None else req.type
                    ,'yes' if req.read else 'no','yes' if req.write else 'no']
                options.append((req.name,cols,[('edit','Edit','edit'),('delete','Delete','trash')]))
            
            actions = [('add','Add requirement','plus')]
            return any_task(
                enter_information(editor=TableChoice(options,headers=headers)),
                map_value( enter_information(editor=BulmaButtons(actions,align_right=True)),lambda c: (None,c))
            )
       

        def do_action(ifs_reqs, subject, action):
            if action == 'add':
                return with_information(
                    planstore,lambda ps:ps.read_task_info_spaces_in_scope(plan_id,task_id),
                    lambda ifs_in_scope: after_dialog('Add information space requirement..',
                        lambda task, validate: edit_info_space_requirement(ifs_in_scope,None,task,validate),
                        lambda validate, task: validate_info_space_requirement(task),
                    [('cancel','Cancel','ban',lambda _: constant(None))
                    ,('add','Add','plus', lambda v: update_info_space_requirements(None,ifs_reqs,v))
                    ]),refresh=False)
            elif action == 'edit':
                match = [s for s in ifs_reqs if s.name == subject]
                default_req = None if not match else match[0]
                return with_information(
                    planstore,lambda ps:ps.read_task_info_spaces_in_scope(plan_id,task_id),
                    lambda ifs_in_scope: after_dialog('Edit information space requirement..',
                        lambda task, validate: edit_info_space_requirement(ifs_in_scope,default_req,task,validate),
                        lambda validate, task: validate_info_space_requirement(task),
                    [('cancel','Cancel','ban',lambda _: constant(None))
                    ,('save','Save','save', lambda v: update_info_space_requirements(subject,ifs_reqs,v))
                    ]),refresh=False)
            elif action == 'delete':
                ifs_reqs = [s for s in ifs_reqs if s.name != subject]
                return active(write_information(planstore,lambda ps:ps.write_task_info_space_requirements(plan_id,task_id,ifs_reqs)))
            return fail('Unknown action')

        def edit_info_space_requirement(ops_in_scope, default_req, value, validate):
            value = dict() if value is None else value

            def edit_name():
                default_name = None if default_req is None else default_req.name
                value_name = value['name'] if 'name' in value else default_name
                editor = BulmaTextInput(label='Name')
                return update_information(value_name,editor=editor)
            def edit_type():
                default_type = None if default_req is None else default_req.type
                value_type = value['type'] if 'type' in value else default_type
                return with_source(value_type, lambda type_source:
                    right_task(
                        edit_local_type(planstore, plan_id, type_source,lambda v: write_information(type_source,v)),
                        read_information(type_source,lambda ts,v:ts.read(v),refresh=True)
                    )
                )

            def edit_readwrite():
                default_readwrite = 'readwrite'
                if default_req is not None:
                    if default_req.read and default_req.write:
                        default_readwrite = 'readwrite'
                    elif default_req.read:
                        default_readwrite = 'read'
                    elif default_req.write:
                        default_readwrite = 'write'
                value_readwrite = value['readwrite'] if 'readwrite' in value else default_readwrite
                options = [('Read only','read'),('Write only','write'),('Read and Write','readwrite')]
                editor = BulmaSelect(options,label='Read/write',allow_empty=False)
                return update_information(value_readwrite,editor=editor)
          
            def result(parts):
                res = dict()
                for i, field in enumerate(['name','type','readwrite']):
                    res[field] = parts[i].value
                return TaskResult(res,TaskStatus.ACTIVE)

            return ParallelTask([
                edit_name(),
                edit_type(),
                edit_readwrite()
            ],result_builder=result)

        def validate_info_space_requirement(value):
            return constant(dict())
       
        def update_info_space_requirements(req_id, ifs_reqs, value):
            #TODO description
            read = value['readwrite'] in {'read','readwrite'}
            write = value['readwrite'] in {'write','readwrite'}
            name = value['name']

            req = InformationSpaceRequirement(name,None,value['type'],read,write)
            if req_id is None:
                ifs_reqs = ifs_reqs + [req]
            else:
                ifs_reqs = [req if r.name == req_id else r for r in ifs_reqs]
            
            return write_information(planstore,lambda ps:ps.write_task_info_space_requirements(plan_id,task_id,ifs_reqs))

        return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id),
            lambda ifs_reqs: 
                forever(after_value(choose_action(ifs_reqs),lambda r: do_action(ifs_reqs,*r)))
        )
    
    def edit_info_space_bindings():

        def view(is_parametric):

            def view_task_bindings(requirements: list[InformationSpaceRequirement],bindings):
                headers = ['Requirement','Binding']
                options: list[tuple[Any,list[str],list[str]]] = [
                    (None,[r.name, bindings.get(r.name,'-')],[])
                    for r in requirements]
                return view_information(None,editor=TableChoice(options,headers))
            
            def view_instances_bindings(requirements: list[InformationSpaceRequirement], instances: list[TaskInstance], bindings):
                headers = ['Requirement','Task instance','Binding']
                options: list[tuple[Any, list[str], list[str]]] = [
                    (None,[r.name,i.name,i.info_space_bindings.get(r.name,'-')],[])
                    for r in requirements for i in instances]
                return view_information(None,editor=TableChoice(options,headers))
    
            if is_parametric:        
                return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id), lambda requirements:
                    with_information(planstore,lambda ps:ps.read_task_instances(plan_id,task_id), lambda instances:
                    with_information(planstore,lambda ps:ps.read_task_instances_info_space_bindings(plan_id,task_id), lambda bindings:
                    view_instances_bindings(requirements,instances,bindings))))
            else:
                return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id),lambda requirements:
                    with_information(planstore,lambda ps:ps.read_task_info_space_bindings(plan_id,task_id),lambda bindings:
                    view_task_bindings(requirements,bindings)))

        def edit(is_parametric,_):

            def edit_bindings(requirements: list[InformationSpaceRequirement],scope, bindings):
                rows = []
                value = []
                n = 0
                for req in requirements:
                    rows.append([req.name])
                    value.append(bindings.get(req.name))
                    n += 1
                headers = ['Requirement','Binding']
                return update_information(value,editor=TableSelect(rows,headers,scope))
            
            def edit_instances_bindings(requirements: list[InformationSpaceRequirement], instances: list[TaskInstance], scope, bindings):
                rows = []
                value = []
                n = 0
                for req in requirements:
                    for instance in instances:
                        rows.append([req.name,instance.name])
                        value.append(bindings[instance.name].get(req.name))
                        n += 1
         
                headers = ['Requirement','Task instance','Binding']
                return update_information(value,editor=TableSelect(rows,headers,scope))
            
            if is_parametric:
                return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id),lambda requirements:
                    with_information(planstore,lambda ps:ps.read_task_instances(plan_id,task_id), lambda instances:
                    with_information(planstore,lambda ps:ps.read_task_info_spaces_in_scope(plan_id,task_id), lambda scope:           
                    with_information(planstore,lambda ps:ps.read_task_instances_info_space_bindings(plan_id,task_id), lambda bindings:
                    edit_instances_bindings(requirements,instances,scope,bindings)))))
            else:
                return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id),lambda requirements:
                    with_information(planstore,lambda ps:ps.read_task_info_spaces_in_scope(plan_id,task_id), lambda scope:           
                    with_information(planstore,lambda ps:ps.read_task_info_space_bindings(plan_id,task_id), lambda bindings:
                    edit_bindings(requirements,scope,bindings))))

        def save(is_parametric,update):

            def save_bindings(requirements: list[InformationSpaceRequirement], update):
                bindings = {}
                for req in requirements:
                    binding = update.pop()
                    if not binding is None:
                        bindings[req.name] = binding
                return write_information(planstore,lambda ps:ps.write_task_info_space_bindings(plan_id,task_id,bindings))
            
            def save_instances_bindings(requirements: list[InformationSpaceRequirement], instances: list[TaskInstance], update):

                bindings : dict[str,dict[str,str]]= {}
                for req in requirements:
                    for instance in instances:
                        if not instance.name in bindings:
                            bindings[instance.name] = {}
                        binding = update.pop(0)
                        if not binding is None:
                            bindings[instance.name][req.name] = binding
                    
                return write_information(planstore,lambda ps:ps.write_task_instances_info_space_bindings(plan_id,task_id,bindings))
            
            if is_parametric:
                return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id),lambda requirements:
                    with_information(planstore,lambda ps:ps.read_task_instances(plan_id,task_id), lambda instances:
                    save_instances_bindings(requirements,instances,update)))
            else:
                return with_information(planstore,lambda ps:ps.read_task_info_space_requirements(plan_id,task_id),lambda requirements:
                    save_bindings(requirements,update))

        return with_information(planstore,lambda ps:ps.read_task_is_parametric(plan_id,task_id),lambda is_parametric:
            view_with_edit(view(is_parametric), partial(edit,is_parametric),partial(save,is_parametric)))
    
    def layout(parts):
        return f'''
        <div class="content">
        <h2 class="is-size-4"><i class="fas fa-chalkboard"></i> Required information spaces</h2>
        <p>Define what type of information spaces need to be accessible during the execution of this task.</p>
        </div>
        <div class="content mt-2">
        {parts['info_space_requirements']}
        </div>
        <div class="content">
        <h2 class="is-size-4"><i class="fas fa-link"></i> Information space bindings</h2>
        <p>Define how the information space requirements are bound to information spaces defined in the parent context.</p>
        </div>
        {parts['info_space_bindings']}
        '''
    return ParallelTask([
        ('info_space_requirements',edit_info_space_requirements()),
        ('info_space_bindings',edit_info_space_bindings())
    ],layout = layout)


def edit_task_contraints(planstore: ModelStore, plan_id: ModelID, task_id: TaskID):
    
    def view(is_parametric):
        def view_for_actor(for_actor):
            headers = ['Actor']
            options = [(None,['-' if for_actor is None else for_actor],[])]
            return view_information(None,editor=TableChoice(options,headers))
        
        def view_instances_for_actor(instances, for_actors):
            headers = ['Task instance','Actor']
            options = [(None,[i.name,'-' if fa is None else fa],[]) for i, fa in zip(instances,for_actors)]
            return view_information(None,editor=TableChoice(options,headers))

        if is_parametric:
            return with_information(planstore,lambda ps:ps.read_task_instances(plan_id,task_id), lambda instances:
                    with_information(planstore,lambda ps:ps.read_task_instances_for_actor(plan_id, task_id),lambda for_actors:
                    view_instances_for_actor(instances, for_actors)))
        else:
            return with_information(planstore,lambda ps:ps.read_task_for_actor(plan_id, task_id),lambda for_actor:
                    view_for_actor(for_actor))
        
    def edit(is_parametric, value):

        def edit_for_actor(options, for_actor):
           
            headers = ['Actor']
            rows = [[]]
            return update_information([for_actor],editor=TableSelect(rows,headers,options))
        
        def edit_instances_for_actor(options, instances, for_actors):
            
            headers = ['Task instance','Actor']
            rows = [[instance.name] for instance in instances]
            
            return update_information(for_actors,editor=TableSelect(rows,headers,options))
        
        if is_parametric:
            return with_information(planstore,lambda ps:ps.read_list_actor_id_options(plan_id),lambda options: 
                    with_information(planstore,lambda ps:ps.read_actor_affiliationstask_instances(plan_id,task_id), lambda instances:
                    with_information(planstore,lambda ps:ps.read_task_instances_for_actor(plan_id, task_id),lambda for_actors:
                        edit_instances_for_actor(options,instances,for_actors))))

        else:
            return with_information(planstore,lambda ps:ps.read_list_actor_id_options(plan_id),lambda options: 
                    with_information(planstore,lambda ps:ps.read_task_for_actor(plan_id, task_id),lambda for_actor:
                    edit_for_actor(options,for_actor)))
    
    def save(is_parametric, value):
        if is_parametric:
            return write_information(planstore,lambda ps:ps.read_task_instances_for_actor(plan_id,task_id,value))
        else:
            return write_information(planstore,lambda ps:ps.read_task_for_actor(plan_id,task_id,value[0]))
    
    def layout(ui):
        return f'''
        <div class="content">
        <h2 class="is-size-4">Constrain to actor(s)</h2>
        <p>If specified, only the specified actor or its affiliated actors can perform this task.</p>
        </div>
        <div class="content">
        {ui}
        </div>
        '''
    
    return map_ui(layout,with_information(planstore,lambda ps:ps.read_task_is_parametric(plan_id,task_id),lambda is_parametric:
        view_with_edit(view(is_parametric), partial(edit,is_parametric),partial(save,is_parametric))))

def view_task_source(model_store: ModelStore, model_id: ModelID, task_id: TaskID):
    
    def view():
        return view_information(model_store,lambda ms: ms.task_source(model_id,task_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_task_source(model_id,task_id,value))
        
    return view_with_edit(view(),edit,save)