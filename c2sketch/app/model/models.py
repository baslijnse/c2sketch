import asyncio, aiofiles
import os
import tempfile
import pygraphviz as pgv

from toppyt import (ViewEditor, after_value,
                    all_tasks, any_task, enter_information, fail,
                    forever, map_value, constant,
                    update_information, view_information, with_information,
                    with_dependent, with_download, write_information)
from toppyt.bulma import (BulmaRecordEditor, BulmaSelect, BulmaTextArea,
                          BulmaTextInput)

from c2sketch.models import name_from_model_id, ModelID

from ..patterns import choose_task, view_with_edit
from ..ui import ConfirmedDropdown, TableChoice, SourceView, view_title
from ...visualizations import svg_org_chart, svg_task_hierarchy, dot_actor_information_flow
from ..data import ModelStore
from ..config import AppConfig

__all__ = ('edit_plan',)

def edit_plan(model_store, model_id, path_var):
    
    return all_tasks (
        view_title(name_from_model_id(model_id)),
        choose_task([
            ('Attributes','file-alt', edit_attributes(model_store, model_id)),
            ('Imports','arrow-down',edit_imports(model_store, model_id)),
            ('Graph','chart-line', view_model_graph(model_store, model_id)),
            ('Source','code', view_model_source(model_store,model_id))
        ],'Attributes')
    )
    
def edit_attributes(planstore: ModelStore, plan_id):
    def view():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label = 'Title',disabled = True)),
            ('summary',BulmaTextArea(label= 'Summary', disabled = True))
        ])
        return view_information(planstore,lambda ps: ps.read_model_attributes(plan_id), editor=editor)
    
    def edit(fields):
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label = 'Title')),
            ('summary',BulmaTextArea(label= 'Summary'))
        ])
        return update_information(fields, editor=editor)
    
    def save(updated_fields):
        return write_information(planstore,lambda ps:ps.write_model_attributes(plan_id, updated_fields))

    return view_with_edit(view(),edit,save)

def edit_imports(model_store: ModelStore, model_id):
    def choose_action():
        def choose(imports,import_options):
            options = [(iname,[iname],[('delete','Delete','trash')]) for iname in imports]
        
            return any_task(
                enter_information(editor=TableChoice(options)),
                map_value(enter_information(editor=ConfirmedDropdown(sorted(n for (n,_,_) in import_options),'Add import',icon='plus')),lambda c: (c,'add'))
            )

        return with_information(model_store,lambda ps:ps.model_imports(model_id), lambda a:
            with_information(model_store,lambda ps:ps.list_models(), lambda ao: choose(a,ao)))

    def do_action(subject,action):
            if action == 'add':
                return write_information(model_store,lambda ps:ps.write_plan_add_import(model_id,subject))
            elif action == 'delete':
                return write_information(model_store,lambda ps:ps.write_plan_remove_import(model_id,subject))
            return fail('Unknown action')

    return forever([after_value(choose_action(),lambda r: do_action(*r))])

def view_model_graph(model_store: ModelStore, model_id):
    def choose_analysis():
        options = [
            ('Organization chart','org-chart'),
            ('Task decomposition','task-decomposition'),
            ('Information flow graph','flow-graph')
        ]
        editor = BulmaSelect(options,sync=True)
        return enter_information(editor = editor)

    def view_analysis(model, selection):
        if selection == 'task-decomposition':

            editor = ViewEditor(lambda svg: svg)        
            return view_information(svg_task_hierarchy(model_store.model_set, model_id),editor=editor)

        elif selection == 'flow-graph':
            def view_graph(url):
                return view_information(url,editor=ViewEditor(lambda u: f'<img src="{u}" alt="Information flow graph">'))
            
            headers = [(b'Content-Type',b'image/png')]
            async def generate_graph():
                op_flow_dot = dot_actor_information_flow(model_store.model_set, model_id)
                with tempfile.TemporaryDirectory() as workdir:
                    dotfile_path =os.path.join(workdir,'op-flow.dot')
                    pngfile_path =os.path.join(workdir,'op-flow.png')
                    async with aiofiles.open(dotfile_path, mode='w') as f:
                        await f.write(op_flow_dot)

                    dot_graph = pgv.agraph.AGraph()
                    dot_graph.read(dotfile_path)
                    dot_graph.draw(pngfile_path, prog='dot')
                    
                    #Async version which requires explicit executable path instead of pygraphviz
                    #dot = await asyncio.create_subprocess_exec('/opt/homebrew/bin/dot','-Tpng',dotfile_path,'-o',pngfile_path)
                    #await dot.wait()
                    
                    async with aiofiles.open(pngfile_path, mode='rb') as f:
                        png_data = await f.read()
                    return png_data

            return with_download(view_graph,headers,generate_graph)
        elif selection == 'org-chart':
            editor = ViewEditor(lambda svg: svg)        
            return view_information(svg_org_chart(model_store.model_set, model_id),editor=editor)

        return view_information('')
   
    return with_dependent(choose_analysis(),lambda sel: 
        with_information(model_store,lambda ps:ps.read_plan(model_id),lambda plan: view_analysis(plan,sel),refresh=False))

def view_model_source(model_store: ModelStore, model_id):

    def view():
        return view_information(model_store,lambda ms: ms.model_source(model_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_model_source(model_id,value))
        
    return view_with_edit(view(),edit,save)