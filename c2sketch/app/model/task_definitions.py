__all__ = ('edit_task_def',)

from toppyt import ParallelTask, view_information, update_information, all_tasks, with_information, write_information
from toppyt.bulma import BulmaTextInput, BulmaTextArea, BulmaRecordEditor

from ..ui import SourceView, view_title, record_editor
from ..patterns import choose_task, view_with_edit
from ..data import ModelStore
from c2sketch.models import ModelID, TaskID

def edit_task_def(model_store: ModelStore, model_id, task_id, path_var):
    #Edit parameters
    #Edit info space bindings
    def choose(general):
        if general is None:
            return view_information(f'Task instance "{task_id}" does not exist.')
        return all_tasks (
            view_title(general['name']),
            choose_task([
            ('Attributes','file-alt', edit_task_def_attributes(model_store, model_id, task_id)),
            ('Source','code', view_task_def_source(model_store,model_id,task_id)),
            ],'Attributes')
        )
    return with_information(model_store,lambda ps:ps.task_def_attributes(model_id, task_id),choose,refresh=False)
    
def edit_task_def_attributes(model_store: ModelStore, model_id, task_id):
    
    def view_attributes():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled= True)),
            ('title',BulmaTextInput(label = 'Title', disabled = True)),
            ('description',BulmaTextArea(label= 'Description', disabled = True))
        ])
        return view_information(model_store, lambda ps:ps.task_def_attributes(model_id,task_id), editor=editor)

    return ParallelTask([
        view_attributes() 
    ])

def view_task_def_source(model_store: ModelStore, model_id: ModelID, task_id: TaskID):
   
    def view():
        return view_information(model_store,lambda ms: ms.task_def_source(model_id,task_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_task_def_source(model_id,task_id,value))
        
    return view_with_edit(view(),edit,save)