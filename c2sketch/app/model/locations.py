__all__ = ('edit_location',)

from toppyt import view_information, all_tasks, with_information, update_information, write_information
from toppyt.bulma import BulmaTextInput, BulmaTextArea, BulmaRecordEditor
from ..ui import SourceView, view_title
from ..patterns import choose_task, view_with_edit
from ..data import ModelStore
from c2sketch.models import ModelID, LocationID

def edit_location(model_store: ModelStore, model_id, location_id, path_var):
    
    def choose(general):
        if general is None:
            return view_information(f'Location "{location_id}" does not exist.')

        return all_tasks (
            view_title(general['name']),
            choose_task([
                ('Attributes','file-alt', edit_location_attributes(model_store, model_id, location_id)),
                ('Source','code', view_location_source(model_store,model_id,location_id))
            ],'Attributes')
        )
    return with_information(model_store,lambda ps:ps.location_attributes(model_id,location_id),choose,refresh=False)

def edit_location_attributes(model_store, model_id, location_id):
    return view_information(location_id)

def edit_location_attributes(model_store: ModelStore, model_id: ModelID, location_id: LocationID):
    
    def view():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label = 'Title', disabled = True)),
            ('description',BulmaTextArea(label= 'Description', disabled = True))
        ])
        return view_information(model_store,lambda ps: ps.location_attributes(model_id, location_id), editor=editor)
    
    def edit(fields):
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label = 'Title')),
            ('description',BulmaTextArea(label = 'Description'))
        ])
        return update_information(fields, editor=editor)
    
    def save(updated_fields):
        return write_information(model_store,lambda ps:ps.write_location_attributes(model_id, location_id,updated_fields))

    return view_with_edit(view(),edit,save)

def view_location_source(model_store: ModelStore, model_id, location_id):

    def view():
        return view_information(model_store,lambda ms: ms.location_source(model_id,location_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_location_source(model_id,location_id,value))
        
    return view_with_edit(view(),edit,save)