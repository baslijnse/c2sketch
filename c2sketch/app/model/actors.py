
from toppyt import (ParallelTask, ViewEditor, after_value,
                    all_tasks, any_task, constant, fail, forever,
                    map_value, with_information, update_information,
                    enter_information, view_information, write_information)
from toppyt.bulma import (BulmaButtons, BulmaRecordEditor, BulmaTextArea,
                          BulmaTextInput, BulmaSelect)

from c2sketch.models import ModelID, ActorID, ActorType

from ..patterns import choose_task, view_with_edit
from ..ui import (ConfirmedDropdown, TableChoice, SourceView, record_editor, record_view,
                  view_title)

from ..data import ModelStore

__all__ = ('edit_actor',)

def edit_actor(model_store: ModelStore, model_id: ModelID, actor_id: ActorID, path_var):
    
    def choose(general):
        if general is None:
            return view_information(f'Actor "{actor_id}" does not exist.')

        return all_tasks (
            view_title(general['name']),
            choose_task([
                ('Attributes','file-alt', edit_actor_attributes(model_store, model_id, actor_id)),
                ('Relations','sitemap',edit_actor_relations(model_store, model_id, actor_id, path_var)),
                ('Source','code', view_actor_source(model_store,model_id,actor_id))
            ],'Attributes')
        )
    return with_information(model_store,lambda ps:ps.actor_attributes(model_id,actor_id),choose,refresh=False)

def edit_actor_attributes(model_store: ModelStore, model_id: ModelID, actor_id: ActorID):
    
    def view():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('type',BulmaSelect(ActorType.select_options(),label='Type',disabled=True)),
            ('title',BulmaTextInput(label = 'Title', disabled = True)),
            ('description',BulmaTextArea(label= 'Description', disabled = True)) 
        ])
        return view_information(model_store,lambda ps: ps.actor_attributes(model_id, actor_id), editor=editor)
    
    def edit(fields):
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('type',BulmaSelect(ActorType.select_options(),label='Type')),
            ('title',BulmaTextInput(label = 'Title')),
            ('description',BulmaTextArea(label = 'Description'))
        ])
        return update_information(fields, editor=editor)
    
    def save(updated_fields):
        return write_information(model_store,lambda ps:ps.actor_attributes(model_id, actor_id,updated_fields))

    return view_with_edit(view(),edit,save)
    
def edit_actor_relations(model_store: ModelStore, model_id, actor_id, path_var):  
    def edit_affiliations():
        def choose_action():
            def choose(affiliations,affiliation_options):
                options = [(a,[('view',a)],[('delete','Delete','trash')]) for a in affiliations]
            
                return any_task(
                    enter_information(editor=TableChoice(options)),
                    map_value(enter_information(editor=ConfirmedDropdown(sorted(affiliation_options),'Add affiliation',icon='plus')),lambda c: (c,'add'))
                )

            return with_information(model_store, lambda ps:ps.read_actor_affiliations(model_id,actor_id), lambda a:
                with_information(model_store,lambda ps:ps.read_actor_affiliation_options(model_id,actor_id), lambda ao: choose(a,ao)))

        def do_action(subject,action):
            if action == 'view':
                url = f'/model/{model_id}/a/{subject}'
                return write_information(path_var,lambda pv:pv.write(url))
            elif action == 'add':
                return write_information(model_store,lambda ps:ps.write_actor_add_affiliation(model_id,actor_id,subject))
            elif action == 'delete':
                return write_information(model_store,lambda ps:ps.write_actor_remove_affiliation(model_id,actor_id,subject))
            return fail('Unknown action')

        return forever([after_value(choose_action(),lambda r: do_action(*r))])

    def edit_affiliated():
        def choose_action():
            def choose(affiliated, affiliated_options):
                options = [(a.name,[('view',a.name)],[('delete','Delete','trash')]) for a in affiliated]
                return any_task(
                    enter_information(editor=TableChoice(options)),
                    map_value(enter_information(editor=ConfirmedDropdown(sorted(affiliated_options),'Add affiliated',icon='plus')),lambda c: (c,'add'))
                )
            return with_information(model_store,lambda ps:ps.read_actor_affiliated(model_id,actor_id),lambda a:
                with_information(model_store,lambda ps:ps.read_actor_affiliated_options(model_id,actor_id), lambda ao: choose(a,ao)))

        def do_action(subject,action):
            if action == 'view':
                url = f'/model/{model_id}/a/{subject}'
                return write_information(path_var,lambda pv:pv.write(url))
            elif action == 'add':
                return write_information(model_store,lambda ps:ps.write_actor_add_affiliated(model_id,actor_id,subject))
            elif action == 'delete':
                return write_information(model_store,lambda ps:ps.write_actor_remove_affiliated(model_id,actor_id,subject))
            return fail('Unknown action')
        return forever([after_value(choose_action(),lambda r: do_action(*r))])

    def layout(parts):
        return f'''
        <div class="content">
        <h2 class="is-size-4">Affiliations</h2>
        <p>This actor is part of the following organizations:</p>
        </div>
        {parts['affiliations']}
        <div class="content">
        <h2 class="is-size-4">Affiliated actors</h2>
        <p>The following actors are part of this actor/organization:</p>
        </div>
        {parts['affiliated']}  
        '''

    return ParallelTask([
        ('affiliations',edit_affiliations()),
        ('affiliated',edit_affiliated())
    ],layout = layout)

def view_actor_source(model_store: ModelStore, model_id, actor_id):

    def view():
        return view_information(model_store,lambda ms: ms.actor_source(model_id,actor_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_actor_source(model_id,actor_id,value))
        
    return view_with_edit(view(),edit,save)