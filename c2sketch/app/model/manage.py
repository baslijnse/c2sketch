"""Creating and selection models"""

from toppyt import TaskStatus, TaskResult, TaskVariable, ParallelTask, SequenceTask, forever, update_information,\
    constant, with_information, with_uploads, write_information, view_information, sequence_tasks, progress_on_stable
from toppyt.bulma import BulmaTextInput, BulmaSelect, BulmaFileInput

from ..config import AppConfig
from ..data import ModelStore
from ..ui import TreeChoice, after_dialog, header_layout, choose_app_mode
from ..patterns import action_choice

from c2sketch.models import *

from functools import partial
import os

def manage_models(config: AppConfig, model_store: ModelStore, path_var, cookie_jar):
    
    selection = TaskVariable(None)

    def select_model():
        def to_nodes(models):

            nodes = []
            for model_id, model_title, model_description in sorted(models,key=lambda e:e[0]):
                
                node_list = nodes
                segments = model_id.split('.')
                for segment in segments[:-1]:
                    if not node_list or node_list[-1]['name'] != segment:
                        node_list.append({'name': segment,'icon': 'folder','value': None,'children':[]})
                    node_list = node_list[-1]['children']
                
                node_list.append({'name': segments[-1],'icon': 'sitemap','value': model_id,'children':[]})
           
            return nodes

        return with_information(model_store, lambda ps:ps.list_models(),
            lambda models: update_information(selection, editor=TreeChoice(to_nodes(models)))
        )

    def view_details():
        return view_information(selection)
    
    def layout(parts):
        return f'''
        <div class="prepare-grid">
            <div class="prepare-header">{header_layout(config.name, 'Model','','',parts['app_mode'])}</div>
            <div class="prepare-body">
                <div class="prepare-side">
                    <div class="panel-block buttons">
                    {parts['model_actions']}
                    </div>
                    <div class="panel-block">
                    {parts['choose_model']}
                    </div>
                </div>
                <flex-resizer></flex-resizer>
                <div class="prepare-main">
                <div class="container prepare-inner">
                {parts['preview_model']}
                </div>
                </div>
            </div>
        </div>
        '''
    def select_action(model_id):
        actions = [
                ('add','Add...','plus',add_model(model_store,path_var)),
                ('edit','Edit...','edit',edit_model(model_id, path_var)),
                ('delete','Delete...','trash',remove_model(model_store,model_id,path_var))
                ]
        return forever(action_choice(actions))
    
    return ParallelTask(
        [('choose_model',select_model())
        ,('model_actions',with_information(selection,lambda s:s.read(), select_action))
        ,('preview_model',view_details())
        ,('app_mode',choose_app_mode(path_var))
        ],layout=layout,result_builder=lambda rs:rs[-1])

def add_model(model_store: ModelStore, path_var):
   
    def enter(upload_url, task, validate):
        def enter_title(title):
            return update_information(title,editor=BulmaTextInput(label='Title',sync=True))            

        def enter_name(name, title, validate):
            help = None
            if validate is not None and 'name' in validate:
                help = (validate['name'],'danger')
            placeholder = to_safe_name('' if title.value is None else title.value)
            return update_information(name,editor=BulmaTextInput(placeholder=placeholder,help=help,label='Name'))
        
        def enter_parent(plans, parent):
            options = [('None (top-level)','')] + [p[0] for p in plans]
            return update_information(parent, editor=BulmaSelect(options, label='As part of',allow_empty=False))
          
        def enter_content_type(type, validate):
            options = [
                ('Blank plan','blank'),
                ('Upload file','upload'),
                ('Copy existing plan','copy')
            ]
            return update_information(type,editor=BulmaSelect(options,label='Initial content',allow_empty=False,sync=True))
    
        def enter_content_data(plans, upload_url, content_type, data, validate):
            match content_type:
                case 'blank':
                    return constant(None)
                case 'upload':
                    help = None
                    if validate is not None and 'content_data' in validate:
                        help = (validate['content_data'],'danger')
                    return update_information(data, editor=BulmaFileInput(upload_url=upload_url, label='YAML file', text='Choose...', help=help))
                case 'copy':
                    help = None
                    if validate is not None and 'content_data' in validate:
                        help = (validate['content_data'],'danger')
                    return update_information(data, editor=BulmaSelect([p[0] for p in plans], label='Copy of', help=help))

            return constant(None)

        def result(results):
            names = ['title','name','parent','content_type','content_data']
            return TaskResult({k: r.value for k,r in zip(names,results)}, TaskStatus.ACTIVE)
    
        return with_information(model_store, lambda ps: ps._list_models(),lambda plans: ParallelTask([
            ('title',enter_title(None if task is None else task.get('title'))),
            ('name','title',lambda title: enter_name(None if task is None else task.get('name'),title,validate)),
            ('parent',enter_parent(plans, None if task is None else task.get('parent'))),
            ('content_type',enter_content_type(None if task is None else task.get('content_type'),validate)),
            ('content_data','content_type', lambda content_type: enter_content_data(plans, upload_url,content_type.value, None if task is None else task.get('content_data'),validate))
        ],result_builder=result))
    
    def validate(task, action):
        if action == 'cancel':
            return constant(dict())

        def check_input(existing_names):
            errors = {}
            name = None if task is None else task.get('name','')
            parent = task.get('parent','')
            if name == '':
                name = to_safe_name(task.get('title',''))
        
            if not is_safe_name(name):
                errors['name'] = 'Name is not a valid name. It may not contain spaces and cannot start with a number.'
            else:
                new_plan_id = name if parent == '' else f'{parent}.{name}'
                if new_plan_id in [item[0] for item in existing_names]:
                    if parent == '':
                        errors['name'] = f'A plan named \'{name}\' already exists.'
                    else:
                        errors['name'] = f'A plan named \'{name}\' already exists as part of \'{parent}\'.'

            content_type = 'blank' if task is None else task.get('content_type','blank')
            content_data =  None if task is None else task.get('content_data')
            if content_type == 'copy':
                if content_data is None or content_data == '':
                    errors['content_data'] = 'You need to select a plan to copy.'
            elif content_type == 'upload':
                if content_data is None or content_data == '':
                    errors['content_data'] = 'You need to select a file to upload.'
            return constant(errors)
        
        return with_information(model_store, lambda ps: ps._list_models(),check_input,refresh=False)
   
    def save(upload_dir, options):
        title = options.get('title')
        name = options.get('name','')
        if name == '':
            name = to_safe_name('' if title is None else title)
        parent = options.get('parent','')

        content_type = options.get('content_type','blank')
        content_data = options.get('content_data')

        new_plan_id = name if parent == '' else f'{parent}.{name}'
        if content_type == 'copy':
            content = content_data
            action = lambda ps: ps.create_plan(new_plan_id,content,title)
        elif content_type == 'upload':
            content = os.path.join(upload_dir,content_data['id'])
            action = lambda ps: ps.import_plan(new_plan_id,content,title)
        else:
            content = None
            action = lambda ps: ps.create_plan(new_plan_id,content,title)
            
        

        return SequenceTask([
            write_information(model_store, action),
            write_information(path_var, lambda pv: pv.write(f'/{new_plan_id}'))
        ],progress_check=progress_on_stable)

    return with_uploads(lambda url, directory:
        forever(action_choice(
            [('add','Add plan','plus',after_dialog('Add plan...',
                partial(enter,url),
                validate,
                    [('cancel','Cancel','ban',lambda _: constant(None))
                    ,('add','Add','plus',partial(save,directory))
                    ])
                )]
            ,compact_buttons=False)))

def edit_model(model_id: ModelID, path_var: TaskVariable[str]):
    if model_id is None:
        return constant(None)
    return write_information(path_var, lambda pv:pv.write(f'/model/{model_id}'))

def remove_model(model_store: ModelStore, model_id, path_var: TaskVariable[str]):
    
    def enter(*args):
        return view_information(f'Are you sure you want to remove {model_id}? This cannot be undone.')
    
    def validate(task, action):
        return constant(dict())

    def save(options):
        return sequence_tasks(
            write_information(model_store,lambda ps:ps.delete_model(model_id)),
            write_information(path_var,lambda pv:pv.write('/model'))
        )
    
    return after_dialog('Remove...',enter,validate,
        [('cancel','Cancel','ban',lambda _: constant(None))
        ,('delete','Delete','trash',save)
        ])