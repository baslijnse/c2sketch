
from typing import Callable, Any

from toppyt import (after_value, all_tasks, any_task, constant,
                    enter_information, forever,
                    map_value, update_information, view_information,
                    with_information, with_dependent, write_information)
from toppyt.bulma import (BulmaButtons, BulmaRecordEditor, BulmaSelect,
                          BulmaTextInput, BulmaTextArea)

from c2sketch.models import *
from ..data import ModelStore
from ..patterns import choose_task, view_with_edit
from ..ui import TableChoice, SourceView, after_dialog, view_title

__all__ = ('edit_type',)

def edit_type(planstore: ModelStore, plan_id: ModelID, type_name: RecordTypeID, path_var):
    def choose(general):
        if general is None:
            return view_information(f'Type "{type_name}" does not exist.')

        return all_tasks (
            view_title(type_name),
            choose_task([
                ('Fields','puzzle-piece', edit_global_type(planstore, plan_id, type_name)),
                ('Use','exchange-alt', view_type_use(planstore, plan_id, type_name, path_var)),
                ('Source','code', view_record_type_source(planstore,plan_id,type_name)),
            ],'Fields')
        )
    return with_information(planstore,lambda ps:ps.read_type_fields(plan_id,type_name),choose)

def edit_global_type(planstore: ModelStore, plan_id, type_name):
    def write_task(fields):
        return write_information(planstore,lambda ps:ps.write_type_fields(plan_id,type_name,fields))

    return edit_type_fields(planstore, lambda ps:ps.read_type_fields(plan_id,type_name), lambda x: x, write_task)

def edit_local_type(planstore: ModelStore, plan_id: ModelID, read_fun: Callable[[ModelStore],Any], write_task: Callable[[RecordType],Task]):

    def edit(global_types, local_type):
        if isinstance(local_type,RecordType):
            type_choice = '_custom_'
        elif isinstance(local_type,str):
            type_choice = local_type
        else:
            type_choice = None

        options = [('Custom...','_custom_')] + global_types

        def edit_details(choice):
            if choice != type_choice:
                if choice == '_custom_':
                    return write_task(RecordType(None,'_custom_',[]))
                else:
                    return write_task(choice)
                
            if choice == '_custom_':
                def write_fields(fields):
                    return write_task(RecordType(None, '_custom_',fields))
                return edit_type_fields(planstore,read_fun,lambda m: m.fields,write_fields)
            
            return view_information('')

        return with_dependent(update_information(type_choice,editor=BulmaSelect(options,label='Type',sync=True)),edit_details)

    return with_information(planstore, lambda ps:ps.read_list_type_options(plan_id),
        lambda global_types: with_information(planstore,read_fun,lambda t: edit(global_types,t))
    )
  
def edit_type_fields(store: ModelStore, read_fun, read_transform, write_task):

    def edit(fields):
        return forever([after_value(choose_action(fields),lambda r: do_action(fields,*r))])
    
    def choose_action(fields):
        headers = ['Field name','Field type','Optional','Possible values','Description']
        options = [(n,[f.name,f.type,'-','-'],[('edit','Edit','edit'),('delete','Delete','trash')]) for (n,f) in enumerate(fields)]
        actions = [('add','Add field','plus')]
        return any_task(
            enter_information(editor=TableChoice(options,headers)),
            map_value(enter_information(editor=BulmaButtons(actions,align_right=True)),lambda c: (None,c))
        )
    
    def field_to_dict(field:RecordTypeField):
        raw = dict()
        raw['name'] = field.name
        raw['type'] = field.type.value
        raw['optional'] = 'yes' if field.optional else 'no'
        raw['choices'] = '' if field.choices is None else ', '.join(field.choices)
        raw['description'] = '' if field.description is None else field.description
        return raw

    def field_from_dict(raw):
        name = raw.get('name')
        if name is None or name == '':
            name = 'untitled'
        type = raw.get('type','string')
        type = 'string' if type is None else type
        optional = raw.get('optional','no') == 'yes'
        choices = raw.get('choices','')
        if choices == '':
            choices = None
        else:
            choices = [c.strip() for c in choices.split(',')]
        description = raw.get('description')
        if description == '':
            description = None
        return RecordTypeField(name,type,optional,choices,description)
    
    def do_action(fields, subject, action):
        if action == 'add':
            return after_dialog('Add field..',
                lambda task, validate: edit_field(None,task,validate),
                lambda validate, task: constant(dict()),
                [('cancel','Cancel','ban',lambda _: constant(None))
                ,('add','Add','plus', lambda v: add_field(fields,v))
                ])

        if action == 'edit':
            field_no = int(subject)
            return after_dialog('Edit field..',
                lambda task, validate: edit_field(fields[field_no],task,validate),
                lambda validate, task: constant(dict()),
                [('cancel','Cancel','ban',lambda _: constant(None))
                ,('add','Save','save', lambda v: update_field(fields,field_no,v))
                ])
        if action == 'delete':
            field_no = int(subject)
            return delete_field(fields,field_no)

        return constant(None)

    def edit_field(default_field, value, validate):
        editor_value = value

        if default_field is not None and validate is None:
            editor_value = field_to_dict(default_field)

        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label='Field name')),
            ('type',BulmaSelect(['string','integer'],allow_empty=False,label='Field type')),
            ('optional',BulmaSelect(['no','yes'],allow_empty=False,label='Optional')),
            ('choices', BulmaTextInput(label='Choices')),
            ('description',BulmaTextInput(label='Description'))
        ])

        return update_information(editor_value,editor=editor)
    
    def add_field(fields,new_field):
        return write_task(fields + [field_from_dict(new_field)])

    def update_field(fields,field_no,updated_field):
        fields = [field_from_dict(updated_field) if i == field_no else field for i, field in enumerate(fields)]
        return write_task(fields)

    def delete_field(fields,field_no):
        fields = [field for i, field in enumerate(fields) if i != field_no]
        return write_task(fields)

    return with_information(store,read_fun,lambda r: edit(read_transform(r)))

def view_type_use(planstore: ModelStore, plan_id, type_name, path_var):
  
    def do_action(action):
        task_id = action[0]
        return write_information(path_var,lambda pv:pv.write(f'/{plan_id}/prepare/t/{task_id}'))
    
    def view(uses):
        if not uses:
            return view_information('This type is not used anywhere')
        
        options = [(t,[('view-task',task_from_id(t))],[]) for t in uses]
        return after_value(enter_information(editor=TableChoice(options)),do_action)

    return with_information(planstore,lambda ps:ps.read_type_uses(plan_id,type_name),view)

def view_record_type_source(model_store: ModelStore, model_id: ModelID, type_id: RecordTypeID):

    def view():
        return view_information(model_store,lambda ms: ms.record_type_source(model_id,type_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_record_type_source(model_id,type_id,value))
        
    return view_with_edit(view(),edit,save)