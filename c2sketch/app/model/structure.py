"""Structural edits such as creating, renaming and deleting nodes"""

from toppyt import (ParallelTask, all_tasks, constant, map_value, forever,
                    read_information, right_task, update_information,
                    view_information, write_information, with_information)
from toppyt.bulma import (BulmaRecordEditor, BulmaSelect, BulmaTextArea,
                          BulmaTextInput)

from c2sketch.models import *
from ..config import AppConfig
from ..data import ModelStore
from ..ui import *
from ..patterns import action_choice
from .actors import edit_actor
from .locations import edit_location
from .tasks import edit_task
from .task_definitions import edit_task_def
from .task_instances import edit_task_instance
from .information_spaces import edit_info_space
from .record_types import edit_type
from .models import edit_plan

from functools import partial

__all__ = ('edit_model','edit_model_node')

def edit_model(config: AppConfig, model_store: ModelStore, plugins, model_id: ModelID, path_var, cookie_jar):
    return edit_node_in_context(config,model_store,plugins,model_id,path_var,cookie_jar,
                                edit_plan(model_store, model_id, path_var))

def edit_model_node(config: AppConfig, model_store: ModelStore, plugins, model_id: ModelID, node_type, node_id, path_var, cookie_jar):
    if node_type == 'a':
        node_task = edit_actor(model_store, model_id, node_id, path_var)
    elif node_type == 'l':
        node_task = edit_location(model_store, model_id, node_id, path_var)
    elif node_type == 't':
        node_task = edit_task(model_store, plugins, model_id, node_id, path_var)
    elif node_type == 'td':
        node_task = edit_task_def(model_store, model_id, node_id, path_var)
    elif node_type == 'ti':
        node_task = edit_task_instance(model_store, model_id, node_id, path_var)
    elif node_type == 'i':
        node_task = edit_info_space(model_store, plugins, model_id, node_id, path_var)
    elif node_type == 'm':
        node_task = edit_type(model_store, model_id, node_id, path_var)
    return edit_node_in_context(config,model_store,plugins,model_id,path_var,cookie_jar,node_task)

def edit_node_in_context(config: AppConfig, model_store: ModelStore, plugins, model_id: ModelID, path_var, cookie_jar, node_task):
    
    def edit(model: Model):
    
        node_type = None
        node_id = None
    
        actions = [
            ('add','Add...','plus',add_node(model_store,model.id,node_type,node_id)),
            ('move','Move...','arrow-right',move_node(model_store,model.id,node_type,node_id,path_var)),
            ('rename','Rename...','i-cursor',rename_node(model_store,model.id,node_type,node_id,path_var)),
            ('delete','Delete...','trash',delete_node(model_store,model.id,node_type,node_id,path_var))
            ]
        
        return ParallelTask(
            [('subtitle',view_model_title(model.id, model.title, path_var))
            ,('choose_node',choose_node(model_store, model.id, path_var))
            ,('node_actions',forever(action_choice(actions)))
            ,('edit_node',node_task)
            ,('app_mode',choose_app_mode(path_var))
            ],layout=partial(edit_model_layout,config.name),result_builder=lambda rs:rs[-1])
    
    return with_information(model_store,lambda ps:ps.read_plan(model_id),edit,refresh=False)

def edit_model_layout(title, parts):
    
    header = header_layout(title=title, actor_location=parts['subtitle'],app_mode=parts['app_mode'])

    return f'''
        <div class="prepare-grid">
            <div class="prepare-header">{header}</div>
            <div class="prepare-body">
                <div class="prepare-side">
                    <div class="panel-block buttons">
                    {parts['node_actions']}
                    </div>
                    <div class="panel-block">
                    {parts['choose_node']}
                    </div>
                </div>
                <flex-resizer></flex-resizer>
                <div class="prepare-main">
                <div class="container prepare-inner">
                {parts['edit_node']}
                </div>
                </div>
            </div>
        </div>
    '''

def choose_node(model_store: ModelStore, model_id, path_var):
    def model_nodes(model: Model | None):
        if model is None:
            return []

        def to_tree_nodes(nodes: list[Node]):
            result = []
            for node in nodes:
                match node:
                    case Actor():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': node.name,'icon': 'user','value': f'/model/{model.id}/a/{node_id}'})
                    case Location():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': node.name,'icon':'map-pin','value':f'/model/{model.id}/l/{node_id}'})
                    case Task():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': node.name, 'icon': 'check','value': f'/model/{model.id}/t/{node_id}','children': to_tree_nodes(node.nodes)})
                    case TaskDefinition():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': node.name, 'icon': 'check-double','value': f'/model/{model.id}/td/{node_id}','children': to_tree_nodes(node.nodes)})
                    case TaskInstance():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': f'{node.name}-{node.sequence}', 'icon': 'check','value': f'/model/{model.id}/ti/{node_id}','children': to_tree_nodes(node.nodes)})
                    case InformationSpace():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': node.name, 'icon': 'chalkboard','value': f'/model/{model.id}/i/{node_id}'})
                    case RecordType():
                        node_id = local_id_from_global_id(node.node_id)[0]
                        result.append({'name': node.name, 'icon':'puzzle-piece','value': f'/model/{model.id}/m/{node_id}'})
            return result
               
        return [{'name':model.id,'icon':'sitemap'
                 ,'value': f'/model/{model.id}'
                 ,'children': to_tree_nodes(model.nodes)}]
    
    return with_information(model_store, lambda ms:ms.model(model_id),
        lambda model: update_information(path_var, editor=TreeChoice(model_nodes(model)))
    )


def parent_options(plan:Model) -> list[tuple[str,str]]:
    options: list[tuple[str,str]] = list()
    
    def add_task(prefix: list[str], task: Task,options):
        path = prefix + [task.name]
        options.append(('.'.join(path),task.get_id()))
        for part in task.tasks:
            add_task(path,part,options)

    for task in plan.tasks:
        add_task([],task,options)
    
    return options


#Pure check for node names
def validate_name(errors, name):
    if name is None or name == '':
        errors['name'] = 'Name cannot be empty.'
    elif not is_safe_name(name):
        errors['name'] = 'Name may only contain alphanumeric characters or underscores.' 
    return errors

#Impure uniqueness checks need the plan store
def validate_unique_actor(planstore, plan_id, errors, name):
    def check(exists):
        if exists:
            errors['name'] = f'An actor named \'{name}\' already exists.'
        return errors
    return map_value(read_information(planstore,lambda ps: ps.actor_exists(plan_id, name)),check)

def validate_unique_task(planstore, plan_id, errors, name, parent):
    def check(exists):
        if exists:
            errors['name'] = f'A task named \'{name}\' already exists as part of \'{parent}\'.'
        return errors
    task_id = name if parent is None else f'{parent}.{name}'
    return map_value(read_information(planstore,lambda ps:ps.task_exists(plan_id, task_id)),check)

def validate_unique_info_space(planstore, plan_id, errors, name, parent):
    def check(exists):
        if exists:
            errors['name'] = f'A picture named \'{name}\' already exists as part of \'{parent}\'.'
        return errors
    return map_value(read_information(planstore,lambda ps:ps.info_space_exists(plan_id,f'{parent}.{name}')),check)

def validate_unique_type(planstore, plan_id, errors, name):
    def check(exists):
        if exists:
            errors['name'] = f'A message type named \'{name}\' already exists.'
        return errors
    return map_value(read_information(planstore,lambda ps:ps.type_exists(plan_id, name)),check)

def add_node(planstore, plan_id, node_type, node_id, from_parent=False, suggested_type = None):
   
    def enter(task, validate):
        return with_information(planstore,lambda ps:ps.plan(plan_id),
            lambda plan: enter_options(plan,task,validate),refresh = False)
 
    def enter_options(plan, value, validate):
        type_value, detail_value = (None, None) if value is None else value

        def choose_type(suggested_type):
            suggestions = {'a':'Task','t':'Task','i':'Information Space','m':'Message Type'}
            
            help = None
            if validate is not None and 'type' in validate:
                help = (validate['type'],'danger')

            if value is None:
                if suggested_type is not None:
                    editor_value = suggested_type
                else:
                    editor_value = 'Task' if node_type is None else suggestions[node_type]
            else:
                editor_value = type_value
      
            editor = BulmaSelect(['Actor','Task','Information Space','Message Type'], disabled = from_parent, sync = True,help=help)
            return update_information(editor_value,editor=editor)
        
        def enter_details(type):
            
            help = dict()
            for field_name in ['name','title','description','parent']:
                if validate is not None and field_name in validate:
                    help[field_name] = (validate[field_name],'danger')
                else:
                    help[field_name] = None

            if type == 'Actor':
                actor_type_options = ActorType.select_options()

                fields = [
                    ('name',BulmaTextInput(label='Name',help=help['name'])),
                    ('title',BulmaTextInput(label='Title',help=help['description'])),
                    ('description',BulmaTextArea(label='Description',help=help['description'])),
                    ('type',BulmaSelect(actor_type_options,label='Type'))
                ]
                if detail_value is None:
                    editor_value = {'name':None,'title':None,'description':None}
                else:
                    editor_value = detail_value
            elif type == 'Task':
                parent_nodes = parent_options(plan)
                fields = [
                    ('name',BulmaTextInput(label='Name',help=help['name'])),
                    ('title',BulmaTextInput(label='Title',help=help['description'])),
                    ('description',BulmaTextArea(label='Description',help=help['description'])),
                    ('parent',BulmaSelect(parent_nodes,label='As part of', disabled = from_parent,help=help['parent']))
                ]
                if detail_value is None:
                    suggested_parent = None
                    if node_type == 't' and node_id is not None:
                        suggested_parent = node_id
                    elif node_type == 'i' and node_id is not None:
                        suggested_parent = parent_from_id(node_id)
                    editor_value = {'name':None,'title':None,'description':None,'parent':suggested_parent}
                else:
                    editor_value = detail_value
            elif type == 'Information Space':
                parent_nodes = parent_options(plan)
                fields = [
                    ('name',BulmaTextInput(label='Name',help=help['name'])),
                    ('title',BulmaTextInput(label='Title',help=help['title'])),
                    ('description',BulmaTextArea(label='Description',help=help['description'])),
                    ('parent',BulmaSelect(parent_nodes,label='As part of', disabled = from_parent,help=help['parent']))
                ]
                if detail_value is None:
                    suggested_parent = None
                    if node_type == 't' and node_id is not None:
                        suggested_parent = node_id
                    elif node_type == 'i' and node_id is not None:
                        suggested_parent = parent_from_id(node_id)
                    editor_value = {'name':None,'title':None,'description':None,'parent':suggested_parent}
                else:
                    editor_value = detail_value
            elif type == 'Message Type':
                fields = [('name',BulmaTextInput(label='Name',help=help['name']))]
                if detail_value is None:
                    editor_value = {'name':None}
                else:
                    editor_value = detail_value
            else:
                fields = list()
                editor_value = None
            return update_information(editor_value, editor=BulmaRecordEditor(fields))
            
        return ParallelTask([
            ('type',choose_type(suggested_type)),
            ('details','type',lambda type: enter_details(type.value))
        ],result_builder=lambda rs: TaskResult([r.value for r in rs], TaskStatus.ACTIVE))

 
    def validate(task, action):
        if action == 'cancel':
            return constant(dict())
        errors = dict()

        if task is None:
            node_type, options = None, None
        else: 
            node_type, options = task

        #First check all pure properties
        if node_type is None:
            errors['type'] = 'Node type cannot be empty.'
        else:
            if options is not None:
                validate_name(errors, options['name']) 
    
        #If name is ok, check uniqueness of the new node
        if not 'name' in errors and not 'parent' in errors:
            if node_type == 'Actor':
                return with_action_check(action,validate_unique_actor(planstore, plan_id, errors,options['name']))
            if node_type == 'Task':
                return with_action_check(action,validate_unique_task(planstore, plan_id, errors,options['name'],options['parent']))
            if node_type == 'Information Space':
                return with_action_check(action,validate_unique_info_space(planstore, plan_id, errors,options['name'],options['parent']))
            if node_type == 'Message Type':
                return with_action_check(action,validate_unique_type(planstore, plan_id, errors,options['name']))
        
        return with_action_check(action,constant(errors))
   
    def save(options):
        if options[0] == 'Actor' and options[1]['name'] is not None:
            return write_information(planstore,lambda ps:ps.create_actor(plan_id,options[1]['name'],options[1]['title'],options[1]['description'],options[1]['type']))
        if options[0] == 'Task' and options[1]['name'] is not None:
            return write_information(planstore,lambda ps:ps.create_task(plan_id,options[1]['parent'],options[1]['name'],options[1]['title'],options[1]['description']))
        if options[0] == 'Information Space' and options[1]['name'] is not None:
            return write_information(planstore,lambda ps:ps.create_info_space(plan_id,options[1]['parent'],options[1]['name'],options[1]['title'],options[1]['description']))
        if options[0] == 'Message Type' and options[1] is not None:
            return write_information(planstore,lambda ps:ps.create_type(plan_id,options[1]['name']))
        return constant(None)

    return after_dialog('Add..',
        enter,
        validate,
        [('cancel','Cancel','ban',lambda _: constant(None))
        ,('add','Add','plus',save)
        ])

def move_node(planstore, plan_id, node_type, node_id, path_var):
    if node_type is None or node_id is None or (not node_type in ['t','i']):
        return constant(None)

    def enter(task,validate):
        return with_information(planstore,lambda ps:ps.plan(plan_id),
            lambda plan: enter_options(plan,task,validate), refresh=False)

    def enter_options(plan, value, validate):
        cur_parent = parent_from_id(node_id)
        # You can't move a task to a sub-node of the moved node
        # Only tasks can be attached directly to actors, ops need to be part of a task.
        parent_nodes = [o for o in parent_options(plan) if not o[1].startswith(node_id) and o[1] != cur_parent]
        help = None
        if validate is not None and 'target' in validate:
            help = (validate['target'],'danger')

        return right_task(
            view_information(cur_parent,editor=BulmaTextInput(label='From',disabled=True)),
            update_information(value,editor=BulmaSelect(parent_nodes, label='To',help=help))
        )

    def validate(task,action):
        if action == 'cancel' or node_type in ['t','i']:
            return constant(dict())

        errors = dict()
        if task is None or task == '':
            errors['target'] = 'You need to specify a destination for the moved node.'
        return with_action_check(action,constant(errors))

    def save(new_parent):
        

        if new_parent is None or new_parent == '':
            new_path = f'/{plan_id}/prepare/{node_type}/{name_from_task_id(node_id)}'
        else:
            new_path = f'/{plan_id}/prepare/{node_type}/{new_parent}.{name_from_task_id(node_id)}'
        
        actions = {'t':lambda ps: ps.move_task(plan_id,node_id,new_parent)
                   ,'i':lambda ps: ps.move_info_space(plan_id,node_id, new_parent)}
        return all_tasks(
            write_information(planstore,actions[node_type]),
            write_information(path_var,lambda pv: pv.write(new_path))
        )
        
    return after_dialog(f'Move {name_from_task_id(node_id)}',
        enter,
        validate,
        [('cancel','Cancel','ban',lambda _: constant(None))
        ,('add','Move','arrow-right',save)
        ])

def rename_node(planstore,plan_id,node_type,node_id,path_var):
    if node_type is None or node_id is None:
        return constant(None)

    def enter(task, validate):
        if node_type == 'a' or node_type == 'm':
            cur_name = node_id
        else:
            cur_name = name_from_task_id(node_id)

        new_name = cur_name if task is None else task
        help = None
        if validate is not None and 'name' in validate:
            help = (validate['name'],'danger')

        return right_task(
            view_information(cur_name,editor=BulmaTextInput(label='Current name',disabled=True)),
            update_information(new_name,editor=BulmaTextInput(label='New name',help=help))
        )

    def validate(task,action):
        if action == 'cancel':
            return constant(dict())

        errors = dict()
        errors = validate_name(errors,task)

        #If name is ok, check uniquness of the target node
        if not 'name' in errors:
            if node_type == 'a':
                return with_action_check(action,validate_unique_actor(planstore, plan_id, errors,task))
            if node_type == 't':
                return with_action_check(action,validate_unique_task(planstore, plan_id, errors,task,parent_from_id(node_id)))
            if node_type == 'i':
                return with_action_check(action,validate_unique_info_space(planstore, plan_id, errors,task,parent_from_id(node_id)))
            if node_type == 'm':
                return with_action_check(action,validate_unique_type(planstore, plan_id, errors,task))
        
        return with_action_check(action,constant(errors))

    def save(new_name):
       
        if node_type in ['t','i'] and not is_top_level(node_id):
            new_path = f'/{plan_id}/prepare/{node_type}/{parent_from_id(node_id)}.{new_name}'
        else:
            new_path = f'/{plan_id}/prepare/{node_type}/{new_name}'

        actions = {'a': lambda ps:ps.rename_actor(plan_id,node_id,new_name)
                   ,'t': lambda ps:ps.rename_task(plan_id,node_id,new_name)
                   ,'i': lambda ps:ps.rename_info_space(plan_id,node_id,new_name)
                   ,'m': lambda ps:ps.rename_type(plan_id,node_id,new_name)}
        
        return all_tasks(
            write_information(planstore,actions[node_type]),
            write_information(path_var,lambda pv:pv.write(new_path))
        )
        
    return after_dialog(f'Rename {node_id}',
        enter,
        validate,
        [('cancel','Cancel','ban',lambda _: constant(None))
        ,('add','Rename','i-cursor',save)
        ])

def delete_node(planstore, plan_id, node_type, node_id, path_var):
    if node_type is None or node_id is None:
        return constant(None)

    def enter_options():
        messages = {
            'a': f'Are you sure you want to delete actor "{node_id}"?',
            't': f'Are you sure you want to delete task "{node_id}"?',
            'i': f'Are you sure you want to delete information space "{node_id}"?',
            'm': f'Are you sure you want to message type "{node_id}"?'
        }
        return view_information(f'{messages[node_type]} This cannot be undone.')

    def save():
        if node_type in ['t','i']:
            parent = parent_from_id(node_id)
            if parent is not None:
                new_path = f'/{plan_id}/prepare/t/{parent_from_id(node_id)}'
            else:
                new_path = f'/{plan_id}/prepare'         
        else:
            new_path = f'/{plan_id}/prepare'

        actions = {'a':lambda ps:ps.delete_actor(plan_id,node_id),
                   't':lambda ps:ps.delete_task(plan_id,node_id),
                   'i':lambda ps:ps.delete_info_space(plan_id,node_id),
                   'm':lambda ps:ps.delete_type(plan_id,node_id)} 
        return all_tasks(
            write_information(planstore,actions[node_type]),
            write_information(path_var,lambda pv:pv.write(new_path))
        )
      
    return after_dialog('Delete..',
        lambda task, validate: enter_options(),
        lambda validate, task: constant(dict()),
        [('cancel','Cancel','ban',lambda _: constant(None))
        ,('delete','Delete','check',lambda _: save())
        ])
