
from toppyt import (ParallelTask, TaskVariable, after_task, after_value,
                    all_tasks, any_task, constant,
                    enter_information, forever, map_value,
                    read_information, update_information, view_information,
                    with_information, with_dependent, write_information, ViewEditor)
from toppyt.bulma import (BulmaButtons, BulmaIntInput, BulmaRecordEditor,
                          BulmaSelect, BulmaTextArea, BulmaTextInput)

from c2sketch.models import *
from ..data import ModelStore
from ..patterns import choose_task, view_with_edit
from ..plugins import AppPluginLoader
from ..ui import (TableChoice, SourceView, after_dialog, record_editor, record_view,
                  view_title)
from .record_types import edit_local_type


def edit_info_space(planstore: ModelStore, visualizations, plan_id: ModelID, ifs_id: InformationSpaceID, path_var: TaskVariable):
    def choose(general):
        if general is None:
            return view_information(f'Information space "{ifs_id}" does not exist.')

        return all_tasks (
            view_title(general['name']),
            choose_task([
            ('Attributes','file-alt', edit_info_space_general(planstore, plan_id,ifs_id)),
            ('Type','puzzle-piece', edit_info_space_type(planstore, plan_id,ifs_id)),
            ('Content','envelope', edit_info_space_content(planstore, visualizations, plan_id, ifs_id)),
            ('Visualization','image', view_info_space_visualization(planstore, visualizations, plan_id,ifs_id)),
            ('Use','exchange-alt', view_info_space_use(planstore, plan_id,ifs_id,path_var)),
            ('Source','code', view_info_space_source(planstore,plan_id,ifs_id)),
            ],'Attributes')
        )
    return with_information(planstore,lambda ps:ps.read_info_space_general(plan_id,ifs_id),choose)

def edit_info_space_general(planstore: ModelStore, plan_id: ModelID, ifs_id: InformationSpaceID):
    def view():
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label= 'Title', disabled = True)),
            ('description',BulmaTextArea(label= 'Description', disabled = True))
        ])
        return view_information(planstore,lambda ps:ps.read_info_space_general(plan_id, ifs_id), editor=editor)
    
    def edit(fields):
        editor = BulmaRecordEditor([
            ('name',BulmaTextInput(label = 'Name', disabled = True)),
            ('title',BulmaTextInput(label= 'Title')),
            ('description',BulmaTextArea(label= 'Description'))
        ])
        return update_information(fields, editor=editor)
    
    def save(updated_fields):
        return write_information(planstore,lambda ps:ps.write_info_space_general(plan_id,ifs_id,updated_fields))

    return view_with_edit(view(),edit,save)
    
def edit_info_space_type(model_store: ModelStore, model_id: ModelID, ifs_id: InformationSpaceID):

    def edit(type_options):
        return update_information(model_store,
                                    lambda ps: ps.read_info_space_type(model_id,ifs_id),
                                    lambda ps, choice :ps.write_info_space_type(model_id,ifs_id,choice),
                                    editor=BulmaSelect(type_options,label='Type',sync=True)
        )
    return with_information(model_store, lambda ps:ps.read_list_type_options(model_id),edit)
      

def edit_info_space_content(model_store: ModelStore, visualizations, plan_id: ModelID, ifs_id: InformationSpaceID):
   
    def edit_messages(type):
        if type is None:
            return view_information('Configure a type first')

        def edit(type_def, records):
            return forever([after_value(choose_action(type_def, records),lambda r: do_action(records,*r))])
    
        def choose_action(type_fields: list[RecordTypeField], records):
            headers = [field.name for field in type_fields]
            def row(record:Record):
                return [record.fields[field.name] if field.name in record.fields else '-' for field in type_fields]

            options = [(n,row(m),[('edit','Edit','edit'),('delete','Delete','trash')]) for (n,m) in enumerate(records)]
            actions = [('add','Add message','plus')]
            return any_task(
                enter_information(editor=TableChoice(options,headers)),
                map_value(enter_information(editor=BulmaButtons(actions,align_right=True)),lambda c: (None,c))
            )
        def do_action(messages, subject, action):
            if action == 'add':
                return after_dialog('Add message..',
                    lambda task, validate: edit_message(None, task, validate),
                    lambda validate, task: constant(dict()),
                    [('cancel','Cancel','ban',lambda _: constant(None))
                    ,('add','Add','plus', lambda v: add_message(messages,v))
                    ])
            if action == 'edit':
                message_no = int(subject)
                return after_dialog('Edit message..',
                    lambda task, validate: edit_message(messages[message_no], task, validate),
                    lambda validate, task: constant(dict()),
                    [('cancel','Cancel','ban',lambda _: constant(None))
                    ,('add','Save','save', lambda v: update_message(messages,message_no,v))
                    ])
            if action == 'delete':
                message_no = int(subject)
                return delete_message(messages,message_no)

            return constant(None)

        def edit_message(default, value, validate):
            editor_value = value
            if validate is None and default is not None:
                editor_value = default

            return update_information(editor_value,editor=record_editor(type))

        def add_message(messages, message):
            return write_information(model_store,lambda ps:ps.write_info_space_init_content(plan_id,ifs_id,messages + [message]))
    
        def update_message(messages, message_no, updated_message):
            messages = [updated_message if i == message_no else message for i, message in enumerate(messages)]
            return write_information(model_store,lambda ps:ps.write_info_space_init_content(plan_id,ifs_id,messages))
        
        def delete_message(messages, message_no):
            messages = [message for i, message in enumerate(messages) if i != message_no]
            return write_information(model_store,lambda ps:ps.write_info_space_init_content(plan_id,ifs_id,messages))


        return with_information(model_store,
                lambda ps:ps.read_type_fields(plan_id,type),
                lambda type_fields: with_information(model_store,
                    lambda ps:ps.read_info_space_init_content(plan_id,ifs_id),
                    lambda records: edit(type_fields,records)))
    

    return with_information(model_store,lambda ps:ps.read_info_space_type(plan_id,ifs_id,True),edit_messages)


def view_info_space_visualization(model_store: ModelStore, plugins: AppPluginLoader, model_id: ModelID, ifs_id: InformationSpaceID):
    
    
    def view(mb_plugin):
        if mb_plugin is None:
            return view_information('No visualization configured')
        
        if mb_plugin not in plugins.info_space_graphics:
            return view_information('Unknown visualization plugin')
        else:
            plugin_cls = plugins.info_space_graphics[mb_plugin]
            plugin = plugin_cls()
            models = model_store.model_set #FIXME: No direct access
            time = 0
            plugin.start(time,global_id_from_id(ifs_id,model_id),models)

            editor = ViewEditor(lambda records: plugin.render_svg(time,records))

        return view_information(model_store, lambda ms:ms.read_info_space_init_content(model_id,ifs_id),editor=editor)

    return with_information(model_store,lambda es:es.read_info_space_visualization_plugin(model_id,ifs_id),view)

def view_info_space_use(planstore, plan_id: ModelID, ifs_id: InformationSpaceID, path_var):
    def do_action(action):
        if action[1] == 'view-task':
            task_id = action[0]
            return write_information(path_var,lambda pv:pv.write(f'/{plan_id}/prepare/t/{task_id}'))
        return constant(None)
    
    def view_producers():
        def view(producers):
            options = [(t,[('view-task',task_from_id(t))],[]) for t in producers]
            return after_value(enter_information(editor=TableChoice(options)),do_action)

        return with_information(planstore, lambda ps:ps.read_info_space_producers(plan_id,ifs_id),view)

    def view_consumers():
        def view(consumers):
            options = [(t,[('view-task',task_from_id(t))],[]) for t in consumers]
            return after_value(enter_information(editor=TableChoice(options)),do_action)

        return with_information(planstore, lambda ps:ps.read_info_space_consumers(plan_id,ifs_id),view)
    
    def view_triggered():
        def view(triggered):
            options = [(t,[('view-task',task_from_id(t))],[]) for t in triggered]
            return after_value(enter_information(editor=TableChoice(options)),do_action)

        return with_information(planstore,lambda ps:ps.read_info_space_triggered(plan_id,ifs_id),view)
          
    def layout(parts):
        return f'''
            <div class="content">
                <h2 class="subtitle">Producers</h2>
                <p>Tasks contributing to this information space:</p>
            </div>
            {parts['producers']}
            <div class="content">
                <h2 class="is-size-4">Consumers</h2>
                <p>Tasks using information from this information space:</p>
            </div>
            {parts['consumers']}
            <div class="content">
                <h2 class="is-size-4">Triggers</h2>
                <p>Tasks that may be intitiated based on the information from this information space:</p>
            </div>
            {parts['triggered']}
        '''
    return ParallelTask([
        ('producers',view_producers()),
        ('consumers',view_consumers()),
        ('triggered',view_triggered())
    ],layout = layout)

def view_info_space_source(model_store: ModelStore, model_id: ModelID, ifs_id: InformationSpaceID):
   
    def view():
        return view_information(model_store,lambda ms: ms.info_space_source(model_id,ifs_id),editor=SourceView())
    
    def edit(value):
        return update_information(value,editor=BulmaTextArea())
    
    def save(value):
        return write_information(model_store,lambda ms: ms.write_info_space_source(model_id,ifs_id,value))
        
    return view_with_edit(view(),edit,save)