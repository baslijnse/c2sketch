
from toppyt import view_information, update_information, with_download, ViewEditor, all_tasks, ParallelTask, with_information, TaskVariable

from ..config import AppConfig
from ..data import ModelStore
from ..ui import TreeChoice, header_layout, choose_app_mode, view_model_title
from c2sketch import models
from c2sketch.visualizations import nx_actor_information_flow
from c2sketch.export import nx_full_network, plan_to_docx


import tempfile
import networkx as nx

__all__ = ('manage_exports','export_model')

async def plan_to_actor_information_flow_gexf(plan: models.Model):
    with tempfile.TemporaryFile() as tmp:
        nx.write_gexf(nx_actor_information_flow(plan),tmp)
        tmp.flush()
        tmp.seek(0)
        content = tmp.read()     
    return content    

async def plan_to_full_network_gexf(plan: models.Model):
    with tempfile.TemporaryFile() as tmp:
        nx.write_gexf(nx_full_network(plan),tmp)
        tmp.flush()
        tmp.seek(0)
        content = tmp.read()     
    return content

def manage_exports(config: AppConfig, model_store: ModelStore, path_var, cookie_jar):
    
    selection = TaskVariable(None)

    def select_model():
        
        def to_nodes(models):
            nodes = []
            for model_id, model_title, model_description in sorted(models,key=lambda e:e[0]):
                
                node_list = nodes
                segments = model_id.split('.')
                for segment in segments[:-1]:
                    if not node_list or node_list[-1]['name'] != segment:
                        node_list.append({'name': segment,'icon': 'folder','value': None,'children':[]})
                    node_list = node_list[-1]['children']
                
                node_list.append({'name': segments[-1],'icon': 'sitemap','value': model_id,'children':[]})
            return nodes

        return with_information(model_store, lambda ps:ps.list_models(),
            lambda models: update_information(selection, editor=TreeChoice(to_nodes(models)))
        )

    def do_export():
        return with_information(selection, lambda s:s.read(), lambda model_id:
                                export_model(config,model_store,model_id,path_var,cookie_jar)
                                if model_id is not None else
                                view_information(''))
    
    def layout(parts):
        return f'''
        <div class="prepare-grid">
            <div class="prepare-header">{header_layout(config.name, 'Export','','',parts['app_mode'])}</div>
            <div class="prepare-body">
                <div class="prepare-side">
                    <div class="panel-block">
                    {parts['choose_model']}
                    </div>
                </div>
                <div class="prepare-hsizer"></div>
                <div class="prepare-main">
                <div class="container prepare-inner">
                {parts['do_exports']}
                </div>
                </div>
            </div>
        </div>
        '''
    
    return ParallelTask(
        [('choose_model',select_model())
        ,('do_exports',do_export())
        ,('app_mode',choose_app_mode(path_var))
        ],layout=layout,result_builder=lambda rs:rs[-1])

def export_model(config: AppConfig, model_store: ModelStore, model_id, path_var, cookie_jar):

    def export(plan: models.Model):
        def export_docx():
            filename = f'{plan.id.replace(".","_")}.docx'
            export_headers = [
                (b'Content-Type',b'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                (b'Content-Disposition',bytes(f'attachment; filename="{filename}"','utf-8'))
            ]
            def view_export(url):
                view = f'''
                    <div class="content">
                    <h2 class="subtitle">Human readable plan as Microsoft Word document</h2>
                    <p>
                    Export the mission plan as a report with all information structured in an
                    easily readable format.
                    </p>
                    <a class="button" href="{url}">Download {filename}</a>
                    </div>
                '''
                return view_information(None,editor=ViewEditor(lambda _: view))
            
            async def export_content():
                return await plan_to_docx(plan)
            return with_download(view_export,export_headers,export_content)
            
        def export_full_graph_gexf():
            filename = f'{plan.id.replace(".","_")}.gexf'
            export_headers = [
                (b'Content-Type',b'text/xml'),
                (b'Content-Disposition',bytes(f'attachment; filename="{filename}"','utf-8'))
            ]
            def view_export(url):
                view = f'''
                    <div class="content">
                    <h2 class="subtitle">Full graph in GEXF format</h2>
                    <p>
                    Export the complete mission plan as a graph definition for graph analysis in
                    another tool.
                    </p>
                    <a class="button" href="{url}">Download {filename}</a>
                    <div class="content">
                '''
                return view_information(None,editor=ViewEditor(lambda _: view))
            
            async def export_content():
                return await plan_to_full_network_gexf(plan)
            return with_download(view_export,export_headers,export_content)

        def export_actor_information_flow_gexf():
            filename = f'{plan.id.replace(".","_")}-information-flow.gexf'
            export_headers = [
                (b'Content-Type',b'text/xml'),
                (b'Content-Disposition',bytes(f'attachment; filename="{filename}"','utf-8'))
            ]
            def view_export(url):
                view = f'''
                    <div class="content">
                    <h2 class="subtitle">Information flow graph in GEXF format</h2>
                    <p>
                    Export the information flow graph definition for graph analysis in
                    another tool.
                    </p>
                    <a class="button" href="{url}">Download {filename}</a>
                    <div class="content">
                '''
                return view_information(None,editor=ViewEditor(lambda _: view))
            
            async def export_content():
                return await plan_to_actor_information_flow_gexf(plan)
            return with_download(view_export,export_headers,export_content)


        def layout(parts):
            return f'''
                <div class="container">
                <div class="columns">
                    <div class="column is-10 is-offset-1 mt-4">
                    {parts['task']}
                    </div>
                </div>
                </div>
                '''
        return ParallelTask([
                ('subtitle',view_model_title(plan.id, plan.title, path_var)),
                ('task',all_tasks(
                    export_docx(),
                    export_full_graph_gexf(),
                    export_actor_information_flow_gexf()))
            ],layout = layout,result_builder=lambda rs:rs[-1])

    return with_information(model_store,lambda ps:ps.model(model_id),export,refresh=False)