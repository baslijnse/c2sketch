from ..models.structure import Node, Model
from ..write.model import model_node_to_c2s_str
from ..read.model import model_node_from_c2s_str

def _indent(lines: list[str], levels: int) -> list[str]:
    return [('    ' * levels) + line for line in lines]
    
def _dedent(lines: list[str], levels: int) -> list[str]:
    return [line[4*levels:] for line in lines]

def insert_node(parent: Node, index: int, insert_node: Node, insert_source: str):
    ... #TODO

def remove_node(parent: Node, index: int):
    ... #TODO

def get_node_source(node: Node) -> str:
    if node.source_start is None or node.source_end is None:
        return model_node_to_c2s_str(node)
    model = node.model
    if model.source:
        return '\n'.join(_dedent(model.source[node.source_start-1:node.source_end],node.depth - 1))
    else:
        return model_node_to_c2s_str(node)

def update_node_source(node: Node, source: str) -> None:
    
    if isinstance(node, Model):
        new_model = model_node_from_c2s_str(source, Model, node.id)
        node.source = source.splitlines()
        node.nodes = new_model.nodes
    else:
        assert node.parent is not None
        node_index = [n.node_id for n in node.parent.nodes].index(node.node_id)
       
        node_depth = node.depth
        model = node.model

        new_node = model_node_from_c2s_str(source, node.__class__)
        new_node_lines = _indent(source.splitlines(), node_depth - 1)
        new_node.parent = node.parent
        if node.source_start is not None:
            adjust_source_position(new_node, node.source_start - 1)
        
        if node.source_start is not None and node.source_end is not None:
            line_num_change = len(new_node_lines) - (node.source_end - node.source_start + 1)
            for sibling in node.parent.nodes[node_index + 1:]:
                adjust_source_position(sibling, line_num_change)

            model.source = model.source[:node.source_start - 1] + new_node_lines + model.source[node.source_end:]

        node.parent.nodes[node_index] = new_node

def adjust_source_position(node: Node, adjustment: int):
    if adjustment == 0:
        return
    if node.source_start is not None:
        node.source_start += adjustment
    if node.source_end is not None:
        node.source_end += adjustment
    for sub_node in node.nodes:
        adjust_source_position(sub_node, adjustment)

def replace_node(node: Node, replacement_node: Node):
    ...

def move_node_up(node: Node):
    if node.parent is None:
        return
    container = node.parent.nodes
    index = container.index(node)
    if index > 0:
        item = container.pop(index)
        container.insert(index-1,item)
        
def move_down(node: Node):
    if node.parent is None:
        return
    container = node.parent.nodes
    index = container.index(node)
    if index < len(container) - 1:
        item = container.pop(index)
        container.insert(index+1,item)
