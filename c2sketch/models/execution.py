"""Scenarios and events for dynamic analysis"""

from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any

from .identifier import *
from .structure import Record, TaskDefinition, ImplicitTaskDefinition
from .collection import ModelSet

__all__ = [
    'Scenario','ScenarioEvent',
    'ScenarioInformationEvent','ScenarioTaskInitiateEvent',
    'ScenarioJoinGroupEvent','ScenarioLeaveGroupEvent',
    'ScenarioEnterLocationEvent','ScenarioLeaveLocationEvent','ScenarioMoveEvent',
    'apply_scenario_event'
]

@dataclass
class Scenario:
    name: ScenarioName
    model: ModelID # The initial network state
    events: list[ScenarioEvent] = field(default_factory=list) # The sequence of events that transform the model over time

    @property
    def label(self) -> str:
        return self.name

@dataclass
class ScenarioInformationEvent:
    """Send information to an information space"""
    time: int
    actor: ActorID 
    information_space: InformationSpaceID
    fields: dict[str,Any]
    task: TaskID | None = None # Task context in which the information was sent, can be unknown

@dataclass
class ScenarioTaskInitiateEvent:
    """Initiate a new task instance"""
    time: int
    actor: ActorID 
    task_definition: TaskID
    parameter: dict[str,Any]
    trigger: InformationSpaceID | None = None
    for_actor: ActorID | None = None

@dataclass
class ScenarioJoinGroupEvent:
    """Actor joins a group/organization"""
    time: int
    actor: ActorID 
    group: ActorID

@dataclass
class ScenarioLeaveGroupEvent:
    """Actor leaves a group/organization"""
    time: int
    actor: ActorID 
    group: ActorID

@dataclass
class ScenarioEnterLocationEvent:
    """Actor adds location to its current set of locations"""
    time: int
    actor: ActorID
    location: LocationID

@dataclass
class ScenarioLeaveLocationEvent:
    """Actor removes location from its current set of locations"""
    time: int
    actor: ActorID
    location: LocationID

@dataclass
class ScenarioMoveEvent:
    """Actor leaves previous location(s) and enters new location"""
    time: int
    actor: ActorID
    location: LocationID

ScenarioEvent = ScenarioInformationEvent | ScenarioTaskInitiateEvent | ScenarioJoinGroupEvent | ScenarioLeaveGroupEvent | ScenarioEnterLocationEvent | ScenarioLeaveLocationEvent | ScenarioMoveEvent

### Application of events to a network to evolve it over time

def apply_scenario_event(model_set: ModelSet, main_model: ModelID, event: ScenarioEvent) -> None:
    #TODO: Check if the events are possible given the constraints of the model
    # E.g. Don't accept for information spaces that unreachable from the actor's locations
    # Or when information is sent to an information space, but the actor has no tasks in which
    # the information space is required
    
    match event:
        case ScenarioInformationEvent():
            info_space = model_set.get_info_space_by_id(event.information_space)
            info_space.create_records(
                records=[Record(info_space,[],event.fields)],
                create_time=event.time,
                create_actor=event.actor
                )
            info_space.limit_records(event.time)
        case ScenarioTaskInitiateEvent():
            definition = model_set.get_task_by_id(event.task_definition)
            if isinstance(definition,TaskDefinition) or isinstance(definition,ImplicitTaskDefinition):
                definition.create_instance(event.parameter,event.for_actor)
        case ScenarioJoinGroupEvent():
            actor = model_set.get_actor_by_id(event.actor)
            actor.add_group(event.group)
        case ScenarioLeaveGroupEvent():
            actor = model_set.get_actor_by_id(event.actor)
            actor.remove_group(event.group)
            group = model_set.get_actor_by_id(event.group)
            group.remove_member(event.actor)
        case ScenarioEnterLocationEvent():
            actor = model_set.get_actor_by_id(event.actor)
            actor.add_location(event.location)
        case ScenarioLeaveLocationEvent():
            actor = model_set.get_actor_by_id(event.actor)
            actor.remove_location(event.location)
        case ScenarioMoveEvent():
            actor = model_set.get_actor_by_id(event.actor)
            actor.set_locations([event.location])
        