"""Structural definition of actor-task-information network models, i.e. the C2Sketch meta-model"""

from __future__ import annotations
from dataclasses import dataclass, field
from enum import Enum
from typing import Any, Sequence

from .identifier import *
import copy

__all__ = [
    'Node',
    'Model','Import','Attribute','Constraint',
    'Actor','ActorType','ActorLocation','ActorGroup','ActorMember',
    'Location','LocationGroup','LocationMember',
    'Task','TaskDefinition','TaskInstance','TaskReference','TaskNode',
    'ImplicitTask','ImplicitTaskDefinition','ImplicitTaskInstance',
    'Trigger','InformationSpaceRequirement','InformationSpaceBinding',
    'InformationSpace','FieldMode','AgeLimit','KeyLimit','Record',
    'RecordType','RecordTypeField',
]

#For updating, we need an "update" method that either takes an updated
#note or a fragment of source code, it will update the fragment
#and adjust all start and end points 

class Node:
    """Models are heterogenous recursive tree structures consisting of nodes"""

    #Doubly linked structure for easy traversal
    parent: Node | None
    nodes: list[Node]

    #References to source text if available
    source_start: int | None #Reference to start line in source code
    source_end: int | None #Reference to end line in source code

    @property
    def node_id(self) -> str: #Always return globally qualified identifiers
        return '-'
    
    @property
    def complete_nodes(self) -> list[Node]:
        """List both explicit and implicit nodes"""
        return [node for node in self.nodes]
    
    @property
    def model(self) -> Model:
        if isinstance(self, Model):
            return self
        else:
            assert self.parent is not None #Only Model nodes are root nodes
            return self.parent.model

    @property
    def depth(self) -> int:
        if isinstance(self, Model):
            return 0
        else:
            assert self.parent is not None
            return self.parent.depth + 1
    
    def get_attribute(self, key: str) -> str | None:
        for node in self.nodes:
            if isinstance(node,Attribute) and node.name == key:
                return node.value
        return None
    
    def set_attribute(self, key: str, value:str) -> None:
        for node in self.nodes:
            if isinstance(node,Attribute) and node.name == key:
                node.value = value
                return
        self.nodes.append(Attribute(self,[],key,value))

    def del_attribute(self, key: str) -> None:
        for i, node in enumerate(self.nodes):
            if isinstance(node,Attribute) and node.name == key:
                self.nodes.pop(i)
                return

    def get_constraint(self, key: str) -> str | None:
        for node in self.nodes:
            if isinstance(node,Constraint) and node.name == key:
                return node.value
        return None       
     
@dataclass
class Model(Node):
    """Models are the main containers in which actors, tasks and information spaces are defined"""

    #Globally unique identifier of the model
    id: ModelID
    
    #Heterogenous list of model nodes
    nodes: list[Node] = field(default_factory=list)

    #Raw source code 
    source: list[str] = field(default_factory=list)

    @property
    def title(self) -> str | None:
        return self.get_attribute('title')

    @property
    def summary(self) -> str | None:
        return self.get_attribute('summary')

    @property
    def label(self) -> str:
        return self.id if self.title is None else self.title

    @property
    def node_id(self) -> str:
        return '-'
    
    @property
    def number(self) -> int:
        return 0
    
    ## Child nodes

    @property
    def imports(self) -> list[ModelID]:
        return [node.reference for node in self.nodes if isinstance(node,Import)]
    
    # The actors relevant to the execution of the mission.
    # Both the parties directly responsible for the mission as well as partners
    @property
    def actors(self) -> list[Actor]:
        return [node for node in self.nodes if isinstance(node,Actor)]

    # Named physical locations such as specific buildings or more abstract places
    # like 'home' or 'HQ'
    @property
    def locations(self) -> list[Location]:
        return [node for node in self.nodes if isinstance(node,Location)]
    
    # The top-level set of tasks that are performed by the actors
    @property
    def tasks(self) -> list[Task]:
        return [node for node in self.nodes if isinstance(node,Task)]
    
    @property
    def task_definitions(self) -> list[TaskDefinition]:
        return [node for node in self.nodes if isinstance(node,TaskDefinition)]
    
    @property
    def task_instances(self) -> list[TaskInstance]:
        return [node for node in self.nodes if isinstance(node,TaskInstance)]

    # The top-level information spaces that define the environment
    @property
    def info_spaces(self) -> list[InformationSpace]:
        return [node for node in self.nodes if isinstance(node,InformationSpace)]
    
    # A collection of record type sepecifications that define
    # the data that is exchanged between tasks and shared in information spaces
    @property
    def record_types(self) -> list[RecordType]:
        return [node for node in self.nodes if isinstance(node,RecordType)]
    
    ## Model manipulation methods

    def set_title(self, title: str | None) -> None:
        if title is not None:
            self.set_attribute('title',title)
        else:
            self.del_attribute('title')

    def set_summary(self, summary: str | None) -> None:
        if summary is not None:
            self.set_attribute('summary',summary)
        else:
            self.del_attribute('summary')

    def add_import(self, reference: ModelID) -> None:
        if reference in self.imports:
            raise ValueError(f'Model id "{reference}" already imported')
     
        self.nodes.append(Import(self,[],reference))
            
    def add_actor(self, name: ActorName, title: str | None, description: str | None = None, type: str | None = None) -> Actor:
        if name in [a.name for a in self.actors]:
            raise ValueError(f'Actor name "{name}" already exists')

        actor = Actor(self, [], name, None if type is None else ActorType(type))
        if title is not None:
            actor.set_attribute('title',title)
        if description is not None:
            actor.set_attribute('description',description)
        self.nodes.append(actor)
            
        return actor
    
    def add_location(self, name: LocationName, title: str | None, description: str | None = None) -> Location:
        if name in [l.name for l in self.locations]:
            raise ValueError(f'Location name "{name}" already exists')
        location = Location(self, [], name)
        if title is not None:
            location.set_attribute('title',title)
        if description is not None:
            location.set_attribute('description',description)
        self.nodes.append(location)
        return location
    
    def add_task(self, name: str, title: str | None, description: str | None = None) -> Task:
        if name in [t.name for t in self.tasks]:
            raise ValueError(f'Task "{name}" already exists')
      
        task = Task(self, [], name)
        if title is not None:
            task.set_attribute('title',title)
        if description is not None:
            task.set_attribute('description',description)
        self.nodes.append(task)
        return task
    
    def add_info_space(self, ifs_name: str, ifs_title: str | None = None, ifs_description: str | None = None) -> InformationSpace:
        if ifs_name in (ifs.name for ifs in self.info_spaces):
            raise ValueError(f'Information space "{ifs_name}" already exists')
        node = InformationSpace(self, [], ifs_name)
        if ifs_title is not None:
            node.set_attribute('title', ifs_title)
        if ifs_description is not None:
            node.set_attribute('description', ifs_description)
        self.nodes.append(node)
        return node
        
    def add_record_type(self, type_name: RecordTypeName) -> RecordType:
        if type_name in (t.name for t in self.record_types):
            raise ValueError(f'Message type "{type_name}" already exists')
        node = RecordType(self,[],type_name)
        self.nodes.append(node)
        return node
    
    
    def get_info_spaces_for_display(self, display_name: str) -> list[InformationSpace]:
        return [ifs for ifs in self.info_spaces if ifs.display == display_name]

@dataclass
class Import(Node):
    parent: Model | None
    nodes: list[Node]

    reference: ModelID
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class Attribute(Node):
    parent: Node | None
    nodes: list[Node]

    name: str
    value: str

    source_start: int | None = None
    source_end: int | None = None

@dataclass
class Constraint(Node):
    parent: Node | None
    nodes: list[Node]

    name: str
    value: str
 
    source_start: int | None = None
    source_end: int | None = None

# An actor is any entity that can perform tasks.
#
# For the primary organisation you may model this in more detail than for
# partner organisations which remain abstract organisations.

class ActorType(Enum):
    PERSON = "person" # an indivual human being that can perform tasks
    ANIMAL = "animal" # a trained animal that can perform tasks (e.g. a rescue dog)
    MACHINE = "machine" # a machine, or component that can autonomously perform tasks
    ORGANIZATION = "organization" #a formal organization such as a company, department, ngo or club
    TEAM = "team" # A less formal organization, that has a distinct identity
    CAPABILITY_GROUP = "capability_group" # a group of actors that possess a certain capability
    AUTHORITY_GROUP = "authority_group" #a group of actors that have a certain mandate or authority (e.g. having a driver's licence)

    @staticmethod
    def select_options():
        return [
            ('Person','person'),
            ('Animal','animal'),
            ('Machine','machine'),
            ('Organization','organization'),
            ('Team','team'),
            ('Capability Group','capability_group'),
            ('Authority Group','authority_group')
        ]

@dataclass
class ActorMember(Node):
    parent: Actor | None
    nodes: list[Node]

    actor_id: ActorID
 
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class ActorGroup(Node):
    parent: Actor | None
    nodes: list[Node]

    group_id: ActorID

    source_start: int | None = None
    source_end: int | None = None

@dataclass
class ActorLocation(Node):
    parent: Actor | None
    nodes: list[Node]

    location_id: LocationID
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class Actor(Node):
    parent: Model | None
    nodes: list[Node]

    name: ActorID
    type: ActorType | None = None

    source_start: int | None = None
    source_end: int | None = None

    @property
    def node_id(self):
        if self.parent is None:
            return self.name
        return f'{self.name}@{self.parent.id}'
    
    @property
    def title(self) -> str | None:
        return self.get_attribute('title')
    @title.setter
    def title(self,title: str | None):
        if title is None:
            self.del_attribute('title')
        else:
            self.set_attribute('title', title)

    @property
    def description(self) -> str | None:
        return self.get_attribute('description')
    @description.setter
    def description(self,description: str | None):
        if description is None:
            self.del_attribute('description')
        else:
            self.set_attribute('description', description)

    @property
    def color(self) -> str | None:
        return self.get_attribute('color')

    @property
    def label(self) -> str:
        return self.name if self.title is None else f'{self.title} ({self.name})'

    @property
    def groups(self) -> list[ActorID]:
        model_id = self.model.id
        return [global_id_from_id(node.group_id,model_id) for node in self.nodes if isinstance(node,ActorGroup)]
    
    @property
    def members(self) -> list[ActorID]:
        model_id = self.model.id
        return [global_id_from_id(node.actor_id,model_id) for node in self.nodes if isinstance(node,ActorMember)]
    
    @property
    def locations(self) -> list[LocationID]:
        model_id = self.model.id
        return [global_id_from_id(node.location_id,model_id) for node in self.nodes if isinstance(node,ActorLocation)]
    
    def set_locations(self, new_locations: list[LocationID]):
        #Naive implementation: remove all location nodes and create new ones
        self.nodes = [node for node in self.nodes if not isinstance(node,ActorLocation)]
        self.nodes.extend(ActorLocation(self,[],location_id) for location_id in new_locations)
    
    def add_location(self, location: LocationID):
        if location not in self.locations:
            self.nodes.append(ActorLocation(self,[],location))

    def remove_location(self, location: LocationID):
        if location in self.locations:
            self.nodes = [node for node in self.nodes if not (isinstance(node,ActorLocation) and node.location_id == location)]

    def add_group(self, group_id: ActorID) -> None:
        if group_id in self.groups:
            return
        self.nodes.append(ActorGroup(self,[],group_id))

    def add_member(self, member_id: ActorID) -> None:
        if member_id in self.members:
            return
        self.nodes.append(ActorMember(self,[],member_id))
        
    def remove_group(self, group_id: ActorID) -> None:
        remove = None
        model_id = self.model.id
        for i, node in enumerate(self.nodes):
            if isinstance(node,ActorGroup) and global_id_from_id(node.group_id,model_id) == group_id:
                remove = i
                break
        if remove is not None:
            self.nodes.pop(remove)

    def remove_member(self, member_id: ActorID) -> None:
        remove = None
        model_id = self.model.id
        for i, node in enumerate(self.nodes):
            if isinstance(node,ActorMember) and global_id_from_id(node.actor_id,model_id) == member_id:
                remove = i
                break
        if remove is not None:
            self.nodes.pop(remove)
    
    def update_actor_references(self, old_name: ActorID, new_name: ActorID) -> None:
        #Update affiliation list
        for i in range(len(self.groups)):
            if self.groups[i] == old_name:
                self.groups[i] = new_name

@dataclass
class LocationMember(Node):
    parent: Location | None
    nodes: list[Node]

    location_id: LocationID
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class LocationGroup(Node):
    parent: Location | None
    nodes: list[Node]

    group_id: LocationID

    source_start: int | None = None
    source_end: int | None = None

@dataclass
class Location(Node):
    parent: Model | None
    nodes: list[Node]
    
    name: LocationName

    source_start: int | None = None
    source_end: int | None = None

    @property
    def node_id(self):
        assert self.parent is not None
        return f'{self.name}@{self.parent.node_id}'
    
    @property
    def groups(self) -> list[LocationID]:
        return [node.group_id for node in self.nodes if isinstance(node,ActorGroup)]
    
    @property
    def members(self) -> list[ActorID]:
        return [node.actor_id for node in self.nodes if isinstance(node,ActorMember)]
    
    @property
    def title(self) -> str | None:
        return self.get_attribute('title')
    
    @property
    def description(self) -> str | None:
        return self.get_attribute('description')

    @property
    def label(self) -> str:
        return self.name if self.title is None else self.title


class TaskNode(Node):
    name: str
    label: str
    number: str
    
    def is_concrete(self) -> bool:
        """A task is concrete if none of its ancestors is a definition"""
        node = self
            
        while not isinstance(node,Model):
            if isinstance(node,TaskDefinition) or isinstance(node,ImplicitTaskDefinition) or node is None:
                return False
            node = node.parent
        return True
    
    @property
    def for_actor(self) -> ActorID | None:
        return None
    
    def parameter_scope(self) -> dict[str,Any]:
        if isinstance(self,ImplicitTask) :
            return self.parent.parameter_scope()
        
        if isinstance(self,TaskInstance):
            scope = {} if (isinstance(self.parent,Model) or self.parent is None) else self.parent.parameter_scope()
            if self.parameter:
                scope.update(self.parameter)
            return scope
        if isinstance(self,ImplicitTaskInstance):
            scope = self.parent.parameter_scope()
            if self.template.parameter:
                scope.update(self.template.parameter)
            return scope
        return {}
    
    def expanded_label(self, label: str) -> str:
        parameters = self.parameter_scope()
        for key, value in parameters.items():
            label = label.replace('{'+key+'}',str(value))
        return label


    @staticmethod
    def task_number(complete_nodes: list[Node], self_node_name: str) -> int:

        task_number = 0
        for node in complete_nodes:
            #Only tasks and definitions have a number, instances have the same number as their definition

            if isinstance(node,Task) or isinstance(node,ImplicitTask) or isinstance(node,TaskDefinition) or isinstance(node,ImplicitTaskDefinition):
                task_number += 1
                if node.name == self_node_name:
                    break
        else:
            raise ReferenceError('Node not found in parent node list')
        return task_number
    
@dataclass
class Task(TaskNode):
    parent: Model | Task | TaskDefinition | None
    nodes: list[Node]

    name: TaskName
    
    source_start: int | None = None
    source_end: int | None = None

    @property
    def node_id(self):
        if self.parent is None:
            return self.name
        if isinstance(self.parent,Model): #Root level
            return f'{self.name}@{self.parent.id}'
        else:
            parent_id, model_id = local_id_from_global_id(self.parent.node_id)
            return f'{parent_id}.{self.name}@{model_id}'

    @property
    def number(self) -> str:
        if self.parent is None:
            return '1'
        task_number = TaskNode.task_number(self.parent.complete_nodes,self.name)
        if isinstance(self.parent, Model):
            return str(task_number)
        else:
            return f'{self.parent.number}.{task_number}'
       
    @property
    def title(self) -> str | None:
        return self.get_attribute('title')
    @title.setter
    def title(self,title: str | None):
        if title is None:
            self.del_attribute('title')
        else:
            self.set_attribute('title', title)

    @property
    def description(self) -> str | None:
        return self.get_attribute('description')
    @description.setter
    def description(self,description: str | None):
        if description is None:
            self.del_attribute('description')
        else:
            self.set_attribute('description', description)
            
    @property
    def for_actor(self) -> ActorID | None:
        for_actor = self.get_constraint('for-actor')
        return None if for_actor is None else global_id_from_id(for_actor,self.model.id)
     
    @property
    def label(self) -> str:
        return self.name if self.title is None else self.title
    
    ## Child nodes
    @property
    def info_space_requirements(self) -> list[InformationSpaceRequirement]:
        return [node for node in self.nodes if isinstance(node,InformationSpaceRequirement)]
    
    # Information spaces (in the tasks parent context) that can trigger
    # an actor to initiate an instance of the task
    @property
    def triggers(self) -> list[str]:
        return [node.reference for node in self.nodes if isinstance(node,Trigger)]
    
    @property
    def tasks(self) -> list[Task]:
        return [node for node in self.nodes if isinstance(node,Task)]
    
    @property
    def task_definitions(self) -> list[TaskDefinition]:
        return [node for node in self.nodes if isinstance(node,TaskDefinition)]
    
    @property
    def task_instances(self) -> list[TaskInstance]:
        return [node for node in self.nodes if isinstance(node,TaskInstance)]

    def get_task_by_name(self, name: str) -> Task:
        for task in self.tasks:
            if task.name == name:
                return task
        raise KeyError(f'No task with name "{name}" in {self.node_id}')

    def get_task_def_by_name(self, name: str) -> TaskDefinition:
        for node in self.nodes:
            if isinstance(node,TaskDefinition) and node.name == name:
                return node
        raise KeyError(f'No task definition with name "{name}" in {self.node_id}')
    
    def is_compound(self) -> bool:
        return len(self.tasks) > 0
        
    def add_task(self, task_name: str, task_title: str | None = None, task_description: str | None = None) -> Task:
        if task_name in [t.name for t in self.tasks]:
            raise ValueError(f'Task "{task_name}" already exists')
        
        nodes = []
        if task_title is not None:
            nodes.append(Attribute(self,[],'title',task_title))
        if task_description is not None:
            nodes.append(Attribute(self,[],'description',task_description))
        node = Task(self,nodes,task_name)
        self.nodes.append(node)
        return node
    
@dataclass
class TaskDefinition(TaskNode):
    """A parameterized specification of a task that can be instantiated multiple times."""

    parent: Model | Task | TaskDefinition | None
    nodes: list[Node]

    name: TaskName
    parameter_type: RecordTypeID | None = None
    
    source_start: int | None = None
    source_end: int | None = None

    @property
    def node_id(self): #Same as task
        if self.parent is None:
            return self.name
        if isinstance(self.parent,Model): #Root level
            return f'{self.name}@{self.parent.id}'
        else:
            parent_id, model_id = local_id_from_global_id(self.parent.node_id)
            return f'{parent_id}.{self.name}@{model_id}'

    @property
    def number(self) -> str:
        if self.parent is None:
            return '1'
        task_number = TaskNode.task_number(self.parent.complete_nodes,self.name)
        
        if isinstance(self.parent, Model):
            return str(task_number)
        else:
            return f'{self.parent.number}.{task_number}'
      
    @property
    def title(self) -> str | None:
        return self.get_attribute('title')
    
    @property
    def description(self) -> str | None:
        return self.get_attribute('description')
    
    @property
    def for_actor(self) -> ActorID | None:
        for_actor = self.get_constraint('for-actor')
        return None if for_actor is None else global_id_from_id(for_actor,self.model.id)
    
    @property
    def label(self) -> str:
        return self.name if self.title is None else self.title
        
    ## Child nodes

    @property
    def info_space_requirements(self) -> list[InformationSpaceRequirement]:
        return [node for node in self.nodes if isinstance(node,InformationSpaceRequirement)]
    
    @property
    def triggers(self) -> list[str]:
        return [node.reference for node in self.nodes if isinstance(node,Trigger)]
    
    @property
    def tasks(self) -> list[Task]:
        return [node for node in self.nodes if isinstance(node,Task)]
    
    @property
    def task_definitions(self) -> list[TaskDefinition]:
        return [node for node in self.nodes if isinstance(node,TaskDefinition)]
    
    @property
    def task_instances(self) -> list[TaskInstance]:
        return [node for node in self.nodes if isinstance(node,TaskInstance)]

    def get_instances(self) -> list[TaskInstance]:
        if self.parent is None:
            return []
        return [node for node in self.parent.nodes if isinstance(node,TaskInstance) and node.name == self.name]
    
    def get_task_by_name(self, name: str) -> Task:
        for task in self.tasks:
            if task.name == name:
                return task
        raise KeyError(f'No task with name "{name}" in {self.node_id}')

    def get_task_def_by_name(self, name: str) -> TaskDefinition:
        for node in self.nodes:
            if isinstance(node,TaskDefinition) and node.name == name:
                return node
        raise KeyError(f'No task definition with name "{name}" in {self.node_id}')
 
    def is_compound(self) -> bool:
        return len(self.tasks) > 0

    def is_concrete(self) -> bool:
        return False
       
    def add_task(self, task_name: str, task_title: str | None = None, task_description: str | None = None) -> Task:
        if task_name in [t.name for t in self.tasks]:
            raise ValueError(f'Task "{task_name}" already exists')
        
        nodes = []
        if task_title is not None:
            nodes.append(Attribute(self,[],'title',task_title))
        if task_description is not None:
            nodes.append(Attribute(self,[],'description',task_description))

        self.tasks.append(Task(self,nodes,task_name))
        return self.tasks[-1]

   
    def create_instance(self, parameter: dict[str,Any], for_actor: ActorID | None = None):
        assert self.parent is not None

        if self.parent is None or not (isinstance(self.parent,Model) or self.parent.is_concrete()):
            raise Exception('Cannot create task instance without a concrete parent task')
        
        existing = [node.sequence for node in self.parent.nodes if isinstance(node,TaskInstance) and node.name == self.name]
        sequence = max(existing) + 1 if existing else 1
        instance_node = TaskInstance(self.parent,[],self.name,sequence,parameter=copy.deepcopy(parameter))
        self.parent.nodes.append(instance_node)
        #Initial constraints
        if for_actor is not None:
            instance_node.nodes.append(Constraint(self,[],'for-actor',for_actor))


@dataclass
class TaskInstance(TaskNode):
    parent: Model | Task | TaskDefinition | TaskInstance | ImplicitTask | ImplicitTaskInstance | None
    nodes: list[Node]

    name: str
    sequence: int

    # The a-priori information that defines the task instance
    parameter: dict[str,Any] | None = None
    
    source_start: int | None = None
    source_end: int | None = None

    @property
    def node_id(self):
        if self.parent is None:
            return f'{self.name}-{self.sequence}' 
        if isinstance(self.parent,Model): #Root level
            return f'{self.name}-{self.sequence}@{self.parent.id}'
        else:
            parent_id, model_id = local_id_from_global_id(self.parent.node_id)
            return f'{parent_id}.{self.name}-{self.sequence}@{model_id}'
    
    @property
    def number(self) -> str:
        if self.parent is None:
            return f'1-{self.sequence}'
        task_number = TaskNode.task_number(self.parent.complete_nodes,self.name)
       
        if isinstance(self.parent, Model):
            return f'{task_number}-{self.sequence}'
        else:
            return f'{self.parent.number}.{task_number}-{self.sequence}'
    
   
    @property
    def complete_nodes(self) -> list[Node]:
        """List both explicit and implicit nodes"""

        definition = self.get_definition()
        nodes = []
        #The tasks and instances defined in the definition are implicit 
        for node in definition.nodes:
            if isinstance(node,Task):
                nodes.append(ImplicitTask(self,node))
            elif isinstance(node,TaskDefinition):
                nodes.append(ImplicitTaskDefinition(self,node))
            elif isinstance(node,TaskInstance):
                nodes.append(ImplicitTaskInstance(self,node))
        #Add the explicitly defined nodes 
        nodes.extend(self.nodes)  
        return nodes

    @property
    def for_actor(self) -> ActorID | None:
        for_actor = self.get_constraint('for-actor')
        return None if for_actor is None else global_id_from_id(for_actor,self.model.id)
    
    @property
    def label(self) -> str:
        return self.expanded_label(self.get_definition().label)

    @property
    def info_space_requirements(self) -> list[InformationSpaceRequirement]:
        return self.get_definition().info_space_requirements
    @property
    def info_space_bindings(self)-> list[InformationSpaceBinding]:
        return [node for node in self.nodes if isinstance(node,InformationSpaceBinding)]

    @property
    def task_instances(self) -> list[TaskInstance]:
        return [node for node in self.nodes if isinstance(node,TaskInstance)]

    def get_definition(self) -> TaskDefinition:
        if self.parent is not None:
            for node in self.parent.complete_nodes:
                if isinstance(node,TaskDefinition) and node.name == self.name:
                    return node
            
        raise KeyError(f'Definition of instance {self.name} not found')  
    
@dataclass
class ImplicitTask(TaskNode):
    parent:  TaskInstance | ImplicitTask | ImplicitTaskDefinition | ImplicitTaskInstance
    template: Task

    @property
    def name(self) -> str:
        return self.template.name
    
    @property
    def node_id(self):
        parent_id, model_id = local_id_from_global_id(self.parent.node_id)
        return f'{parent_id}.{self.template.name}@{model_id}'

    @property
    def number(self) -> str:
        task_number = TaskNode.task_number(self.parent.complete_nodes,self.name)
        return f'{self.parent.number}.{task_number}'

    @property
    def complete_nodes(self) -> list[Node]:
        nodes = []
        for node in self.template.nodes:
            if isinstance(node,Task):
                nodes.append(ImplicitTask(self,node))
            elif isinstance(node,TaskDefinition):
                nodes.append(ImplicitTaskDefinition(self,node))
            elif isinstance(node,TaskInstance):
                nodes.append(ImplicitTaskInstance(self,node))
        return nodes

    @property
    def for_actor(self) -> ActorID | None:
        return self.template.for_actor
    @property
    def label(self) -> str:
        return self.expanded_label(self.template.label)
     
@dataclass
class ImplicitTaskDefinition(TaskNode):
    parent: TaskInstance | ImplicitTask | ImplicitTaskDefinition | ImplicitTaskInstance
    template: TaskDefinition

    @property
    def name(self) -> str:
        return self.template.name

    @property
    def node_id(self):
        parent_id, model_id = local_id_from_global_id(self.parent.node_id)
        return f'{parent_id}.{self.template.name}@{model_id}'
        
    @property
    def number(self) -> str:
        task_number = TaskNode.task_number(self.parent.complete_nodes,self.name)
        return f'{self.parent.number}.{task_number}'
    
    @property
    def complete_nodes(self) -> list[Node]:
        nodes = []
        for node in self.template.nodes:
            if isinstance(node,Task):
                nodes.append(ImplicitTask(self,node))
            elif isinstance(node,TaskDefinition):
                nodes.append(ImplicitTaskDefinition(self,node))
            elif isinstance(node,TaskInstance):
                nodes.append(ImplicitTaskInstance(self,node))
        return nodes
    
    @property
    def for_actor(self) -> ActorID | None:
        return self.template.for_actor
    @property
    def label(self) -> str:
        return self.template.label
    
    @property
    def parameter_type(self) -> RecordTypeID | None:
        return self.template.parameter_type

    def get_instances(self) -> list[TaskInstance|ImplicitTaskInstance]:
        return [node for node in self.parent.complete_nodes 
                if (isinstance(node,TaskInstance) or isinstance(node,ImplicitTaskInstance)) and node.name == self.name]
    
    def create_instance(self, parameter: dict[str,Any], for_actor: ActorID | None = None):
        assert self.parent is not None

        if self.parent is None or isinstance(self.parent,ImplicitTaskDefinition) or not (isinstance(self.parent,Model) or self.parent.is_concrete()):
            raise Exception('Cannot create task instance without a concrete parent task')
        
        existing = [node.sequence for node in self.parent.nodes if (isinstance(node,TaskInstance) or isinstance(node,ImplicitTaskInstance)) and node.name == self.name]
        sequence = max(existing) + 1 if existing else 1
        instance_node = TaskInstance(self.parent,[],self.name,sequence,parameter=copy.deepcopy(parameter))
        self.parent.nodes.append(instance_node)
        #Initial constraints
        if for_actor is not None:
            instance_node.nodes.append(Constraint(self,[],'for-actor',for_actor))
 
@dataclass
class ImplicitTaskInstance(TaskNode):
    parent: TaskInstance | ImplicitTask | ImplicitTaskDefinition | ImplicitTaskInstance
    template: TaskInstance

    @property
    def name(self) -> str:
        return self.template.name
    
    @property
    def node_id(self):
        parent_id, model_id = local_id_from_global_id(self.parent.node_id)
        return f'{parent_id}.{self.name}-{self.template.sequence}@{model_id}'

    @property
    def number(self) -> str:
        task_number = TaskNode.task_number(self.parent.complete_nodes, self.name)
        return f'{self.parent.number}.{task_number}-{self.template.sequence}'

    @property
    def sequence(self) -> int:
        return self.template.sequence
    
    @property
    def parameter(self) -> dict[str,Any] | None:
        return self.template.parameter
    
    @property
    def for_actor(self) -> ActorID | None:
        return self.template.for_actor
    
    @property
    def complete_nodes(self) -> list[Node]:
        definition = self.template.get_definition()
        nodes = []
        for node in definition.nodes:
            if isinstance(node,Task):
                nodes.append(ImplicitTask(self,node))
            elif isinstance(node,TaskDefinition):
                nodes.append(ImplicitTaskDefinition(self,node))
            elif isinstance(node,TaskInstance):
                nodes.append(ImplicitTaskInstance(self,node))
    
        return nodes
    
    @property
    def label(self) -> str:
        return self.expanded_label(self.template.get_definition().label)

    def get_definition(self) -> ImplicitTaskDefinition:
        for node in self.parent.complete_nodes:
            if isinstance(node,ImplicitTaskDefinition) and node.name == self.name:
                return node
        
        raise KeyError(f'Definition of instance {self.name} not found')
@dataclass
class TaskReference(Node):
    parent: Task | TaskDefinition | None
    nodes: list[Node]

    reference: str

    # The a-priori information that defines the task instance when
    # a task definition is referenced. None if a task is referenced.
    parameter: dict[str,Any] | None = None

    source_start: int | None = None
    source_end: int | None = None

@dataclass
class Trigger(Node):
    parent: Task | TaskDefinition | TaskInstance | None
    nodes: list[Node]
    
    reference: InformationSpaceID
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class InformationSpaceRequirement(Node):
    """Allows referencing dynamically bound external information spaces"""
    parent: Task | TaskDefinition | None
    nodes: list[Node]

    name: str
    type: RecordTypeID | None = None
    read: bool = True
    write: bool = True
    binding: str | None = None #default binding
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class InformationSpaceBinding(Node):
    parent: TaskInstance | None
    nodes: list[Node]

    name: str #References an information space requirement with the same name
    binding: str #References an information space from the task scope
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class InformationSpace(Node):
    parent: Model | InformationSpace | None
    nodes: list[Node]

    name: InformationSpaceName
    type: RecordTypeID | None = None

    source_start: int | None = None
    source_end: int | None = None

    #Run time index of record keys to records
    _index: dict[str,Record] = field(default_factory=dict)

    @property
    def node_id(self):
        if self.parent is None:
            return self.name
        if isinstance(self.parent,Model): #Root level
            return f'{self.name}@{self.parent.id}'
        else:
            parent_id, model_id = local_id_from_global_id(self.parent.node_id)
            return f'{parent_id}.{self.name}@{model_id}'
        
    @property
    def title(self) -> str | None:
        return self.get_attribute('title')
    @title.setter
    def title(self,title: str | None):
        if title is None:
            self.del_attribute('title')
        else:
            self.set_attribute('title', title)

    @property
    def description(self) -> str | None:
        return self.get_attribute('description') 
    @description.setter
    def description(self,description: str | None):
        if description is None:
            self.del_attribute('description')
        else:
            self.set_attribute('description', description)
    
    @property
    def color(self) -> str | None:
        return self.get_attribute('color')
   
    @property
    def label(self) -> str:
        return self.name if self.title is None else self.title 

    @property
    def display(self) -> str | None:
        return self.get_attribute('display')
    
    @display.setter
    def display(self, display: str | None) -> None:
        if display is None:
            self.del_attribute('display')
        else:
            self.set_attribute('display', display)
         
    @property
    def records(self) -> list[Record]:
        return [node for node in self.nodes if isinstance(node,Record)]

    @property
    def at_location(self) -> LocationID | None:
        at_location = self.get_constraint('at-location')
        return None if at_location is None else global_id_from_id(at_location,self.model.id)
    
    @property
    def for_actor(self) -> ActorID | None:
        for_actor = self.get_constraint('for-actor')
        return None if for_actor is None else global_id_from_id(for_actor,self.model.id)

    @property
    def key_field(self) -> str | None:
        for node in self.nodes:
            if isinstance(node,FieldMode) and node.mode == 'key':
                return node.field_name
        return None

    @property
    def field_modes(self) -> dict[str,str]:
        modes = {}
        for node in self.nodes:
            if isinstance(node,FieldMode):
                modes[node.field_name] = node.mode
        return modes
    
    @property
    def age_limit(self) -> int | None:
        for node in self.nodes:
            if isinstance(node,AgeLimit):
                return node.limit
        return None

    @property
    def key_limit(self) -> int | None:
        for node in self.nodes:
            if isinstance(node,KeyLimit):
                return node.limit
        return None

    def set_display(self, display: str | None) -> None:
        self.display = None if display == '' else display
     
    def update_record_type_references(self, old_name: RecordTypeName, new_name: RecordTypeName | None) -> None:
        if self.type == old_name:
            self.type = new_name

    def create_records(self, records: Sequence[Record],
                    create_time: int | None = None,
                    create_actor: ActorID | None = None,
                    create_location: LocationID | None = None) -> None:
      
        sequence_number = max(r.sequence_number for r in self.records) + 1 if self.records else 1
        key_field = self.key_field
        field_modes = self.field_modes

        if key_field is None:
            #Simply append a copy with updated fields
            #Conceptually the sequence_number is used as key, which guarantees exactly one value exist for each key
            for record in records:
                self.nodes.append(Record(
                    self,
                    nodes = [],
                    fields = copy.deepcopy(record.fields),
                    sequence_number = sequence_number,
                    create_time = create_time if create_time is not None else 0,
                    create_actor = create_actor,
                    create_location = create_location
                ))
                sequence_number += 1
        else:
            for record in records:
                key = record.fields[key_field]
                if key in self._index:
                    #Update the record based on the modes of the fields
                    key_record = self._index[key]
                    for field, value in record.fields.items():
                        field_mode = field_modes.get(field,'last')
                        if (field_mode == 'last') or \
                            (key not in key_record.fields and field_mode in {'first','min','max'}) or \
                            (field_mode == 'min' and value < key_record.fields[key]) or\
                            (field_mode == 'max' and value > key_record.fields[key]):
                                key_record.fields[field] = value
                            
                    key_record.sequence_number = sequence_number
                    if create_time is not None:
                        key_record.create_time = create_time
                    if create_actor is not None:
                        key_record.create_actor = create_actor
                    if create_location is not None:
                        key_record.create_location = create_location
                else:
                    #Add a new record
                    key_record = Record(self,
                        [],
                        fields = copy.deepcopy(record.fields),
                        sequence_number = sequence_number,
                        create_time= create_time if create_time is not None else 0,
                        create_actor = create_actor,
                        create_location = create_location)
                    self.nodes.append(key_record)
                    self._index[key] = key_record
                sequence_number += 1
        
    
    def limit_records(self, time: int) -> None:
        #First remove records that are too old
        age_limit = self.age_limit
        if age_limit is not None:
            keep_time = time - age_limit
            self.nodes = [node for node in self.nodes if (node.create_time >= keep_time if isinstance(node,Record) else True)]   
        
        #If there are still to much records, drop the oldest
        key_limit = self.key_limit
        if key_limit is not None:
            records = self.records
            if len(records) > key_limit:
                for node in sorted(records, key = lambda record: record.create_time)[len(records) - key_limit:]:
                    self.nodes.remove(node)

    def last_updated(self) -> int | None:
        return self.records[-1].create_time if self.records else None
    
@dataclass
class FieldMode(Node):
    parent: InformationSpace | None
    nodes: list[Node]

    field_name: str
    mode: str # key | first | last | min | max

    source_start: int | None = None
    source_end: int | None = None

@dataclass
class AgeLimit(Node):
    parent: InformationSpace | None
    nodes: list[Node]

    limit: int

    source_start: int | None = None
    source_end: int | None = None

@dataclass
class KeyLimit(Node):
    parent: InformationSpace | None
    nodes: list[Node]

    limit: int
    
    source_start: int | None = None
    source_end: int | None = None

@dataclass
class Record(Node):
    parent: InformationSpace | None
    nodes: list[Node]

    fields: dict[str,Any] = field(default_factory=dict)
    
    #Meta-data
    sequence_number: int = 0

    create_time: int = 0
    create_actor: ActorID | None = None
    create_location: LocationID | None = None

    source_start: int | None = None
    source_end: int | None = None
    
    def inline_references(self, values: Record) -> Record:
        inlined = {}
        for key,value in self.fields.items():
            if isinstance(value,str) and values is not None:
                try:
                    inlined[key] = value.format(**values.fields)
                except KeyError: #If a specified replacement is not available, just use the value
                    inlined[key] = value
            else:
                inlined[key] = value
        return Record(values.parent,[],inlined)

@dataclass
class RecordType(Node):
    parent: Model | None
    nodes: list[Node]

    name: RecordTypeName
    
    source_start: int | None = None
    source_end: int | None = None

    def __str__(self):
        fields = ','.join(f'{field.name}: {field.type}' for field in self.fields)
        return f'Custom type: {fields}'
    
    @property
    def node_id(self):
        if self.parent is None:
            return self.name
        return f'{self.name}@{self.parent.id}'
        
    @property
    def fields(self) -> list[RecordTypeField]:
        return [node for node in self.nodes if isinstance(node,RecordTypeField)]

@dataclass
class RecordTypeField(Node):
    parent: RecordType | None
    nodes: list[Node]
    
    name: str
    type: RecordTypeID | PrimitiveTypeID | None

    source_start: int | None = None
    source_end: int | None = None
