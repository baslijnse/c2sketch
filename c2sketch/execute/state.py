from __future__ import annotations
from c2sketch.models import *
from c2sketch.execute import plugins
from dataclasses import dataclass, field
from copy import deepcopy
import datetime

__all__ = ('ExecutionState',)

class ExecutionState:
    """Execution state during execution of a scenario
    
    Even though the current set of tasks and information spaces at a moment in time
    can be deterministically computed from a scenario it is too expensive during exection.
    This cache keeps the current state of a scenario readily available.
    """

    models: ModelSet
    scenario: Scenario
    time: int = 0 #Discrete time step, resolution can be different for different models

    playback: bool = False #When the time is increased, events from the scenario are applied
    record: bool = False #New events are inserted into the scenario

    agents: dict[ActorID,plugins.Agent]
  
    max_ids: dict[str,int] = field(default_factory=dict)

    def __init__(self, models: ModelSet, scenario: Scenario, playback: bool = False, record: bool = False):
        self.models = models
        self.scenario = scenario
        self.playback = playback
        self.record = record
        self.agents = {}

    def list_agents(self) -> list[ActorID]:
        agents = []
        for actor_id in self.models.list_actors(self.scenario.model):
            actor = self.models.get_actor_by_id(actor_id)
            
            if actor.get_attribute('agent-type') is not None:
                agents.append(actor_id)
        return agents
    
    def list_active_agents(self) -> list[ActorID]:
        return list(self.agents.keys())
    
    def agent_status(self) -> dict[ActorID,bool]:
        return {agent_id: agent_id in self.agents for agent_id in self.list_agents()}
    
    def start_agents(self, loader: plugins.PluginLoader):
        for actor_id in self.models.list_actors(self.scenario.model):
            self.start_agent(loader,actor_id)
        
    def start_agent(self, loader: plugins.PluginLoader, actor_id: ActorID):
        actor = self.models.get_actor_by_id(actor_id)   
        agent_type_id = actor.get_attribute('agent-type')
        if agent_type_id is not None and actor_id not in self.agents:
            agent_cls = loader.agents[agent_type_id]
            agent = agent_cls()
            agent.start(self.time,actor_id,self.models)
            self.agents[actor_id] = agent

    def stop_agents(self):
        for agent in self.agents.values():
            agent.stop(self.time)

        self.agents.clear()

    def stop_agent(self, actor_id: ActorID):
        if actor_id in self.agents:
            agent = self.agents.pop(actor_id)
            agent.stop(self.time)

    def step_time(self):
        """Increase time and apply scenario events, if applicable"""
        self.time += 1

        agent_events = []
        for agent in self.agents.values(): #TODO: Make a deterministic configurable ordering
            agent_events.extend(agent.interact(self.time,self.models))
        
        if agent_events:
            for event in agent_events:
                self.apply_event(event)
              
    def apply_event(self, event: ScenarioEvent):
        #Apply event to the models and
        #If record is enabled insert the event into the scenario
        apply_scenario_event(self.models,self.scenario.model,event)
        if self.record:
            self.scenario.events.append(event)

    def get_task_instances(self, task_name: str, context_instance: TaskInstanceID | None = None) -> list[tuple[TaskInstanceID,TaskInstanceExecution]]:
        
        prefix = task_name if context_instance is None else f'{context_instance}.{task_name}'
       
        def is_sub(identifier:str,  prefix:str) -> bool:
            return identifier.startswith(prefix) and identifier[len(prefix)+1:].isdigit()

        return [(tid,tie) for tid,tie in self.task_instances.items() if is_sub(tid,prefix)]
       
    def num_instances(self) -> int:
        return len(self.task_instances)

    def add_initial_info_space_content(self, model: Model) -> None:

        timestamp = datetime.now().timestamp()
        for ifs in model.info_spaces:
            if ifs.content:
                messages = [{'NUMBER':num,'TIME': timestamp, **message} for num,message in enumerate(ifs.content,1)]
                self.info_space_contents[ifs.name] = InformationSpace(ifs.update_field,ifs.limit,messages)
            else:
                self.info_space_contents[ifs.name] = InformationSpace(ifs.update_field,ifs.limit)

    def add_initial_instances(self, model: Model) -> list[TaskInstanceID]:
        return []
        # timestamp = datetime.now().timestamp()
        # global_ifs_scope = {ifs.name: ifs.name for ifs in model.info_spaces}
        # for top_instance_id, top_instance in model.get_instances().items():
#            self._add_with_sub_instances(model, timestamp, top_instance_id, top_instance, set(), global_ifs_scope)
            # self.max_ids[top_instance_id[:top_instance_id.find('.')]] = int(top_instance_id[top_instance_id.rfind('.')+1:])

        # return list(self.task_instances.keys())

    def add_additional_instance(self,
        model: Model,
        task_id: TaskID,
        parent_instance_id: TaskInstanceID | None = None,
        override: TaskInstance | None = None) -> TaskInstanceID:
    
        timestamp = datetime.now().timestamp()
        task = model.get_task_by_id(task_id)
#        instance = task.get_instance(override)
    
        #Determine instance id and scope
        if is_top_level(task_id):
            #Global scope
            scope =  {ifs.name: ifs.name for ifs in model.info_spaces}
            #Determine next instance id
            next_num = self.max_ids.get(task_id,0) + 1
            instance_id = f'{task_id}-{next_num}'
            self.max_ids[task_id] = next_num
        else:
            if parent_instance_id is None:
                raise ValueError('Parent instance id must be given for sub-tasks')
            #Scope is derived from the parent instance
            scope = dict()
            parent_instance = self.task_instances[parent_instance_id]
            
            scope.update(parent_instance.info_space_bindings)

            #Determine next instance id
            next_num = self.max_ids.get(f'{parent_instance_id}.{task.name}',0) + 1
            instance_id = f'{parent_instance_id}.{task.name}-{next_num}'
            self.max_ids[f'{parent_instance_id}.{task.name}'] = next_num
        
        #self._add_with_sub_instances(model,timestamp,instance_id,instance,set(),scope)

        return instance_id

    # def _add_with_sub_instances(self, model:Model, timestamp, instance_id: TaskInstanceID, instance: TaskInstance, for_actor_set: set[ActorID], ifs_scope: dict[str,InformationSpaceInstanceID]):
            
    #     task_id = task_id_from_instance_id(instance_id)
    #     task = model.get_task_by_id(task_id)

    #     for_actor_set = for_actor_set.union(set() if instance.for_actor is None else {instance.for_actor})
    #     ifs_content = self.init_info_space_content(timestamp, instance, task)
        
    #     #FIXME: set ifs_content in global dictionary

    #     #References to the parent scope are dereferenced to concrete information space instance id's
    #     #References to local information spaces shadow the parent scope
    #     instance_bindings = {}
    #     for name, ref in instance.info_space_bindings.items():
    #         if ref in ifs_content:
    #             instance_bindings[name] = f'{instance_id}.{ref}'
    #         elif ref in ifs_scope:
    #             instance_bindings[name] = ifs_scope[ref]
        
    #     self.task_instances[instance_id] = TaskInstanceExecution(
    #         task_id = task_id,
    #         parameter = instance.parameter,
    #         for_actor_set = for_actor_set,
    #         info_space_bindings = instance_bindings 
    #     )
        
    #     #The info space scope from the parent context is extended with locally defined information spaces
    #     instance_scope = copy(instance_bindings)
    #     instance_scope.update({local_name: f'{instance_id}.{local_name}' for local_name in ifs_content.keys()})
        
    #     # Create all sub instances
    #     for sub_id, sub_instance in instance.instances.items():
    #         self._add_with_sub_instances(model,timestamp, f'{instance_id}.{sub_id}', sub_instance, for_actor_set, instance_scope)
    #         self.max_ids[f'{instance_id}.{sub_id[:sub_id.find("-")]}'] = int(sub_id[sub_id.rfind('-')+1:])
    
    @staticmethod
    def init_info_space_content(timestamp, instance: TaskInstance, task: Task):
        instance_ifs = dict()
        if task.info_spaces:
            for ifs in task.info_spaces:
                if ifs.name in instance.info_space_content:
                    messages = [{'NUMBER':num,'TIME': timestamp, **message} for num,message in enumerate(instance.info_space_content[ifs.name],1)]
                    instance_ifs[ifs.name] = InformationSpace(ifs.update_field,ifs.limit,messages)
                else:
                    instance_ifs[ifs.name] = InformationSpace(ifs.update_field,ifs.limit)
        return instance_ifs

    def list_instances_of_task(self, task_id: TaskID) -> list[TaskInstanceID]:
        return [instance_id for instance_id, instance in self.task_instances.items() if instance.task_id == task_id]

    def list_instances_for_actor(self, model: Model, actor_id: ActorID) -> list[TaskInstanceID]:
        affiliations = model.get_actor_affiliations(actor_id,indirect=True)
       
        return [instance_id
            for instance_id, instance 
            in self.task_instances.items()
            if instance.for_actor_set.issubset(affiliations) #All actor requirements must be met
        ]

    def list_initiate_options_for_actor(self, model: Model, actor_id: ActorID) -> list[tuple[TaskID,TaskInstanceID | None]]:
        return []
    
        # instance_ids = self.list_instances_for_actor(model,actor_id)
        # active_task_ids = set(task_id_from_instance_id(instance_id) for instance_id in instance_ids)
        # additional_task_ids = model.list_addable_tasks(active_task_ids,actor_id)
        
        # result: list[tuple[TaskID,TaskInstanceID | None]] = list()
        # for task_id in additional_task_ids:
        #     parent_task_id = parent_from_id(task_id)
        #     if parent_task_id is None:
        #         result.append((task_id,None))
        #     else:
        #         for instance_id in instance_ids:
        #             if task_id_from_instance_id(instance_id) == parent_task_id:
        #                 result.append((task_id,instance_id))

        # return result

    def resolve_info_space_binding(self, model: Model, instance_id: TaskInstanceID, ifs_name: str):
        if instance_id in self.task_instances:
            if ifs_name in self.task_instances[instance_id].info_space_bindings:
                return self.task_instances[instance_id].info_space_bindings[ifs_name]
        
        task_id = task_id_from_instance_id(instance_id)
        task = model.get_task_by_id(task_id)
    
        return task.resolve_info_space(ifs_name)

    def _find_info_space(self, modelset: ModelSet, ifs_id: InformationSpaceID) -> InformationSpace | None:
        
        if not ifs_id in self.info_space_contents:
            ifs = modelset.get_info_space_by_id(ifs_id)
            messages = deepcopy(ifs.content)
            self.info_space_contents[ifs_id] = InformationSpace(ifs.update_field, ifs.limit, messages)
        return self.info_space_contents[ifs_id]

    def add_messages(self, modelset: ModelSet, ifs_id: InformationSpaceID, messages: list[Record]) -> None:
        ifs = self._find_info_space(modelset, ifs_id)
        if ifs is not None:
            ifs.add_messages(messages)

    def get_messages(self, modelset: ModelSet, ifs_id: InformationSpaceID, after_no: int | None) -> list[Record]: 
        
        ifs = self._find_info_space(modelset, ifs_id)
        if ifs is None:
            return list()

        update_field = ifs.update_field
        if not update_field is None:
            key = lambda m: m[update_field]
            messages = sorted(ifs.attributes.values(),key=key)
        else:
            messages = ifs.messages

        after = 0 if after_no is None else after_no
        return [m for m in messages if m['NUMBER'] > after]

@dataclass
class TaskInstanceExecution:
    # Task id, the task that this instance is created from
    # Because of refinements this cannot be inferred from the instance id directly
    task_id: TaskID

    # The a-priori information that is needed to start the task
    parameter: Record | None = None

    # Restrictions on which actors can perform this task instance
    # An actor must be affiliated with **all** actors from the set to work on the instance
    for_actor_set: set[ActorID] = field(default_factory=set)
    
    # The references to the required information spaces for this instance
    # In this structure, all requirements are already resolved to concrete information spaces
    info_space_bindings: dict[str,InformationSpaceID] = field(default_factory=dict)
