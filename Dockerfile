#Build stage for getting and building the javascript and css assets,
#and for building the docs.
FROM python:3.12-slim-bookworm AS builder
RUN apt-get update -y; apt-get install -y npm
WORKDIR /app/static
COPY static/package.json ./package.json
RUN npm install .
#Pygraphviz and libsass need extra packages to build, that should not be part of deployment image
#Build it in this image and then copy from the builder in deployment
#RUN apt-get update -y; apt-get install -y build-essential gcc graphviz graphviz-dev libsass-dev
#RUN pip install --no-cache-dir pygraphviz libsass

#Build docs
WORKDIR /app
RUN pip install --no-cache-dir mkdocs
#COPY mkdocs.yml ./mkdocs.yml
#COPY docs ./docs
#RUN mkdocs build

FROM python:3.12-slim-bookworm
WORKDIR /app
#Install external program dependencies

RUN apt-get update; apt-get install -y pandoc build-essential graphviz gcc graphviz-dev libsass-dev
RUN ln -s /usr/bin/pandoc /usr/local/bin/pandoc; ln -s /usr/bin/dot /usr/local/bin/dot; ln -s /usr/bin/twopi /usr/local/bin/twopi
#Copy pygraphviz from builder to prevent having extra dependencies
#COPY --from=builder /usr/local/lib/python3.12/site-packages/pygraphviz-1.14.dist-info /usr/local/lib/python3.12/site-packages/pygraphviz-1.14.dist-info
#COPY --from=builder /usr/local/lib/python3.12/site-packages/pygraphviz /usr/local/lib/python3.12/site-packages/pygraphviz

#Copy libsass dependencies from builder
#COPY --from=builder /usr/local/lib/python3.12/site-packages/libsass-0.23.0.dist-info /usr/local/lib/python3.12/site-packages/libsass-0.23.0.dist-info
#COPY --from=builder /usr/local/lib/python3.12/site-packages/_sass.abi3.so /usr/local/lib/python3.12/site-packages/_sass.abi3.so
#COPY --from=builder /usr/local/lib/python3.12/site-packages/pysassc.py /usr/local/lib/python3.12/site-packages/pysassc.py
#COPY --from=builder /usr/local/lib/python3.12/site-packages/sass.py /usr/local/lib/python3.12/site-packages/sass.py
#COPY --from=builder /usr/local/lib/python3.12/site-packages/sasstests.py /usr/local/lib/python3.12/site-packages/sasstests.py
#COPY --from=builder /usr/local/lib/python3.12/site-packages/sassutils /usr/local/lib/python3.12/site-packages/sassutils

#Install python dependencies
RUN pip install --no-cache-dir poetry
#Install static assets
COPY --from=builder /app/static/node_modules ./static/node_modules
#COPY --from=builder /app/static/docs ./static/docs
#Install local python project
COPY c2sketch ./c2sketch
COPY pyproject.toml ./pyproject.toml
RUN poetry install
#Create empty data storage (mount /app/data locally to access on the host)
RUN mkdir -p data/models; mkdir -p data/scenarios

#Create empty plugins folder (mount /app/plugins locally to add custom plugins)
RUN mkdir -p plugins/static

EXPOSE 8400
CMD ["poetry","run","c2sketch-app","--host","0.0.0.0","--port","8400","--model_path","/app/data/models","--scenario_path","/app/data/scenarios","--plugin_path","/app/plugins"]
