C2Sketch is a modeling package for modeling, analysis, and simulation of information
driven collaborative work systems.
In a military context such systems are commonly referred to as Command and Control (C2) systems,
but similar goal-oriented task coordination and support systems exist other organizations by different names.
C2Sketch is not limited to strict hierarchic organizations but aims to support a wide range of organizations
and systems.

C2Sketch provides the following:

- A meta-model and concrete modeling language for expressing networks of actors, their (shared) tasks, and
  the information-spaces in which they communicate and coordinate their efforts.
- Modules for reading and writing these network models
- Inference algorithms to compute logically implied properties of the networks
- Visualization of the network and organization structure
- An agent-based simulation framework for off-line simulation of scenario's
- An execution environment for human-in-the loop simulation

C2Sketch can be used as a Python library but also provides an ASGI web application for interactive
modeling, visualization and simulation.
The easiest way to run the C2Sketch web-app is using Docker (a Dockerfile is included).
