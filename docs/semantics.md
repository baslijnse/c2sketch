# Execution semantics #

To understand the meaning of the c2sketch task models we need to understand what
it means to perform a task.
For every task(instance) we need to be able to tell *who* can perform on that task.
And for every potential task in a mission plan we need to understand *when* and
by *whom* it can be instantiated.

## Performing simple tasks ##
The most straightforward case to consider is working on simple tasks.
By "simple" we don't mean the task is easy, but that it is an instance of a task
that is not divided into sub-tasks.
These task instances are the *leaves* in the *forest* of task instances.

A simple task instance can only be accessed by actors that satisfy the `Actor` constraints
for the task instance itself **and** all the compound tasks that it is directly or indirectly
part of. An actor satisfies an `Actor` constraint when it **is** the specified actor, *or* it
is directly or indirectly affiliated with that actor.

Hence for any actor a *task list* of simple task instances can be derived from the complete
forest of task instances for that actor to choose to perform.

When a task instance is selected for performance, its description, its input parameters and all
bound operational pictures are made available to the actor.
For writable operational pictures this also means that a means for contributing to the
picture is made available.

When multiple task instances are selected, the descriptions and inputs of all tasks are shown
together and the **union** of the bound operational pictures is made available.

## Performing compound tasks ##

Compound tasks, that are split up into parts, cannot be performed directly.
They can be worked by selecting one or multiple of the simple tasks it consists of.
Only those compound task instances are made available to a given actor for which that
actor can work on **at least one** simple task that the actor can work on **and** that
is directly or indirectly part of that complex task.
(When actors cannot work on any of the sub-tasks of a task, they cannot work on it)

## Adding task instances ##

Only simple tasks can be initiated. Initiating complex tasks is done inititiating one
of the *initial* sub-tasks of a complex tasks. In many cases even complex compositions of tasks
start with a single first step.
Whenever a simple task is initiated that is part of a complex task, the parent context of
the simple task, and potentially its parent context and so on, has to be indicated by the initiating user. This is can be a new instance of the complex task, or an existing task that
the actor is already involved in.
When a task is initiated, the initiating actor is responsible forselecting not yet specified operational picture bindings and missing input parameters.
When the parent (or other ancestor) is a new complex task, the additional initial instances of
other sub-tasks are also initiated. It is again the responsibility of the initiating actor to
also specify any missing parameters or operational pictures for these tasks.
