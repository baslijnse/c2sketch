# Concepts
C2Sketch models are built around just a handful of concepts.
This "C2Sketch meta-model" explains what these concepts are and how they are related.

## Essentials: Models, Tasks and Information Spaces

*Models* are the primary top-level containers for other elements such as *tasks* and *information spaces*.

```plantuml
@startuml
class Model
class Task 
class InformationSpace

Model *-- "0..*" Task 
Model *-- "0..*" InformationSpace

Task *-- "0..*" Task
InformationSpace *-- "0..*" InformationSpace

Task -> InformationSpace: triggers
Task <- InformationSpace: requires
hide members

@enduml
```

As you can see in the diagram above a *model* contains a number of recursive elements.
*Tasks* describe units of work. These units of work can be decomposed recursively into hierachical structures of tasks containing sub-tasks.

The third essential concept is the notion of an *information space*.
An *information space* is a very broad container for modeling anything where information is exchanged or can be stored. Examples of *information spaces* in the digital realm are databases, computer networks or web pages.
In the physical space you can model for example a whiteboard, a room where people can talk, or even the windows and mirrors through which a driver observes the environment as *information spaces*.
Just like tasks, *information spaces* can also be recursively nested.

*Tasks* and *information spaces* are related in two ways. The first relation is that is common for tasks to require access to information, or to produce new information.
This information needs to come from somewhere, so the specication of *tasks* can contain required *information spaces* for reading information from or writing information to (or both).

The second relationship between *tasks* and *information spaces* is that the "consumption" or processing of information in the context of one task, can trigger work on another related, and potentially new task.
So the specification of (potential) *tasks* may contain specifications of which *information spaces* may trigger them.

## Enactment: Actors and Locations

The specification of a socio-technical model would not be complete without modeling **who** performs the specified *tasks* (the **what**) and **where**.

```plantuml
@startuml
class Model #white
class Actor
class Location

Model *-- "0..*" Actor
Model *-- "0..*" Location

Actor "0..*" o-- "0..*" Actor
Location "0..*" o-- "0..*" Location

hide members

@enduml
```

*Actors* are the entities that can perform work as modeled in the *task* decompositions.
This actor can be used to model people who do work, but also to model machines or parts of machines such as an autopilot or a guidance system.
*Actors* can be freely grouped to specify organizations or teams, which are also *actors*.
C2Sketch does not enforce any hierarchy on organizing *actors*.
*Actors* can be members of multiple organizations or teams and organizations can be arbitrarily nested.
The only restriction is that *actors* can not directly or indirectly be part of themselves.
The set of actors has to form a *directed acyclic graph*.

*Tasks* are performed in the context of a physical or virtual *location*.
These can be symbolically specified as part of a model. *Locations* are not specified as specific geographic coordinates, but by using meaningful names. This also means that *actors* can be in multiple *locations* at once.
They can be simultanously "at work", which allows them to talk directly to coworkers, and "in a videocall" with people across the planet.
*Locations* can also be contained within other *locations* to model different levels of granularity within a single *model*.

```plantuml
@startuml
class Model #white
class Task #white
class InformationSpace #white
class Actor
class Location

Model *-- "0..*" Task 
Model *-- "0..*" InformationSpace
Model *-- "0..*" Actor
Model *-- "0..*" Location

Task *-- "0..*" Task
InformationSpace *-- "0..*" InformationSpace

Actor "0..*" o-- "0..*" Actor
Location "0..*" o-- "0..*" Location

Task <.. Actor: contrains
Task <.. Location: contrains
InformationSpace <.. Actor: contrains
InformationSpace <.. Location: contrains

hide members

@enduml
```

*Actors* and *locations* are related to *tasks* and *information spaces* as **constraints**.
You can specify for all *tasks* and *information spaces* that they are restricted to specific
*actors* or *locations*. This makes it possible to model the notion of capability, simply by restricting *tasks* to (a group of) *actors*.

## Annotation: Meta data in Attributes

The *tasks*, *information spaces*, *actors* and *locations* in a *model* define a connected graph.
For the purpose of analysing or visualizing these graphs it is useful to be able to add arbitrary
information to the nodes of this graph.

This can be done by adding *attributes* to the nodes in the model.

```plantuml
@startuml
interface ModelNode

class Attribute {
  name: str
  value: str
}

class Task #white
class InformationSpace #white
class Actor #white
class Location #white

ModelNode *- "0..*" Attribute

ModelNode <|-- Task
ModelNode <|-- InformationSpace
ModelNode <|-- Actor
ModelNode <|-- Location 

hide members
show Attribute members
@enduml

```

All concepts in a *model* have this possibility to contain *attributes* which are simply named values.

## Data: Types and Records

To add more deph to the modeling of *information spaces*, C2Sketch also allows the specification of the information contained in them (*records*), as well as the specification of *types* to which the information must conform.
This makes it possible to analyze the model for inconsistenties and to enter and display information during execution of the model.

```plantuml
@startuml
class Model #white
class InformationSpace #white

class Record
class RecordField

class RecordType
class RecordTypeField

class Value

class Literal
class LiteralType

Model *-- "0..*" InformationSpace
Model *-- "0..*" RecordType

InformationSpace o-- "0.." Record
InformationSpace -> "0..1" RecordType : specifies

RecordType <.. Record : conforms to

Record *-- "0..*" RecordField
RecordField *-- Value

Value <|-- Literal
Value <|-- Record

RecordType *-- RecordTypeField
RecordTypeField *-- ValueType

ValueType <|-- LiteralType
ValueType <|-- RecordType

hide members

@enduml
```

*Records* are programming language agnostic key-value data structures.
A *record* consists of a set of fields that can either hold primitive value literals, or nested *records*.
The *record types* reflect this structure and also consists of a set of fields that specify the type of the fields of the *records* that conform to the *record type*

## Abstraction: Task Definitions and Instances

To model the *dynamic* unfolding of tasks into sub-tasks based on the context of a scenario, C2Sketch supports the notion of *Task definitions*.
Task definitions are similiar to the specification of tasks but are parameterized.
By supplying different parameter values, multiple instances of a task definition can be instantiated.
Task instances can be specified at design-time, or their creation can be deferred until run-time.

To enable the creation of new task instances at run-time, a *parameter type* can be specified that defines the data type of the allowed parameter values.

```plantuml

@startuml
class Model #white
class Task #white
class RecordType #white
class Record #white

Model *-- "0..*" Task 
Task *- "0..*" Task

class TaskDefinition
class TaskInstance

Model *-- "0..*" TaskDefinition
Task *-- "0..*" TaskDefinition
TaskDefinition *-- "0..*" Task
TaskDefinition *- "0..*" TaskDefinition

Model *-- "0..*" TaskInstance
Task *-- "0..*" TaskInstance
TaskDefinition *-- "0..*" TaskInstance
TaskInstance *- "0..*" TaskInstance

TaskDefinition <.. TaskInstance : conforms to

RecordType <. TaskDefinition : abstracts over
Record .> TaskInstance : defines

hide members
@enduml

```

## Sharing: Task References

Although way of specifying task hierarchies is through composition (defining nested tasks inside a task specification), there is an alternative method to create hiarchies.

Inside task hierarchies a **Task reference** may be specified which references another top-level hierarchy.

```plantuml
@startuml

class Model #white
class Task #white
class TaskInstance #white
class Record #white
class TaskReference

Model *-- "0.." TaskReference
Task *-- "0.." TaskReference
TaskDefinition *-- "0.." TaskReference

Record "0..1" .> TaskReference : defines instance

Task <.. TaskReference : references
TaskInstance <.. TaskReference : references

hide members
@enduml
```

Referenced tasks are treated as if they are a regular branches of the task hierarchy in which they are defined.
Top-level task instances can also be referenced. In these cases the parameter is supplied to identify the task instance.

It is allowed to have **multiple** references pointing to the same top-level task(instance).
This enables *sharing* of sub-tasks between different task hierarchies.

## Modularity: Imports

To facilitate the creation of large models, C2Sketch allows imports of other models.
When models are imported, the concepts defined in those models can be referenced by adding an `@` and the referenced model id to node references in the model. For example `some_info_space@c2sketch.example.model`.

```plantuml
@startuml
class Model {
    id: ModelID
}
class Import {
    reference: ModelID
}

Model *-- "0..*" Import
Model <.. Import : references

hide methods

@enduml
```
