# Preparing mission plans
Mission plans are documents that contain the elements that make up the plan:
Actors, Tasks, Operational Pictures and Message types.

The plan can be thought of as a layered structured. 
At the first level only Actors and Message types can be defined.
Message types, because they are globally accessible throughout the plan, and actors
because each mission plan contains at least the actor(s) whom the mission is defined for.

The second layer can contain actors and tasks.
Actors can be affiliated with other actors to create a hierarchy.
Tasks can be defined as part of an actor. These are the independent tasks that that
actor is responsible for, and/or capabla of performing.

Further layers may also contain operational pictures.
An operational picture is always defined as part of a task.
Tasks can also be decomposed into sub-tasks to create additional layers of tasks.

## The mission plan outline
To navigate or edit the structure of a mission plan you can use the mission plan outline.
This is the tree structure on the left of screen.
The root node of this tree structure is the mission plan itself.
All other nodes are the parts of the plan.
By clicking nodes in the outline you can navigate between the parts of the plan.

In the outline you can also create new nodes, delete nodes, or rename nodes.
Tasks and operational pictures can also be moved within the plan.

### Adding nodes
When you click the `Add...` button a dialog will open that will let you add new node to the plan.
You can select the type of node you want to add.
Depending on the type of node you can enter some initial details of the node.
For message types you only enter a name. For actors, tasks and operational pictures you can
specify a name as well as a more elaborate description of the node.
For tasks and operational pictures you can also specify a parent node.
This defaults to the selected node in the mission plan outline.

### Moving nodes
Tasks and operational pictures can be moved to a different parent node using the `Move...` button. This will open a dialog that lets you specify a new parent node. Tasks can not be moved to their own children, as this would introduce cycles in
the task decomposition which is meaningless. A task simply cannot be a part of itself.

### Renaming nodes
All nodes in the plan can be renamed using the `Rename...` button.
This will open a dialog that will let you specify a new name.

When a node is renamed, all references to that node in the plan are automatically updated
to use the new name.

### Deleting nodes
Nodes can be deleted from the plan using the `Delete...` button.
This will open a confirmation dialog because deleting a node cannot be undone.
Deleting nodes is recursive. All child nodes and their descendants are also deleted
when a node is removed.

## Global plan information
When the root node in the mission plan outline is selected you can view the global
information of the plan as whole.

- **Name:** The name of the plan, used as filename when exporting the plan
- **Title:** A more descriptive title used to describe the plan in the plan management screen and in exported reports
- **Summary:** A longer description of the mission the plan is defined for. You can also
use this field to explain the main mission goal and scope of the plan.

Using the `Edit...` button you can edit the title and summary.
The name can only be changed by renaming the node in the mission plan outline.

## Actors
When an actor node (indicated with the person icon) is selected you can view and edit
the details of that actor. These details are divided into separate sections.

### General information

### Relations

### Tasks


## Tasks

When an task node (indicated with the checkmark icon) is selected you can view and edit
the details of that task. These details are again divided into separate sections.

### General information

### Required information

### Reporting

### Decomposition

### Instances

### Automation

## Operational pictures
When an operational picture node (indicated with the whiteboard icon) is selected you can view and edit the details of that picture. These details are again divided into separate sections.
### General information

### Type

### Visualization

### Messages

### Use

## Message types

When a message type node (indicated with the puzzle piece icon) is selected you can view and edit the details of that type. These details are also divided into separate sections.

### Fields

### Use
