# User Guide
This user guide explains everything you can do with the C2Sketch tool.

It is organized into the following sections:

- [**Concepts:**](concepts.md)
  Explains the "meta-model" with the concepts used in C2Sketch models.
- [**Language Syntax:**](syntax.md)
  Explains the concrete syntax of the C2Sketch modeling language.
- [**Managing models**](manage.md)
  Explains how to create, load and select models using the web app or the library.
- [**Editing models**](editing.md)
  Explains how to edit models in the web app.
- [**Executing models**](execution.md)
  Explains how to execute/enact models in the web app.
- [**Exchanging models**](exchange.md)
  Explains how to import or export your models in various formats using the web app or the library.