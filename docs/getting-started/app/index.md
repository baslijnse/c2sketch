# Getting Started with the Web App#
This getting started guide will help you install and set up C2Sketch.
After that it walks you through a small example to demonstrate the
main features of the tool.

## Installation ##

The easiest way to run C2Sketch is using [Docker](https://www.docker.com).
A `Dockerfile` is included in the root folder of the repository.

First build an image (from within the root folder) using:

```
docker build . -t c2sketch
```

Then start the container:

```
docker run -p 8080:8080 -d c2sketch
```

C2Sketch should now be available at <http://localhost:8080/>

![First use](img/first-use.png)

On your first use of C2Sketch, you will need to create an initial user account.
Simply enter a username and password to create the first user account.

If you want to use your C2Sketch deployment with more users, you can use this first account
to create additional users.

Once you have created the initial user you can proceed to log in.

![Login](img/login.png)

## Creating a minimal model

Once you have installed C2Sketch, you can start creating your first model.

![Plan selection](img/plan-selection.png)

You can create a new model by entering a descriptive name in the 'New Model' panel and
leaving the "Template" field set to "Empty model".

The scenario we'll use as an example in this guide will be a family dinner.
The goal of this familiar task is for every family member to be have a fulfilling and healthy meal.
Although this goal is simple, with the busy lives of a modern family, making it happen
requires a certain amount of collaboration and coordination.

Create an empty model called `Family Dinner` using the 'New Model' panel to continue.

## The phases of a mission plan

Once you have created the empty plan you will be shown the mission details screen.

![Empty Plan](img/empty-plan.png)

This screen allows you to choose between the three main tasks that are supported by
C2Sketch.

- **Prepare**
  will take you to an editing environment where you can edit the mission plan.
- **Execute** 
  will take you to the embedded execution environment that let's you walk through the
  various tasks defined in the plan.
- **Export**
  will allow you to download the mission plan in various formats.

When you have just created an empty plan, "Execute" and "Export" are not very useful yet.
We will therefore continue with the preparation of our plan.

If you click `Prepare` you will see the plan preparation screen.

![Preparation](img/prepare-empty.png)

## Modeling a family of actors

The first thing we need to define is who is involved in the family.
C2Sketch uses the concept of actors both for persons, as well as for organizations.
We will first create the four (nondescript) family members: Mother, Father, Son and Daughter.

To do this click the `Add...` button (with the plus icon) in the top left of the screen. 
This will open a dialog to add a new item to the plan.
When the top-level plan node is selected in the outline on the left, this will by default
be set to adding a new actor.

![Create an actor](img/create-actor.png)

Enter "Father" as `Name` and leave the `Description` empty. Save by clicking the `Add` button.
Repeat this process for the other family members and for an additional actor named "Family".

![All actors](img/all-actors.png)

The last step we need to do complete the minimal model of the family is to structure the relations between actors.
The only relationship you can model in C2Sketch is affiliation. In this example, "Family" is the organisation that all other actors are affiliated with.

You can link create these relationships by clicking the "Family" actor in the outline on the left and selecting the `Relations` tab.
In the `Affiliated actors` section you can select one of the other actors and click `Add affiliated` to add them to the family. Do this for each of the family members.

![Related actors](img/related-actors.png)
The family structure is also reflected in the outline view on the left.

## Sketching out the main tasks
Once all actors are defined we can sketch out the tasks that are needed to have
a succesful family dinner.
We start with defining the main mission task which we simply call "have dinner".
This will be a joint effort by the whole family, so we'll create it as a task.

Do this by selecting the "Family" actor in the outline and clicking the `Add...` button again.
This time select `Task` as the node type, enter "have dinner" in the `Name` field and leave the `Description` field blank.

![Create main task](img/create-main-task.png)

Next we'll decompose this task into the steps that will accomplish our goal of having a nice family dinner.
We can do this by starting with the result and reason backwards:

- We want to eat some nice food together.
- To do so, someone will have to prepare the food.
- That can only be done, if the required groceries have been bought.
- This can only be done if we choose what to eat first.

We can add these subtasks directly to the outline by selecting the "have dinner" task and
using the `Add...` button again. The `As part of` field will be automatically set to "Family/have dinner" because the "have dinner" is selected.

Add sub-tasks with the names "eat dinner", "prepare dinner", "buy groceries", "choose recipe" to the "have dinner" task. The outline should now like like this.

![Main sub-tasks](img/main-sub-tasks.png)

In addition to these main steps we will need to add an extra task.
We'll assume that one of the parents, let's say the father, will be responsible for all
tasks except eating dinner. That means that initially he will be the only family member involved in the main mission of having a nice dinner.
However, the other family members can not just go off and do something else completely.
They will have to at least pay attention to some signal from the father that dinner
is ready and they can come and start eating dinner.

We therefore add another task named "wait for dinner" to the plan that will exist
in parallel with the preparation tasks.

## Sketching the information exchange
To keep this guide short, we'll limit ourselves to just two places of information exchange:
A shopping list, and a signal that dinner is ready.

The first transfers information between the "choose recipe" and the "buy groceries" task.
The result of choosing a recipe determines what groceries are needed for dinner.
By putting the required items on a grocery list, the shopping task remains independent of the
the choose recipe task.

The signal that dinner is ready is needed for coordination.
When dinner is ready it must be communicated to the other family members.
If it is not communicated they will not know when to initiate the "eat dinner" task.

We'll first add a 'shopping list' operational picture to our mission plan.
Operational pictures are available to the subtasks of the task in which they are
defined. We'll therefore select the top-level 'have dinner' task in the outline 
and use the `Add...` button to create the operational picture.
Select `Operational Picture` as node type and enter "shopping list" in the `Name` field.

For the signal that dinner is ready, we'll add another operational picture to the 'have dinner' task. We'll call this one "shouting range" because the signal will be the father
shouting "Dinner is ready!" through the house. Any of the family members may then
respond to that signal if and only if they are within shouting range of the kitchen.

![Operational pictures](img/with-ops.png)

## Adding types

The last two items we'll add to the plan are message types.
These types define the type of intermation that is produced by some tasks and used by others, as well as what information may be exchanged in an operational pictures.
They can be viewed as the interface or contract for the structure of information that is exchanged between different actors.

Adding the nodes is as always done with the `Add...` button. Select `Message Type` as the node type enter "shopping item" in the `Name` field.
Add another message type with the name "shout".

![Empty message types](img/empty-types.png)

Message types in C2Sketch are simple *record like* definitions.
They are similar to database table schema's, data classes or C structs.
They define a list of named fields. Each field has a simple primitive type.

To define the fields of the "shopping item" select its node in the mission plan outline
and click the `Add field` button.

For the first field use "product" as `Field name` and select "string" as `Field type`.
Leave the other fields unchanged.
Add another field using "quantity" as `Field name` and select "int" as `Field type`.

![Shopping item type](img/type-shopping-item.png)

The "shout" type is an even simpler type. Just add a single field named "message" of primitive type "string".

![Shout type](img/type-shout.png)

## Connecting the types
Now that we have all the main parts of the plan (minimally) defined we can flesh
out the remainder of the plan by connecting the dots.

We'll first use the message types we just created to type the operational pictures.
Select the "shopping list" operational picture in the outline and select the `Type` tab.
In the `Type` select box you'll see that you can choose from the globally defined types or define another custom type. Select "shopping item" from the list.
This means that we model that all information exchanged via the shopping list consists of a product name and a quantity as we specified in the message type.

Similarly we can select the "shouting range" operational picture and assign the "shout" message type. This means is how we model that messages (being just strings) can be shouted around the house to anyone who will listen.

![Typed Operational Picture](img/typed-picture.png)

Next we'll add some more detail to the tasks. As you can see based on the various tabs you get when selecting a task node, there are several aspects of tasks you can model.
For now we'll focus on the inputs and outputs.

We'll start by selecting the "choose recipe" task and selecting the `Reporting` tab.
This is where you can specify what information is reported during the executing of the task.
What we would like to express is that as a result of choosing a recipe we'll know what we need to buy next. We can model this by specifying an output type and an operational picture to use for communicating this information.

Choose "shopping item" as the result type of the task, and select "shopping list" as connected operational picture. This means that during the task we can report information of type "shopping item" to the "shopping list".

![Result specification](img/result-spec.png)

For the "buy groceries" task we can now specify how the information from the operational picture is used in that task. Select the "buy groceries" task and select the `Required information` tab.

Connecting the required information is a little more subtle than connecting a task result because C2Sketch distinguishes two types of input information:

- A priori information, that needs to be available before you can even start the task
- Operational information, that you will need access to to perform the task.

For the a priori information you can specify an input type, and optionally an operational picture where that information may come from.
For the operational information you can specify one or multiple operational pictures that you need to do the task.

For the groceries shopping we'll only use the second option. We just want to express that we need access to the shopping list while shopping.
We therefore only select the "shopping list" item in the `Required Operational Pictures` section and use `Add picture` button to it to the task.

![A required operational picture](img/required-op-groceries.png)

Another task that needs access to an operational picture is the "wait for dinner" task.
In this case the sole purpose of the task is to monitor if someone will shout that dinner
is ready. We therefore specify "shouting range" as a required operational picture of this task.

![Another required operational picture](img/required-op-wait.png)

To specify that information from the "shouting range" operational picture may be used to initiate the "eat dinner" task (at least for some of the family members). We can specify this operatonal picture as the `Connected Operational Picture` in the "eat dinner" task.

![Connected input operational picture](img/input-op-shout.png)

To complete the information flow, this signal will be reported as result of type "shout" when performing the "prepare dinner" task.

![Task result shout](img/result-shout.png)

## Defining task instantiation
The last aspect that we need to model is how tasks are instantiated. Plans prepare for tasks that may or may not be executed depending on the situation.
On the other hand, there are tasks that are implied, and will always be performed by certain actors.

To specify when tasks are performed we need to model their instantianion.
This is done through the `Instances` tab of a task node in the plan.
For our main "have dinner" task, we want to model that is implicit and unconditional.
As soon as we enact this plan, there will be a dinner that this plan is about.

Click the `Add instance` button to create a single instance of this task.
You can leave the `Responsible actor` field empty, as the combined task will be a shared responsibility of the entire family.

![Main task instance](img/main-task-instance.png)

For each of the sub-tasks we also need to specify how they are instantiated.
Because we assume that the father will do all preparation and the rest of the family will simply wait for dinner we will go ahead and add instances for the following tasks:

- "choose recipe" will be unconditional and will have one instance with "Father" as responsible actor.
- "wait for dinner" will also be uncondiational and will be instantiated three times. One instance for each remaining family member.

![Sub task instances](img/sub-task-instances.png)

The remaining tasks: "buy groceries", "prepare dinner" and "eat dinner" are all dependent on the earlier tasks succeeding or the observation of a signal in an operational picture. We can therefore all mark them as `Conditional` tasks.

![Conditional task](img/conditional-task.png)

Now that we have modeled how each of the (sub)tasks in the plan is instantiated we have a minimal complete mission plan that we can execute.

## Executing the plan
TODO: Walk through a scenario where the full plan is used

## Exporting the plan
TODO: Explain how you can download a word version of the full plan to
show to your friends 

## Next Steps
TODO: Split up in more tasks, divide tasks over different actors etc.