# C2Sketch Documentation

C2Sketch is a modeling tool for modeling socio-technical systems with a focus on Command and Control (C2) systems.
It can be used to create, analyse and validate models of the complex cooperations and information
sharing activities that are required to collaboratively perform complex tasks.

The C2Sketch tool is implemented as a hybrid [Python](http://python.org) package.
It can be used as a **library** which provides a collection of classes for representing (C2) models and
a useful collection of functions for analysis and visualization of these models.
The package also provides a **web application** that facilitates structured authoring of models and provides an execution engine that can be used to interactively test the behavior of the modeled systems.
Finally, it provides a **modeling language** for concise textual specification of models in a nice programming language-like syntax. A parser and printer are provided that allow use of models expressed in this language with the **library** and **web application**. 


- If you are new to C2Sketch, the [Getting Started](getting-started/app/index.md) guide will get you started quickly.
- If you are are looking for more in-depth information, or are looking for specific features
  you may want to consult the [User Guide](user-guide/index.md).


C2Sketch is still in active development and primarily intended for internal use at
 [NDLA-FMW](https://faculteitmilitairewetenschappen.nl/). The latest publicly available version is `0.2.0`


